#!/usr/bin/env python3
# # -*- coding: utf-8 -*-

import gbtdis.op_codes as oc
from gbtdis.emulator import memory_region as mr
from functools import lru_cache

class origin_type:

  @staticmethod
  def undefined():
    return "U"

  @staticmethod
  def is_undefined(value):
    return value == origin_type.undefined()

  @staticmethod
  def register_A():
    return "A"

  @staticmethod
  def is_register_A(value):
    return value == origin_type.register_A()

  @staticmethod
  def register_F():
    return "F"

  @staticmethod
  def is_register_F(value):
    return value == origin_type.register_F()

  @staticmethod
  def register_B():
    return "B"

  @staticmethod
  def is_register_B(value):
    return value == origin_type.register_B()

  @staticmethod
  def register_C():
    return "C"

  @staticmethod
  def is_register_C(value):
    return value == origin_type.register_C()

  @staticmethod
  def register_D():
    return "D"

  @staticmethod
  def is_register_D(value):
    return value == origin_type.register_D()

  @staticmethod
  def register_E():
    return "E"

  @staticmethod
  def is_register_E(value):
    return value == origin_type.register_E()

  @staticmethod
  def register_H():
    return "H"

  @staticmethod
  def is_register_H(value):
    return value == origin_type.register_H()

  @staticmethod
  def register_L():
    return "L"

  @staticmethod
  def is_register_L(value):
    return value == origin_type.register_L()

  @staticmethod
  def register_SP():
    return "SP"

  @staticmethod
  def is_register_SP(value):
    return value == origin_type.register_SP()

  @staticmethod
  def memory(address = 0):
    return ("M", address)

  @staticmethod
  def is_memory(value):
    if not isinstance(value, tuple):
      return False
    if not len(value) == 2:
      return False
    return value[0] == "M"

  @staticmethod
  def rom(bank=0, address=0):
    return ("R", bank, address)

  @staticmethod
  def is_rom(value):
    if not isinstance(value, tuple):
      return False
    if not len(value) == 3:
      return False
    return value[0] == "R"

class backtrack_point:

  def __init_(self, current_address):
    """
    create a new backtrack point object
    Current address is the address, where the current program flow is.
    """
    self.register_origins = {
      "A" : origin_type.undefined(),
      "F" : origin_type.undefined(),
      "B" : origin_type.undefined(),
      "C" : origin_type.undefined(),
      "D" : origin_type.undefined(),
      "E" : origin_type.undefined(),
      "H" : origin_type.undefined(),
      "L" : origin_type.undefined(),
      "SP" : origin_type.undefined()
    }
    self.current_address = current_address
    self.loaded_bank_origin = origin_type.undefined()
    self.memory_origins = {}

  def get_current_address(self):
    return self.current_address

  def get_register_origin(self, register):
    return self.register_origins[register]

  def get_loaded_bank_origin(self):
    return self.loaded_bank_origin

  def get_memory_origin(self, address):
    # address points to rom bank 0
    if mr.is_rom_bank_0(address):
      return origin_type.rom(0, address)

    # address points to swapable rom and loaded bank origin not known
    if mr.is_rom_bank_swapable(address) and origin_type.is_undefined(self.loaded_bank_origin):
      return origin_type.undefined()

    # return origin type from memory
    if address not in self.memory_origins:
      return origin_type.undefined()
    else:
      return self.memory_origins[address]

class backtrack:
  def __init__(self, jump_graph, loaded_bank, current_bank, current_address, current_section):
    """
    Create a new backtrack object.

    Inputs:
    jump_graph      = flow graph of program
    loaded_bank     = loaded bank at the current point of execution
    current_bank    = bank of the current point of execution
    current_address = address of the current point of execution
    current_section = program section of the current point of execution
    """
    self.rom_image = jump_graph.get_rom_image()
    self.loaded_bank = loaded_bank # bank_number of the loaded bank in 0x4000-0x7FFF
    self.current_bank = current_bank # bank_number of current_address
    self.current_address = current_address
    self.current_section = current_section
    self.jump_graph = jump_graph

  def convert_double_complement_to_singed_int(self, value):
    """
    convert offset from double complement to signed int
    """
    if value >= (1<<7):
      value -= 1<<8
    return value

  def loaded_bank_number(self, depth = 10):
    """
    check if command at self.current_address is of type "load into memory",
    so a bank change is possible.
    if a bank change will happen with this command, the new possible bank numbers will be returned.
    if backtracking will take too many steps into depth, than an empty set is returned
    otherwise None is returned
    """
    if depth <= 0:
      return set()

    op_code_possiblities = self.memory_value(self.current_address, depth-1, self.current_address)
    result = None
    for op_code in op_code_possiblities:
      if not oc.bank_change_possible(op_code):
        continue
      
      address = self.write_destination(self.current_address, op_code, depth)

      # check if write address makes rom change possible
      # if not return and save compute time for determine the value
      for a in address:
        if not (mr.is_rom_select_lower_bits(a) or mr.is_rom_select_upper_bits(a)):
          continue

        # get the value, that is written to bank select
        value = set()
        if oc.is_register_A_input(op_code):
          value = self.register_value_A(depth-1, self.current_address)
        elif oc.is_register_B_input(op_code):
          value = self.register_value_B(depth-1, self.current_address)
        elif oc.is_register_C_input(op_code):
          value = self.register_value_C(depth-1, self.current_address)
        elif oc.is_register_D_input(op_code):
          value = self.register_value_D(depth-1, self.current_address)
        elif oc.is_register_E_input(op_code):
          value = self.register_value_E(depth-1, self.current_address)
        elif oc.is_register_H_input(op_code):
          value = self.register_value_H(depth-1, self.current_address)
        elif oc.is_register_L_input(op_code):
          value = self.register_value_L(depth-1, self.current_address)
        elif oc.is_one_byte_memory_input(op_code):
          value = self.memory_value(self.current_address+1, depth-1, self.current_address)

        # if first value in result
        if result == None:
          result = set()
        # add the new loaded bank number
        result.union(value)
    return result
      
  def write_destination(self, current_address, op_code, depth=20):
    """
    given the current address of execution and the op code
    this function returns all possible values of the writing destination of a LD command
    if depth of recursive value finding is reached, than an empty set is returned
    if the command is not an LD command, than an empty set is returned as well
    """
    address = set()
    if oc.is_write_to_bc_location(op_code):
      address = self.register_value("BC", depth, current_address)
    elif oc.is_write_to_de_location(op_code):
      address = self.register_value("DE", depth, current_address)
    elif oc.is_write_to_hl_location(op_code):
      address = self.register_value("HL", depth, current_address)
    elif oc.is_write_to_address(op_code):
      high_byte = self.memory_value(current_address+2, depth, current_address)
      if len(high_byte) == 0:
        return set()
      low_byte = self.memory_value(current_address+1, depth, current_address)
      if len(low_byte) == 0:
        return set()
      for h in high_byte:
        for l in low_byte:
          address.add( (h<<8) | l)
          if op_code == 0x08:
            address.add( (((h<<8)|l)+1)%0x10000 )
    return address

  def is_write_to(self, current_address, op_code, address, depth=20):
    """
    Check if the current command writes to address.

    Input: 
    current_address = address of the current point of execution
    op_code         = command that is executed
    address         = address that is checked against
    depth           = recursion depth left

    Output:
    True, if current command writes to address
    False, if not
    """
    return address in self.write_destination(current_address, op_code)

  def memory_write_value(self, address, current_address, op_code, depth=20, is_high_byte=False):
    """
    Finds the memory value, that is written to address, by recusion.

    Input:
    address         = address of the wanted value
    current_address = address of the current point of execution
    op_code         = command that is executed
    depth           = recursion depth left
    is_high_byte    = True if value of 16 Bit register is wanted,
                      and wanted is the high byte of it

    Output:
    set of values that could be written by op code
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    result = set()
    # depth will not be further decreased, because its allready done by memory_value()
    if op_code == 0x02:
      return self.register_value_A(depth, current_address)
    if op_code == 0x08:
      for sp in self.register_value_SP(depth, current_address):
        if is_high_byte:
          result.add(sp|0xFF)
        else:
          result.add(sp>>8)
      return result
    if op_code == 0x12:
      return self.register_value_A(depth, current_address)
    if op_code == 0x22:
      return self.register_value_A(depth, current_address)
    if op_code == 0x32:
      return self.register_value_A(depth, current_address)
    if op_code == 0x34:
      for value in self.memory_value(address, depth, current_address):
        result.add((value+1)%0x100)
      return result
    if op_code == 0x35:
      for value in self.memory_value(address, depth, current_address):
        result.add((value-1)%0x100)
      return result
    if op_code == 0x36:
      return self.memory_value(current_address+1, depth)
    if op_code == 0x70:
      return self.register_value_B(depth, current_address)
    if op_code == 0x71:
      return self.register_value_C(depth, current_address)
    if op_code == 0x72:
      return self.register_value_D(depth, current_address)
    if op_code == 0x73:
      return self.register_value_E(depth, current_address)
    if op_code == 0x74:
      return self.register_value_H(depth, current_address)
    if op_code == 0x75:
      return self.register_value_L(depth, current_address)
    if op_code == 0x77:
      return self.register_value_A(depth, current_address)
    if op_code == 0xC5:
      if is_high_byte:
        return self.register_value_B(depth, current_address)
      else:
        return self.register_value_C(depth, current_address)
    if op_code == 0xD5:
      if is_high_byte:
        return self.register_value_D(depth, current_address)
      else:
        return self.register_value_E(depth, current_address)
    if op_code == 0xE0:
      return self.register_value_A(depth, current_address)
    if op_code == 0xE2:
      return self.register_value_A(depth, current_address)
    if op_code == 0xE5:
      if is_high_byte:
        return self.register_value_H(depth, current_address)
      else:
        return self.register_value_L(depth, current_address)
    if op_code == 0xEA:
      return self.register_value_A(depth, current_address)
    if op_code == 0xF5:
      if is_high_byte:
        return self.register_value_A(depth, current_address)
      else:
        # flags tracking not implemented
        return set()

  def memory_value_of_prev_sections(self, address, depth=20):
    """
    Return the union of all memory values, by recursivly moving to
    previous sections.

    Input:
    address = address in memory
    depth   = recursion depth left

    Output:
    a set of all possible values for this memory location
    set is empty if value could not be found
    """
    result = set()
    prev_sections = self.jump_graph.get_previous_code_sections(self.current_section)
    for section in prev_sections:
      loaded_bank = self.loaded_bank
      if section.get_bank() != 0:
        loaded_bank = section.get_bank()
      bt = backtrack(self.jump_graph, loaded_bank, section.get_bank(), section.get_commands()[-1], section)
      result.union(bt.memory_value(address, depth))
    return result

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def memory_value(self, address, depth = 10, current_address = None, is_high_byte=False):
    """
    Returns a set of all possible values of a given address.

    Input:
    address         = loaction of the wanted value
    depth           = recursive depth left
    current_address = address of the current execution point
    is_high_byte    = only interessted at the high byte, if 16Bit value is written
    
    Output:
    set of all possible values
    set is empty, if not value could be found
    """
    if current_address == None:
      current_address = self.current_address
    result = set()

    # read from rom
    if mr.is_rom_bank_0(address):
      result.add(self.rom_image.get_value(self.current_bank, address))
      return result
    if mr.is_rom_bank_swapable(address):
      result.add(self.rom_image.get_value(self.current_bank, address-0x4000))
      return result
    
    # random read
    if mr.is_random_read(address):
      for i in range(0x100):
        result.add(i)
      return result

    if depth <= 0:
      return set()

    # hardware io
    if mr.is_hardware_io_register(address):
      # is joypad
      if address == 0xFF00:
        return set(range(0b1000000)) # only first 6 bits are set
      # is serial byte
      if address == 0xFF01:
        return set(range(0x100))
      # is serial control
      if address == 0xFF02:
        return set(range(0b100)) # only 2 lower bits are set
      # is clock divider
      if address == 0xFF04:
        return set(range(0x100))
      # is timer value
      if address == 0xFF05:
        return set(range(0x100))
      # is timer reload
      if address == 0xFF06:
        return set(range(0x100))
      # is timer control
      if address == 0xFF07:
        return set(range(0b1000)) # only 3 lower bits are set
      # is interrupts asserted
      if address == 0xFF0F:
        return set(range(0b100000))
      # is audio
      if mr.is_in_limits(address, 0xFF10, 0xFF3F):
        return set(range(0x100))
      # is lcd control
      if address == 0xFF40:
        return set(range(0x100))
      # is lcd status
      if address == 0xFF41:
        """
        0xFF41 is ment for read only.
        writing is possible, but triggers interrupts by side effects.
        Read more under the topic of "STAT writing IRQ"
        """
        return set(range(0x100))
      # is background vertical scroll
      if address == 0xFF42:
        return set(range(0x100))
      # is background horizontal scroll
      if address == 0xFF43:
        return set(range(0x100))
      # is lcd Y coordinate
      if address == 0xFF44:
        # indicates the y coordinate of procces of the lcd
        return set(range(0x100))
      # is lcd y compare
      if address == 0xFF45:
        # defines the compare value agains y coordinate of lcd
        return set(range(0x100))
      # is oam dma source address
      if address == 0xFF46:
        # start the oam dma transfer
        # copy 0xZZ00-0xZZ9F into 0xFE00-0xFE9F with value as ZZ
        return set()
      # is background palette
      if address == 0xFF47:
        # this effects only the display colors, so the effect can be ignored here
        return set(range(0x100))
      # is obj 0 palette
      if address == 0xFF48:
        # this effects only the display colors, so the effect can be ignored here
        return set(range(0x100))
      # is obj 1 palette
      if address == 0xFF49:
        # this effects only the display colors, so the effect can be ignored here
        return set(range(0x100))
      # is window y coordinate
      if address == 0xFF4A:
        # this effects only the window of the display, so the effect can be ignored here
        return set(range(0x100))
      # is window x coordinate
      if address == 0xFF4B:
        return set(range(0x100))
      return set()

    depth_penalty = depth*2 # reduce depth after X steps backwards, so infinite loop will end
    penalty_count = 0
    if mr.is_internal_ram_bank_0(address) or mr.is_object_attribute_memory(address) or mr.is_high_ram(address) or mr.is_video_ram(address) or mr.is_cartridge_ram(address):
      # get address of previous command
      prev_command_address = prev_command_address = self.current_section.get_previous_command(current_address)
      while prev_command_address != None:
        if penalty_count >= depth_penalty:
          depth -= 1
          penalty_count = 0
          if depth <= 0:
            return set()
        # get posible op codes of the address of previous command
        prev_command_op_code_possibilities = self.memory_value(address, depth-1, prev_command_address)
        for prev_command_op_code in prev_command_op_code_possibilities:
          # check if op code can change memory value
          if not oc.has_memory_changed(prev_command_op_code):
            # check if op code changed the requested memory value at address
            if self.is_write_to(prev_command_address, prev_command_op_code, address, depth-1):
              return self.memory_write_value(address, prev_command_address, prev_command_op_code, depth-1, is_high_byte)
            # check for ECHO ram write
            if self.is_write_to(prev_command_address, prev_command_op_code, address+0x2000, depth-1):
              return self.memory_write_value(address, prev_command_address, prev_command_op_code, depth-1, is_high_byte)
          # start of section reached, so continue with previous sections
          if prev_command_address == self.current_section.get_start_address():
            return self.memory_value_of_prev_sections(address, depth-1)
          prev_command_address = self.current_section.get_previous_command(prev_command_address)
        penalty_count += 1
      return set()

    if mr.is_internal_ram_bank_swapable(address):
      # TODO ram bank swap        
      # get address of previous command
      prev_command_address = prev_command_address = self.current_section.get_previous_command(current_address)
      while prev_command_address != None:
        if penalty_count >= depth_penalty:
          depth -= 1
          penalty_count = 0
          if depth <= 0:
            return set()
        # get posible op codes of the address of previous command
        prev_command_op_code_possibilities = self.memory_value(address, depth-1, prev_command_address)
        for prev_command_op_code in prev_command_op_code_possibilities:
          # check if op code can change memory value
          if not oc.has_memory_changed(prev_command_op_code):
            # check if op code changed the requested memory value at address
            if self.is_write_to(prev_command_address, prev_command_op_code, address, depth-1):
              return self.memory_write_value(address, prev_command_address, prev_command_op_code, depth-1, is_high_byte)
            # check for ECHO ram write
            if address <= 0xDDFF and self.is_write_to(prev_command_address, prev_command_op_code, address+0x2000, depth-1):
              return self.memory_write_value(address, prev_command_address, prev_command_op_code, depth-1, is_high_byte)
          # start of section reached, so continue with previous sections
          if prev_command_address == self.current_section.get_start_address():
            return self.memory_value_of_prev_sections(address, depth-1)
          prev_command_address = self.current_section.get_previous_command(prev_command_address)
        penalty_count += 1
      return set()

    if mr.is_echo_ram(address):
      return self.memory_value(address-0x2000, depth, current_address)
    return set()

  def get_input_values(self, depth, address, op_code):
    """
    returns the input values for that command

    Input:
    depth   = recursion depth left
    address = address of the current execution point
    op_code = command that is executed

    Output:
    value is None, if value is not used in command
    value is empty set, if value could not be found
    value contains all possible values
    m_value is the memory value, that the command uses
    a, b, c, d, e, h, l, bc, de, hl stands for the register
    """
    a_values = None
    b_values = None
    c_values = None
    d_values = None
    e_values = None
    h_values = None
    l_values = None
    m_values = None
    bc_values = None
    de_values = None
    hl_values = None
    if oc.is_register_A_input(op_code):
      if op_code in [0xAF, 0x97]: # XOR A / SUB A results allways in 0x00
        a_values = set([0x00])
      elif op_code == 0x9F: # SBC A can return 0x00 or 0xFF
        a_values = set([0x00, 0xFF])
      else:
        a_values = self.register_value_A(depth-1, address)
    if oc.is_register_B_input(op_code):
      b_values = self.register_value_B(depth-1, address)
    if oc.is_register_C_input(op_code):
      c_values = self.register_value_C(depth-1, address)
    if oc.is_register_D_input(op_code):
      d_values = self.register_value_D(depth-1, address)
    if oc.is_register_E_input(op_code):
      e_values = self.register_value_E(depth-1, address)
    if oc.is_register_H_input(op_code):
      h_values = self.register_value_H(depth-1, address)
    if oc.is_register_L_input(op_code):
      l_values = self.register_value_L(depth-1, address)
    if b_values != None and c_values != None:
      bc_values = set()
      for b in b_values:
        for c in c_values:
          bc_values.add((b<<8)|c)
    if d_values != None and e_values != None:
      de_values = set()
      for d in d_values:
        for e in e_values:
          de_values.add((d<<8)|e)
    if h_values != None and l_values != None:
      hl_values = set()
      for h in h_values:
        for l in l_values:
          hl_values.add((h<<8)|l)
    if oc.is_memory_at_BC_input(op_code):
      m_values = set()
      for bc in bc_values:
        m_values.union(self.memory_value(bc, depth-1, address))
    if oc.is_memory_at_DE_input(op_code):
      m_values = set()
      for de in de_values:
        m_values.union(self.memory_value(de, depth-1, address))
    if oc.is_memory_at_HL_input(op_code):
      m_values = set()
      for hl in hl_values:
        m_values.union(self.memory_value(hl, depth-1, address))
    return a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, bc_values, de_values, hl_values

  def move_back_until_register_changed(self, address, depth, has_register_changed_function, register_name):
    result = set()
    # continue with previous command until register is changed
    while True:
      # remember current address
      current_address = address
      
      # get new op_code
      op_code_possiblities = self.memory_value(current_address, depth-1, address)
      for op_code in op_code_possiblities:
        if has_register_changed_function(op_code):
          return op_code, current_address, result

      address = self.current_section.get_previous_command(address)
      # start of section reached, so branche to previous sections
      if address == None:
        prev_sections = self.jump_graph.get_previous_code_sections(self.current_section)
        for section in prev_sections:
          loaded_bank = self.loaded_bank
          if section.get_bank() != 0:
            loaded_bank = section.get_bank()
          bt = backtrack(self.jump_graph, loaded_bank, section.get_bank(), section.get_commands()[-1], section)
          if register_name == "A":
            result.union(bt.register_value_A(depth-1))
          if register_name == "B":
            result.union(bt.register_value_B(depth-1))
          if register_name == "C":
            result.union(bt.register_value_C(depth-1))
          if register_name == "D":
            result.union(bt.register_value_D(depth-1))
          if register_name == "E":
            result.union(bt.register_value_E(depth-1))
          if register_name == "H":
            result.union(bt.register_value_H(depth-1))
          if register_name == "L":
            result.union(bt.register_value_L(depth-1))
          if register_name == "SP":
            result.union(bt.register_value_SP(depth-1))
        return None, None, result

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_A(self, depth = 10, address = None):
    """
    Return the value of register A.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register A
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_A_changed, "A")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, _, _, _ = self.get_input_values(depth, address, op_code)

    if op_code == 0x07:
      # RLCA not implemented yet
      return set()
    if op_code == 0x0A:
      return m_values
    if op_code == 0x0F:
      # RRCA not implemented yet
      return set()
    if op_code == 0x17:
      # RLA not implemented yet
      return set()
    if op_code == 0x1A:
      return m_values
    if op_code == 0x1F:
      # RRA not implemented yet
      return set()
    if op_code == 0x27:
      # SCA not implemented yet
      return set()
    if op_code in [0x2A, 0x3A]:
      return m_values
    if op_code == 0x2F:
      # CCF not implemented yet
      return set()
    if op_code == 0x3C:
      for a in a_values:
        result.add( (a+1)%0x100 )
      return result
    if op_code == 0x3D:
      for a in a_values:
        result.add( (a-1)%0x100 )
      return result
    if op_code == 0x3E:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x78:
      return b_values
    if op_code == 0x79:
      return c_values
    if op_code == 0x7A:
      return d_values
    if op_code == 0x7B:
      return e_values
    if op_code == 0x7C:
      return h_values
    if op_code == 0x7D:
      return l_values
    if op_code == 0x7E:
      return m_values
    if op_code == 0x80:
      for a in a_values:
        for b in b_values:
          result.add( (a+b)%0x100 )
      return result
    if op_code == 0x81:
      for a in a_values:
        for c in c_values:
          result.add( (a+c)%0x100 )
      return result
    if op_code == 0x82:
      for a in a_values:
        for d in d_values:
          result.add( (a+d)%0x100 )
      return result
    if op_code == 0x83:
      for a in a_values:
        for e in e_values:
          result.add( (a+e)%0x100 )
      return result
    if op_code == 0x84:
      for a in a_values:
        for h in h_values:
          result.add( (a+h)%0x100 )
      return result
    if op_code == 0x85:
      for a in a_values:
        for l in l_values:
          result.add( (a+l)%0x100 )
      return result
    if op_code == 0x86:
      for a in a_values:
        for m in m_values:
          result.add( (a+m)%0x100 )
      return result
    if op_code == 0x87:
      for a in a_values:
        result.add( (a+a)%0x100 )
      return result
    if op_code == 0x88:
      for a in a_values:
        for b in b_values:
          result.add( (a+b)%0x100 )
          result.add( (a+b+1)%0x100 )
      return result
    if op_code == 0x89:
      for a in a_values:
        for c in c_values:
          result.add( (a+c)%0x100 )
          result.add( (a+c+1)%0x100 )
      return result
    if op_code == 0x8A:
      for a in a_values:
        for d in d_values:
          result.add( (a+d)%0x100 )
          result.add( (a+d+1)%0x100 )
      return result
    if op_code == 0x8B:
      for a in a_values:
        for e in e_values:
          result.add( (a+e)%0x100 )
          result.add( (a+e+1)%0x100 )
      return result
    if op_code == 0x8C:
      for a in a_values:
        for h in h_values:
          result.add( (a+h)%0x100 )
          result.add( (a+h+1)%0x100 )
      return result
    if op_code == 0x8D:
      for a in a_values:
        for l in l_values:
          result.add( (a+l)%0x100 )
          result.add( (a+l+1)%0x100 )
      return result
    if op_code == 0x8E:
      for a in a_values:
        for m in m_values:
          result.add( (a+m)%0x100 )
          result.add( (a+m+1)%0x100 )
      return result
    if op_code == 0x8F:
      for a in a_values:
          result.add( (a+a)%0x100 )
          result.add( (a+a+1)%0x100 )
      return result
    if op_code == 0x90:
      for a in a_values:
        for b in b_values:
          result.add( (a-b)%0x100 )
      return result
    if op_code == 0x91:
      for a in a_values:
        for c in c_values:
          result.add( (a-c)%0x100 )
      return result
    if op_code == 0x92:
      for a in a_values:
        for d in d_values:
          result.add( (a-d)%0x100 )
      return result
    if op_code == 0x93:
      for a in a_values:
        for e in e_values:
          result.add( (a-e)%0x100 )
      return result
    if op_code == 0x94:
      for a in a_values:
        for h in h_values:
          result.add( (a-h)%0x100 )
      return result
    if op_code == 0x95:
      for a in a_values:
        for l in l_values:
          result.add( (a-l)%0x100 )
      return result
    if op_code == 0x96:
      for a in a_values:
        for m in m_values:
          result.add( (a-m)%0x100 )
      return result
    if op_code == 0x97:
      return a_values # is here allready filled with 0x00
    if op_code == 0x98:
      for a in a_values:
        for b in b_values:
          result.add( (a-b)%0x100 )
          result.add( (a-b-1)%0x100 )
      return result
    if op_code == 0x99:
      for a in a_values:
        for c in c_values:
          result.add( (a-c)%0x100 )
          result.add( (a-c-1)%0x100 )
      return result
    if op_code == 0x9A:
      for a in a_values:
        for d in d_values:
          result.add( (a-d)%0x100 )
          result.add( (a-d-1)%0x100 )
      return result
    if op_code == 0x9B:
      for a in a_values:
        for e in e_values:
          result.add( (a-e)%0x100 )
          result.add( (a-e-1)%0x100 )
      return result
    if op_code == 0x9C:
      for a in a_values:
        for h in h_values:
          result.add( (a-h)%0x100 )
          result.add( (a-h-1)%0x100 )
      return result
    if op_code == 0x9D:
      for a in a_values:
        for l in l_values:
          result.add( (a-l)%0x100 )
          result.add( (a-l-1)%0x100 )
      return result
    if op_code == 0x9E:
      for a in a_values:
        for m in m_values:
          result.add( (a-m)%0x100 )
          result.add( (a-m-1)%0x100 )
      return result
    if op_code == 0x9F:
      return a_values # is here allready filled with 0x00 and 0xFF
    if op_code == 0xA0:
      for a in a_values:
        for b in b_values:
          result.add( a&b)
      return result
    if op_code == 0xA1:
      for a in a_values:
        for c in c_values:
          result.add( a&c )
      return result
    if op_code == 0xA2:
      for a in a_values:
        for d in d_values:
          result.add( a&d )
      return result
    if op_code == 0xA3:
      for a in a_values:
        for e in e_values:
          result.add( a&e )
      return result
    if op_code == 0xA4:
      for a in a_values:
        for h in h_values:
          result.add( a&h )
      return result
    if op_code == 0xA5:
      for a in a_values:
        for l in l_values:
          result.add( a&l )
      return result
    if op_code == 0xA6:
      for a in a_values:
        for m in m_values:
          result.add( a&m )
      return result
    # 0xA7 does not change A
    if op_code == 0xA8:
      for a in a_values:
        for b in b_values:
          result.add( a^b )
      return result
    if op_code == 0xA9:
      for a in a_values:
        for c in c_values:
          result.add( a^c )
      return result
    if op_code == 0xAA:
      for a in a_values:
        for d in d_values:
          result.add( a^d )
      return result
    if op_code == 0xAB:
      for a in a_values:
        for e in e_values:
          result.add( a^e )
      return result
    if op_code == 0xAC:
      for a in a_values:
        for h in h_values:
          result.add( a^h )
      return result
    if op_code == 0xAD:
      for a in a_values:
        for l in l_values:
          result.add( a^l )
      return result
    if op_code == 0xAE:
      for a in a_values:
        for m in m_values:
          result.add( a^m )
      return result
    if op_code == 0xAF:
      return a_values # is here allready filled with 0x00
    if op_code == 0xB0:
      for a in a_values:
        for b in b_values:
          result.add( a|b)
      return result
    if op_code == 0xB1:
      for a in a_values:
        for c in c_values:
          result.add( a|c )
      return result
    if op_code == 0xB2:
      for a in a_values:
        for d in d_values:
          result.add( a|d )
      return result
    if op_code == 0xB3:
      for a in a_values:
        for e in e_values:
          result.add( a|e )
      return result
    if op_code == 0xB4:
      for a in a_values:
        for h in h_values:
          result.add( a|h )
      return result
    if op_code == 0xB5:
      for a in a_values:
        for l in l_values:
          result.add( a|l )
      return result
    if op_code == 0xB6:
      for a in a_values:
        for m in m_values:
          result.add( a|m )
      return result
    # 0xB7 does not change A
    if op_code == 0xCE:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add( (a+v)%0x100 )
          result.add( (a+v+1)%0x100 ) # add with carry bit set
      return result
    if op_code == 0xCF:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add( (a+v)%0x100 )
      return result
    if op_code == 0xD6:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add( (a-v)%0x100 )
      return result
    if op_code == 0xDE:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add( (a-v)%0x100 )
          result.add( (a-v-1)%0x100 ) # sub with carry bit set
      return result
    if op_code == 0xE6:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add(a & v)
      return result
    if op_code == 0xEE:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add(a ^ v)
      return result
    if op_code == 0xF0:
      for value in self.memory_value(current_address+1, depth-1, address):
        result.union(self.memory_value(0xFF00 | value, depth-1, address))
      return result
    if op_code == 0xF1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp+1, depth-1, address, True))
      return result
    if op_code == 0xF2:
      for c in c_values:
        result.union(self.memory_value(0xFF00 | c, depth-1, address))
      return result
    if op_code == 0xF6:
      for a in a_values:
        for v in self.memory_value(current_address+1, depth-1, address):
          result.add(a | v)
      return result
    if op_code == 0xFA:
      for value in self.memory_value(current_address+1, depth-1, address):
        for v in self.memory_value(current_address+2, depth-1, address):
          result.union(self.memory_value((v << 8) | value, depth-1, address))
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_B(self, depth = 10, address = None):
    """
    Return the value of register B.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register B
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_B_changed, "B")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, bc_values, _, _ = self.get_input_values(depth, address, op_code)

    if op_code == 0x01:
      return self.memory_value(current_address+2, depth-1, address)
    if op_code == 0x03:
      for bc in bc_values:
        result.add(((bc+1) %0x10000) >> 8)
      return result
    if op_code == 0x04:
      for b in b_values:
        result.add((b+1)%0x100)
      return result
    if op_code == 0x05:
      for b in b_values:
        result.add((b-1)%0x100)
      return result
    if op_code == 0x06:
      return self.memory_value(current_address+1)
    if op_code == 0x0B:
      for bc in bc_values:
        result.add(((bc-1) %0x10000) >> 8)
      return result
    if op_code == 0x41:
      return c_values
    if op_code == 0x42:
      return d_values
    if op_code == 0x43:
      return e_values
    if op_code == 0x44:
      return h_values
    if op_code == 0x45:
      return l_values
    if op_code == 0x46:
      return m_values
    if op_code == 0x47:
      return a_values
    if op_code == 0xC1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp+1, depth-1, address, True))
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_C(self, depth = 10, address = None):
    """
    Return the value of register C.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register C
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_C_changed, "C")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, bc_values, _, _ = self.get_input_values(depth, address, op_code)

    if op_code == 0x01:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x03:
      for bc in bc_values:
        result.add(((bc+1) %0x10000) & 0xFF)
      return result
    if op_code == 0x0B:
      for bc in bc_values:
        result.add(((bc-1) %0x10000) & 0xFF)
      return result
    if op_code == 0x0C:
      for c in c_values:
        result.add((c+1)%0x100)
      return result
    if op_code == 0x0D:
      for c in c_values:
        result.add((c-1)%0x100)
      return result
    if op_code == 0x0E:
      return self.memory_value(current_address+1)
    if op_code == 0x48:
      return b_values
    if op_code == 0x4A:
      return d_values
    if op_code == 0x4B:
      return e_values
    if op_code == 0x4C:
      return h_values
    if op_code == 0x4D:
      return l_values
    if op_code == 0x4E:
      return m_values
    if op_code == 0x4F:
      return a_values
    if op_code == 0xC1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp, depth-1, address))
      return result
    return set()
      
  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_D(self, depth = 10, address = None):
    """
    Return the value of register D.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register D
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_D_changed, "D")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, _, de_values, _ = self.get_input_values(depth, address, op_code)

    if op_code == 0x11:
      return self.memory_value(current_address+2, depth-1, address)
    if op_code == 0x13:
      for de in de_values:
        result.add(((de+1) %0x10000) >> 8)
      return result
    if op_code == 0x14:
      for d in d_values:
        result.add((d+1) %0x100)
      return result
    if op_code == 0x15:
      for d in d_values:
        result.add((d-1) %0x100)
      return result
    if op_code == 0x16:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x1B:
      for de in de_values:
        result.add(((de-1) %0x10000) >> 8)
      return result
    if op_code == 0x50:
      return b_values
    if op_code == 0x51:
      return c_values
    if op_code == 0x53:
      return e_values
    if op_code == 0x54:
      return h_values
    if op_code == 0x55:
      return l_values
    if op_code == 0x56:
      return m_values
    if op_code == 0x57:
      return a_values
    if op_code == 0xD1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp, depth-1, address, True))
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_E(self, depth = 10, address = None):
    """
    Return the value of register E.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register E
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_E_changed, "E")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, _, de_values, _ = self.get_input_values(depth, address, op_code)

    if op_code == 0x11:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x13:
      for de in de_values:
        result.add(((de+1) %0x10000) & 0xFF)
      return result
    if op_code == 0x1B:
      for de in de_values:
        result.add(((de-1) %0x10000) & 0xFF)
      return result
    if op_code == 0x1C:
      for e in e_values:
        result.add((e+1) %0x100)
      return result
    if op_code == 0x1D:
      for e in e_values:
        result.add((e-1) %0x100)
      return result
    if op_code == 0x1E:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x58:
      return b_values
    if op_code == 0x59:
      return c_values
    if op_code == 0x5A:
      return d_values
    if op_code == 0x5C:
      return h_values
    if op_code == 0x5D:
      return l_values
    if op_code == 0x5E:
      return m_values
    if op_code == 0x5F:
      return a_values
    if op_code == 0xD1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp, depth-1, address))
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_H(self, depth = 10, address = None):
    """
    Return the value of register H.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register H
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address
    
    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_H_changed, "H")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, bc_values, de_values, hl_values = self.get_input_values(depth, address, op_code)

    if op_code == 0x09:
      for hl in hl_values:
        for bc in bc_values:
          result.add(((hl+bc)%1000) >> 8)
      return result
    if op_code == 0x19:
      for hl in hl_values:
        for de in de_values:
          result.add(((de+bc)%1000) >> 8)
      return result
    if op_code == 0x21:
      return self.memory_value(current_address+2, depth-1, address)
    if op_code == 0x22:
      for hl in hl_values:
        result.add(((hl+1)%0x10000) >> 8)
      return result
    if op_code == 0x23:
      for hl in hl_values:
        result.add(((hl+1)%0x10000) >> 8)
      return result
    if op_code == 0x24:
      for h in h_values:
        result.add((h+1)%0x100)
      return result
    if op_code == 0x25:
      for h in h_values:
        result.add((h-1)%0x100)
      return result
    if op_code == 0x26:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x29:
      for hl in hl_values:
        result.add(((hl+hl)%1000) >> 8)
      return result
    if op_code == 0x2A:
      for hl in hl_values:
        result.add(((hl+1)%0x10000) >> 8)
      return result
    if op_code == 0x2B:
      for hl in hl_values:
        result.add(((hl-1)%0x10000) >> 8)
      return result
    if op_code == 0x32:
      for hl in hl_values:
        result.add(((hl-1)%0x10000) >> 8)
      return result
    if op_code == 0x39:
      for hl in hl_values:
        for sp in self.register_value("SP", depth-1, address):
          result.add(((hl+sp)%1000) >> 8)
      return result
    if op_code == 0x3A:
      for hl in hl_values:
        result.add(((hl-1)%0x10000) >> 8)
      return result
    if op_code == 0x60:
      return b_values
    if op_code == 0x61:
      return c_values
    if op_code == 0x62:
      return d_values
    if op_code == 0x63:
      return e_values
    if op_code == 0x65:
      return l_values
    if op_code == 0x66:
      return m_values
    if op_code == 0x67:
      return a_values
    if op_code == 0xE1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp+1, depth-1, address, True))
      return result
    if op_code == 0xF8:
      for sp in self.register_value("SP", depth-1, address):
        for value in self.memory_value(current_address+1, depth-1, address):
          result.add(((sp+self.convert_double_complement_to_singed_int(value))%0x10000) >> 8)
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_L(self, depth = 10, address = None):
    """
    Return the value of register L.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register L
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address
    
    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_L_changed, "L")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    a_values, b_values, c_values, d_values, e_values, h_values, l_values, m_values, bc_values, de_values, hl_values = self.get_input_values(depth, address, op_code)

    if op_code == 0x09:
      for hl in hl_values:
        for bc in bc_values:
          result.add(((hl+bc)%1000) & 0xFF)
      return result
    if op_code == 0x19:
      for hl in hl_values:
        for de in de_values:
          result.add(((hl+de)%1000) & 0xFF)
      return result
    if op_code == 0x21:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x22:
      for hl in hl_values:
        result.add(((hl+1)%1000) & 0xFF)
      return result
    if op_code == 0x23:
      for hl in hl_values:
        result.add(((hl+1)%1000) & 0xFF)
      return result
    if op_code == 0x29:
      for hl in hl_values:
        result.add(((hl+hl)%1000) & 0xFF)
      return result
    if op_code == 0x2A:
      for hl in hl_values:
        result.add(((hl+1)%1000) & 0xFF)
      return result
    if op_code == 0x2B:
      for hl in hl_values:
        result.add(((hl-1)%1000) & 0xFF)
      return result
    if op_code == 0x2C:
      for l in l_values:
        result.add((l+1)%10)
      return result
    if op_code == 0x2D:
      for l in l_values:
        result.add((l-1)%10)
      return result
    if op_code == 0x2E:
      return self.memory_value(current_address+1, depth-1, address)
    if op_code == 0x32:
      for hl in hl_values:
        result.add(((hl-1)%1000) & 0xFF)
      return result
    if op_code == 0x39:
      for hl in hl_values:
        for sp in self.register_value("SP", depth-1, address):
          result.add(((hl+sp)%1000) & 0xFF)
      return result
    if op_code == 0x3A:
      for hl in hl_values:
        result.add(((hl-1)%1000) & 0xFF)
      return result
    if op_code == 0x68:
      return b_values
    if op_code == 0x69:
      return c_values
    if op_code == 0x6A:
      return d_values
    if op_code == 0x6B:
      return e_values
    if op_code == 0x6C:
      return h_values
    if op_code == 0x6E:
      return m_values
    if op_code == 0x6F:
      return a_values
    if op_code == 0xE1:
      for sp in self.register_value("SP", depth-1, address):
        result.union(self.memory_value(sp, depth-1, address))
      return result
    if op_code == 0xF8:
      for sp in self.register_value("SP", depth-1, address):
        for value in self.memory_value(current_address+1, depth-1, address):
          result.add(((sp+self.convert_double_complement_to_singed_int(value))%0x10000) & 0xFF)
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value_SP(self, depth = 10, address = None):
    """
    Return the value of register SP.

    Input:
    depth   = recusrsion depth left
    address = point of current excecution

    Output:
    set with all possible values for register SP
    set is empty if no value could be found
    """
    if depth <= 0:
      return set()

    if address == None:
      address = self.current_address

    # move so long backwards until a op code changed this register
    op_code, current_address, result = self.move_back_until_register_changed(address, depth, oc.has_register_SP_changed, "SP")
    # if values was found by visiting previous sections
    if op_code == None:
      return result

    _, _, _, _, _, _, _, _, _, _, hl_values = self.get_input_values(depth, address, op_code)

    if op_code == 0x31:
      for s in self.memory_value(current_address+2, depth-1, address):
        for p in self.memory_value(current_address+1, depth-1, address):
          result.add((s<<8) | p)
      return result
    if op_code == 0x33:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+1)%1000)
      return result
    if op_code == 0x3B:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-1)%1000)
      return result
    if op_code == 0xC0:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xC1:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xC4:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xC5:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xC7:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xC8:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xC9:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xCC:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xCD:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xCF:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xD0:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xD1:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xD4:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xD5:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xD7:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xD8:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xD9:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xDC:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xDF:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xE1:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xE5:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xE7:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xE8:
      for sp in self.register_value("SP", depth-1, address):
        for value in self.memory_value(current_address+1, depth-1, address):
          result.add((sp+self.convert_double_complement_to_singed_int(value))%0x10000)
      return result
    if op_code == 0xEF:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xF1:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp+2)%1000)
      return result
    if op_code == 0xF5:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xF7:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    if op_code == 0xF9:
      return hl_values
    if op_code == 0xFF:
      for sp in self.register_value_SP(depth-1, address):
        result.add((sp-2)%1000)
      return result
    return set()

  @lru_cache(maxsize=1024*128) # cache the result for later calls
  def register_value(self, register, depth = 10, address = None):
    """
    Returns a set of possible values of the requested register.

    Input:
    register = name of the register as string
    depth    = recusrsion depth left
    address  = point of current excecution

    Output:
    set with all possible values for register A
    set is empty if no value could be found
    set is empty if register name is wrong

    For examble:
    Input:  "A"
    Output: {0x00, 0x25, 0xFF}
    """
    if depth <= 0:
      return set()
    if address == None:
      address = self.current_address

    if register == "A":
      return self.register_value_A(depth, address = address)
    if register == "B":
      return self.register_value_B(depth, address = address)
    if register == "C":
      return self.register_value_C(depth, address = address)
    if register == "D":
      return self.register_value_D(depth, address = address)
    if register == "E":
      return self.register_value_E(depth, address = address)
    if register == "H":
      return self.register_value_H(depth, address = address)
    if register == "L":
      return self.register_value_L(depth, address = address)
    if register == "SP":
      return self.register_value_SP(depth, address = address)

    if register == "BC":
      result = set()
      for b in self.register_value_B(depth, address = address):
        for c in self.register_value_C(depth, address = address):
          result.add((b<<8) | c)
      return result
    if register == "DE":
      result = set()
      for d in self.register_value_D(depth, address = address):
        for e in self.register_value_E(depth, address = address):
          result.add((d<<8) | e)
      return result
    if register == "HL":
      result = set()
      for h in self.register_value_H(depth, address = address):
        for l in self.register_value_L(depth, address = address):
          result.add((h<<8) | l)
      return result
    return set() # if string for result is wrong

