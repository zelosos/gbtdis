#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, getopt
sys.path.append(".")

import gbtdis.op_codes as opc
import gbtdis.cartridge as cdg
from gbtdis.emulator import machine_state as mstate
from gbtdis.tracking import track
from gbtdis.jump_map import jump_graph as jg
from separation import separation as sep
from options import options as opti

def main(args):
  # parse arguments and save in options
  options = opti()
  options.parse_args(args)

  # increase recursion depth for python, so backtrack recursion depth can be reached
  sys.setrecursionlimit(1000 + options.backtrack_recursion_depth*3)

  # if no start address is provided start with default one
  if len(options.start_addresses) == 0:
    options.add_start_address(1, 0x100)

  # load rom file
  rom_image = cdg.rom()
  if not rom_image.load_rom(options.rom_file_name):
    print("ROM file could not be found :(")
    sys.exit(2)

  # track with jump graph
  graph = jg(rom_image, options)
  # use the output of sameboy to create a basic graph to build on
  if options.guided_trace_enabled:
    graph.create_graph_guided_by_sameboy()
    separation = sep(rom_image.bank_amount)
    graph.mark_program_sections(separation)
    separation.save_separation_file(options.separation_file_name)
  # explore the program automaticly
  graph.build()

  # add interrupt addresses
  if not options.vblank_interrupt_ignored:
    graph.add_new_unfollowed_address(options.loaded_bank_during_vblank_interrupt, 0x40)
  if not options.lcdstat_interrupt_ignored:
    graph.add_new_unfollowed_address(options.loaded_bank_during_lcdstat_interrupt, 0x48)
  if not options.serial_interrupt_ignored:
    graph.add_new_unfollowed_address(options.loaded_bank_during_serial_interrupt, 0x50)
  if not options.timer_interrupt_ignored:
    graph.add_new_unfollowed_address(options.loaded_bank_during_timer_interrupt, 0x58)
  if not options.joypad_interrupt_ignored:
    graph.add_new_unfollowed_address(options.loaded_bank_during_vblank_interrupt, 0x60)
  graph.build() # continue build with interrupts

  # continue trying to build graph, until manual list does not change anymore
  graph.build_until_no_change()

  # save resulted files
  if options.separation_file_output_enabled:
    separation = sep(rom_image.bank_amount)
    graph.mark_program_sections(separation)
    separation.save_separation_file(options.separation_file_name)
  if options.jumps_file_output_enabled:
    graph.save_jumps(options.jumps_file_name)
  if options.manual_file_output_enabled:
    graph.save_manual_list(options.manual_file_name)

if __name__ == "__main__":
   main(sys.argv[1:])