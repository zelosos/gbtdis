# -*- coding: utf-8 -*-

import gbtdis.op_codes as oc
from gbtdis.cartridge import rom
import hashlib

class machine_state:
  def __init__(self, rom_image):
    self.register_value = {
      "A":0x01, # Accumulator
      "F":0xB0,
      "B":0x00,
      "C":0x13,
      "D":0x00,
      "E":0xD8,
      "H":0x01,
      "L":0x4D,
      "SP":0xFFFE, # Stack Pointer
      "PC":0x0100, # Program Counter
    }

    self.interrupt_master_enable = False
    self.is_halted = False
    self.loaded_rom_bank = 0x01 # which rom bank is loaded in memory at 4000 to 7FFF
    self.rom_image = rom_image
    self.rom_ram_mode = 0x00 # upper_rom_select_bits are on rom by default, 0x01 has to be written to 0x6000-0x7FFF to select ram_bank_mode
    self.ram_enabled = 0x00
    if rom_image.get_ram_size_type() < 0x03: # ignore that type 0x00 would not have ram, and 0x01 only 2KB for simplicity reasons
      self.ram_banks = [[0x00]*0x2000]
    else:
      self.ram_banks = [[0x00]*0x2000]*4
    self.loaded_ram_bank = 0x00
    self.reset_random_read_address = 0

    # init memory with 0x00
    self.memory = list(range(0xFFFF+1))
    for i in range(0xFFFF+1):
      self.memory[i] = 0x00

    # load rom 0 into memory
    rom_bytes = self.rom_image.get_bank(0)
    for i in range(0x4000):
      self.memory[i] = rom_bytes[i]

    # load rom 1 into memory
    rom_bytes = self.rom_image.get_bank(1)
    for i in range(0x4000):
      self.memory[0x4000 + i] = rom_bytes[i]
    
    # set none zero start values
    self.memory[0xFF11] = 0xBF
    self.memory[0xFF12] = 0xF3
    self.memory[0xFF14] = 0xBF
    self.memory[0xFF16] = 0x3F
    self.memory[0xFF19] = 0xBF
    self.memory[0xFF1A] = 0x7F
    self.memory[0xFF1B] = 0xFF
    self.memory[0xFF1C] = 0x9F
    self.memory[0xFF1E] = 0xBF
    self.memory[0xFF20] = 0xFF
    self.memory[0xFF23] = 0xBF
    self.memory[0xFF24] = 0x77
    self.memory[0xFF25] = 0xF3
    self.memory[0xFF26] = 0xF1
    self.memory[0xFF40] = 0x91
    self.memory[0xFF47] = 0xFC
    self.memory[0xFF48] = 0xFF
    self.memory[0xFF49] = 0xFF

    self.register_origin = {
      "A":None, # Accumulator
      "F":None,
      "B":None,
      "C":None,
      "D":None,
      "E":None,
      "H":None,
      "L":None,
      "SP":None, # Stack Pointer
      "PC":None, # Program Counter
    }

  def get_register_values(self):
    with_register_pairs = {
      "A" : self.register_value["A"],
      "F" : self.register_value["F"],
      "B" : self.register_value["B"],
      "C" : self.register_value["C"],
      "D" : self.register_value["D"],
      "E" : self.register_value["E"],
      "H" : self.register_value["H"],
      "L" : self.register_value["L"],
      "PC" : self.register_value["PC"],
      "SP" : self.register_value["SP"],
      "AF" : (self.register_value["A"]<<8)|self.register_value["F"],
      "BC" : (self.register_value["B"]<<8)|self.register_value["C"],
      "DE" : (self.register_value["D"]<<8)|self.register_value["E"],
      "HL" : (self.register_value["H"]<<8)|self.register_value["L"]
    }
    return with_register_pairs

  def get_register_A(self):
    return self.register_value["A"]

  def get_register_F(self):
    return self.register_value["F"]

  def get_register_B(self):
    return self.register_value["B"]

  def get_register_C(self):
    return self.register_value["C"]

  def get_register_D(self):
    return self.register_value["D"]

  def get_register_E(self):
    return self.register_value["E"]

  def get_register_H(self):
    return self.register_value["H"]

  def get_register_L(self):
    return self.register_value["L"]

  def get_register_SP(self):
    return self.register_value["SP"]

  def get_register_PC(self):
    return self.register_value["PC"]

  def get_register_AF(self):
    return (self.register_value["A"] << 8) + self.register_value["F"]

  def get_register_BC(self):
    return (self.register_value["B"] << 8) + self.register_value["C"]

  def get_register_DE(self):
    return (self.register_value["D"] << 8) + self.register_value["E"]

  def get_register_HL(self):
    return (self.register_value["H"] << 8) + self.register_value["L"]

  def register_concatenated(self):
    result = self.register_value["PC"]
    result = (result << 16) | self.register_value["SP"]
    result = (result << 8) | self.register_value["L"]
    result = (result << 8) | self.register_value["H"]
    result = (result << 8) | self.register_value["E"]
    result = (result << 8) | self.register_value["D"]
    result = (result << 8) | self.register_value["C"]
    result = (result << 8) | self.register_value["B"]
    result = (result << 8) | self.register_value["F"]
    result = (result << 8) | self.register_value["A"]
    return result

  def set_zero_flag(self, value):
    # make use of binaries instead of offset calculation to speed up the computing
    if value:
      self.register_value["F"] |= 0b10000000
    else:
      self.register_value["F"] &= 0b01111111

  def set_subtract_flag(self, value):
    if value:
      self.register_value["F"] |= 0b01000000
    else:
      self.register_value["F"] &= 0b10111111

  def set_half_carry_flag(self, value):
    if value:
      self.register_value["F"] |= 0b00100000
    else:
      self.register_value["F"] &= 0b11011111

  def set_carry_flag(self, value):
    if value:
      self.register_value["F"] |= 0b00010000
    else:
      self.register_value["F"] &= 0b11101111

  def get_zero_flag(self):
    return (self.register_value["F"] & 0b10000000) > 0

  def get_subtract_flag(self):
    return (self.register_value["F"] & 0b01000000) > 0

  def get_half_carry_flag(self):
    return (self.register_value["F"] & 0b00100000) > 0

  def get_carry_flag(self):
    return (self.register_value["F"] & 0b00010000) > 0

  def set_register(self, reg, value):
    if reg == "AF":
      self.register_value["F"] = value & 0xF0 # bit 0-3 is allways 0
      self.register_value["A"] = value >> 8
      return
    if reg == "BC":
      self.register_value["C"] = value & 0xFF
      self.register_value["B"] = value >> 8
      return
    if reg == "DE":
      self.register_value["E"] = value & 0xFF
      self.register_value["D"] = value >> 8
      return
    if reg == "HL":
      self.register_value["L"] = value & 0xFF
      self.register_value["H"] = value >> 8
      return
    if not reg in self.register_value: # register name is false
      return
    if reg == "F":
      value &= 0xF0  # bit 0-3 is allways 0
    self.register_value[reg] = value
  
  def increase_pc(self, value=1):
    self.register_value["PC"] = (self.register_value["PC"] + value) % 0x10000

  def increase_sp(self, value=1):
    self.register_value["SP"] = (self.register_value["SP"] + value) % 0x10000

  def decrease_sp(self, value=1):
    self.register_value["SP"] = (self.register_value["SP"] - value) % 0x10000

  def print_register(self):
    print("AF: 0x{0:0{1}X}".format(self.get_register_AF(), 4))
    print("BC: 0x{0:0{1}X}".format(self.get_register_BC(), 4))
    print("DE: 0x{0:0{1}X}".format(self.get_register_DE(), 4))
    print("HL: 0x{0:0{1}X}".format(self.get_register_HL(), 4))
    print("PC: 0x{0:0{1}X}".format(self.register_value["PC"], 4))
    print("SP: 0x{0:0{1}X}".format(self.register_value["SP"], 4))
    print("="*10)

  def get_memory(self):
    return self.memory

  def get_memory_value(self, address):
    return self.memory[address]

  def set_memory_value(self, address, value):
    self.memory[address] = value

  def enable_master_interrupt(self):
    self.interrupt_master_enable = True
  
  def disable_master_interrupt(self):
    self.interrupt_master_enable = False

  def master_interrupt_enabled(self):
    return self.interrupt_master_enable

  def interrupt_v_blank_enabled(self):
    return self.memory[0xFFFF] & 0b00000001

  def interrupt_lcd_stat_enabled(self):
    return self.memory[0xFFFF] & 0b00000010

  def interrupt_timer_enabled(self):
    return self.memory[0xFFFF] & 0b00000100

  def interrupt_serial_enabled(self):
    return self.memory[0xFFFF] & 0b00001000

  def interrupt_joypad_enabled(self):
    return self.memory[0xFFFF] & 0b00010000

  def set_interrupt_v_blank(self):
    self.memory[0xFFFF] |= 0b00000001
  
  def set_interrupt_lcd_stat(self):
    self.memory[0xFFFF] |= 0b00000010

  def set_interrupt_timer(self):
    self.memory[0xFFFF] |= 0b00000100

  def set_interrupt_serial(self):
    self.memory[0xFFFF] |= 0b00001000

  def set_interrupt_joypad(self):
    self.memory[0xFFFF] |= 0b00010000
  
  def reset_interrupt_v_blank(self):
    self.memory[0xFFFF] &= 0b11111110
  
  def reset_interrupt_lcd_stat(self):
    self.memory[0xFFFF] &= 0b11111101

  def reset_interrupt_timer(self):
    self.memory[0xFFFF] &= 0b11111011

  def reset_interrupt_serial(self):
    self.memory[0xFFFF] &= 0b11110111

  def reset_interrupt_joypad(self):
    self.memory[0xFFFF] &= 0b11101111

  def is_in_halt(self):
    return self.is_halted
  
  def halt_state(self):
    self.is_halted = True
  
  def continue_state(self):
    self.is_halted = False

  def get_rom_image(self):
    return self.rom_image

  def set_rom_image(self, rom_image):
    self.rom_image = rom_image

  def load_rom_bank(self, bank_number):
    # MBC1 do not use 0x20, 0x40, 0x60
    # isntead 0x21, 0x41, 0x61 is used
    if self.rom_image.get_size_type() in [0x05, 0x06] and bank_number in [0x20, 0x40, 0x60]:
      bank_number += 1
    if bank_number == 0x00:
      bank_number = 0x01
    self.loaded_rom_bank = bank_number
    # get bytes from rom image
    rom_bytes = self.rom_image.get_bank(bank_number)
    # copy bytes into memory (0x4000-0x7FFF)
    for i in range(0x4000):
      self.memory[0x4000+i] = rom_bytes[i]

  def get_loaded_rom_bank_number(self):
    return self.loaded_rom_bank

  def get_loaded_ram_bank_number(self):
    return self.loaded_ram_bank

  def set_rom_ram_mode(self, value):
    if value == 0:
      self.rom_ram_mode = 0x00
    else:
      self.rom_ram_mode = 0x01

  def is_in_rom_mode(self):
    return self.rom_ram_mode == 0x00

  def enable_ram(self, enable):
    self.ram_enabled = enable

  def is_ram_enabled(self):
    return self.ram_enabled > 0

  def load_ram_bank(self, bank_number):
    if self.rom_image.get_ram_size_type() < 0x03: # no change if only one bank exsists
      return
    
    # save content to ram bank
    for i in range(0x2000):
      self.ram_banks[self.loaded_ram_bank] = self.memory[0xA000+i]
    # change bank
    self.loaded_ram_bank = bank_number
    # load new content from ram bank
    for i in range(0x2000):
      self.memory[0xA000+i] = self.ram_banks[self.loaded_ram_bank]

  def get_next_op_code(self):
    op_code = self.get_memory_value(self.register_value["PC"])
    op_length = oc.op_codes[op_code]["length"]
    op_code_parameter = 0x00
    if op_length >= 2:
      op_code_parameter = self.get_memory_value((self.register_value["PC"] +1) % 0x10000)
    if op_length == 3:
      op_code_parameter |= (self.get_memory_value((self.register_value["PC"] +2) % 0x10000)) << 8
    return op_code, op_length, op_code_parameter

  def hash(self):
    # new hash
    hash_sum = hashlib.sha256()

    # add register to hash
    hash_sum.update(bytes([self.register_value["A"]]))
    hash_sum.update(bytes([self.register_value["F"]]))
    hash_sum.update(bytes([self.register_value["B"]]))
    hash_sum.update(bytes([self.register_value["C"]]))
    hash_sum.update(bytes([self.register_value["D"]]))
    hash_sum.update(bytes([self.register_value["E"]]))
    hash_sum.update(bytes([self.register_value["H"]]))
    hash_sum.update(bytes([self.register_value["L"]]))
    hash_sum.update(bytes([self.register_value["SP"]&0xFF]))
    hash_sum.update(bytes([self.register_value["SP"]>>8]))
    hash_sum.update(bytes([self.register_value["PC"]&0xFF]))
    hash_sum.update(bytes([self.register_value["PC"]>>8]))

    # add special flags to hash
    hash_sum.update(bytes([self.interrupt_master_enable]))
    hash_sum.update(bytes([self.is_halted]))

    # add loaded banks to hash
    hash_sum.update(bytes([self.loaded_rom_bank]))
    hash_sum.update(bytes([self.loaded_ram_bank]))
    hash_sum.update(bytes([self.rom_ram_mode]))
    hash_sum.update(bytes([self.ram_enabled]))

    for bank in self.ram_banks:
      hash_sum.update(bytes(bank))
    hash_sum.update(bytes(self.memory[0x8000:])) # ignore ROM, ROM Bank is factored in by selected rom bank value
    return hash_sum.digest()

  def set_reset_random_read(self, address):
    """
    remember to reset the given memory value at address
    """
    self.reset_random_read_address = address

  def get_reset_random_read(self):
    return self.reset_random_read_address

class memory_region:
  @staticmethod
  def is_in_limits(address, lower_limit, upper_limit):
    return address >= lower_limit and address <= upper_limit

  @staticmethod
  def is_interrupt_vector(address):
    return memory_region.is_in_limits(address, 0x0000, 0x00FF)

  @staticmethod
  def is_cartridge_header(address):
    return memory_region.is_in_limits(address, 0x0100, 0x014F)

  @staticmethod
  def is_rom_bank_0(address):
    return memory_region.is_in_limits(address, 0x0000, 0x3FFF)

  @staticmethod
  def is_rom_bank_swapable(address):
    return memory_region.is_in_limits(address, 0x4000, 0x7FFF)

  @staticmethod
  def is_rom(address):
    return memory_region.is_rom_bank_0(address) or memory_region.is_rom_bank_swapable(address)

  @staticmethod
  def is_enable_ram(address):
    return memory_region.is_in_limits(address, 0x1000, 0x1FFF)

  @staticmethod
  def is_rom_select_lower_bits(address):
    return memory_region.is_in_limits(address, 0x2000, 0x3FFF)

  @staticmethod
  def is_rom_select_upper_bits(address):
    return memory_region.is_in_limits(address, 0x4000, 0x5FFF)

  @staticmethod
  def is_rom_ram_select_mode(address):
    return memory_region.is_in_limits(address, 0x6000, 0x7FFF)

  @staticmethod
  def is_video_ram(address):
    return memory_region.is_in_limits(address, 0x8000, 0x9FFF)
  
  @staticmethod
  def is_cartridge_ram(address):
    return memory_region.is_in_limits(address, 0xA000, 0xBFFF)

  @staticmethod
  def is_internal_ram_bank_0(address):
    return memory_region.is_in_limits(address, 0xC000, 0xCFFF)

  @staticmethod
  def is_internal_ram_bank_swapable(address):
    return memory_region.is_in_limits(address, 0xD000, 0xDFFF)

  @staticmethod
  def is_echo_ram(address):
    # Mirror of C000~DDFF
    return memory_region.is_in_limits(address, 0xE000, 0xFDFF)

  @staticmethod
  def is_object_attribute_memory(address):
    # Sprite Attribute Memory
    return memory_region.is_in_limits(address, 0xFE00, 0xFE9F)

  @staticmethod
  def is_unusable_memory(address):
    return memory_region.is_in_limits(address, 0xFEA0, 0xFEFF)

  @staticmethod
  def is_hardware_io_register(address):
    return memory_region.is_in_limits(address, 0xFF00, 0xFF7F)

  @staticmethod
  def is_high_ram(address):
    # zero page ram
    return memory_region.is_in_limits(address, 0xFF80, 0xFFFE)

  @staticmethod
  def is_interrupt_enable_flag(address):
    return memory_region.is_in_limits(address, 0xFFFF, 0xFFFF)

  @staticmethod
  def is_random_read(address):
    random_read_addresses = [
      0xFF03, 0xFF08, 0xFF09, 0xFF0A, 0xFF0B, 0xFF0C, 0xFF0D, 0xFF0E,
      0xFF15, 0xFF1F,
      0xFF27, 0xFF28, 0xFF29, 0xFF2A, 0xFF2B, 0xFF2C, 0xFF2D, 0xFF2E, 0xFF2F,
      0xFF4C, 0xFF4D, 0xFF4E, 0xFF4F
    ]
    return address in random_read_addresses or memory_region.is_in_limits(address, 0xFFA0, 0xFEFF)

class command:
  @staticmethod
  def NOP(state, op_code_length):
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def load_value_into_register(state, op_code_length, value, register_name):
    state.set_register(register_name, value)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def load_memory_into_register(state, op_code_length, ram_address, register_name, post_execution=None):
    value = state.get_memory_value(ram_address)
    # read 0x00 from drisabled ram
    if memory_region.is_cartridge_ram(ram_address) and not state.ram_enabled():
      value = 0x00
    # set value
    state.set_register(register_name, value)

    # if memory value read is random value, increase value, to get a new "random" value on next read
    if (memory_region.is_random_read(ram_address)):
      state.set_memory_value(ram_address, value +1)

    # increment/decrement HL if needed
    if post_execution == "increment":
      command.add(state, 0, "HL", 1)
    elif post_execution == "decrement":
      command.subtract(state, 0, "HL", 1)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def load_register_into_memory(state, op_code_length, register_name, ram_address, post_execution=None):
    state = command.load_value_into_memory(state, op_code_length, state.get_register_values()[register_name]&0xFF, ram_address)
    if register_name == "SP":
      state = command.load_value_into_memory(state, 0, state.get_register_values()[register_name]>>8, ram_address+1)
    if post_execution == "increment":
      command.add(state, 0, "HL", 1)
    elif post_execution == "decrement":
      command.subtract(state, 0, "HL", 1)
    return state

  @staticmethod
  def load_value_into_memory(state, op_code_length, value, ram_address):
    bank = state.get_loaded_rom_bank_number()
    state.increase_pc(op_code_length)

    # write into rom is bank change?
    # select lower rom bits
    if memory_region.is_rom_select_lower_bits(ram_address):
      if state.get_rom_image().get_bank_controller_type() in ["MBC1", "HuC1", "HuC3"]: # not much known about HuC, but it is speculated, that it behaves like MBC1
        bank = (bank & 0b11100000) | (value & 0b00011111)
        if bank == 0x00:
          bank = 0x01
      elif state.get_rom_image().get_bank_controller_type() == "MBC2" and (ram_address & 0x0100): # bit 8 of address has to be set to change bank
        bank = (bank & 0xF0) | (value & 0x0F)
      elif state.get_rom_image().get_bank_controller_type() == "MBC3":
        bank = (bank & 0b10000000) | (value & 0b01111111)
        if bank == 0x00:
          bank = 0x01
      state.load_rom_bank(bank)
      return state

    # select upper bit for rom or ram change
    if memory_region.is_rom_select_upper_bits(ram_address):
      if state.is_in_rom_mode(): # rom mode
        if state.get_rom_image().get_bank_controller_type() in ["MBC1", "HuC1", "HuC3"]: # MBC1 change upper bits
          bank = (bank & 0b000111111) | (value & 0b01100000)
          state.load_rom_bank(bank)
      else: # ram mode
        state.load_ram_bank(value & 0x00000011)
      return state

    # toggle enable/disable ram
    if memory_region.is_enable_ram(ram_address):
      if value & 0x0F == 0x0A:
        state.enable_ram(not state.is_ram_enabled)
      return state

    # switch between ram and rom mode for upper bits
    if memory_region.is_rom_ram_select_mode(ram_address):
      if state.get_rom_image().get_bank_controller_type() in ["MBC1", "HuC1", "HuC3"]: # not much known about HuC, but it is speculated, that it behaves like MBC1
        state.enable_ram(value > 0)
      # TODO implement RTC for MBC3
      return state

    # writing into rom
    if memory_region.is_rom(ram_address):
      return state

    # write into video_ram
    if memory_region.is_video_ram(ram_address):
      state.set_memory_value(ram_address, value)
      return state

    # write to external cartridge ram
    if memory_region.is_cartridge_ram(ram_address):
      state.set_memory_value(ram_address, value)
      return state

    # write to internal ram bank 0
    if memory_region.is_internal_ram_bank_0(ram_address):
      state.set_memory_value(ram_address, value)
      if ram_address < 0xDE00:
        state.set_memory_value(ram_address+0x2000, value) # also write a copy to Echo RAM 0xE000 - 0xFDFF
      return state

    # write to internal ram bank 1-7
    if memory_region.is_internal_ram_bank_swapable(ram_address):
      state.set_memory_value(ram_address, value)
      return state

    # write to echo ram
    if memory_region.is_echo_ram(ram_address):
      state.set_memory_value(ram_address, value)
      state.set_memory_value(ram_address-0x2000, value) # also write a copy to 0xC000 - 0xDDFF
      return state

    # write to oam
    if memory_region.is_object_attribute_memory(ram_address):
      state.set_memory_value(ram_address, value)
      return state

    # write to invalide memory space
    if memory_region.is_unusable_memory(ram_address):
      return None

    # write to hardware io
    if memory_region.is_hardware_io_register(ram_address):
      # is joypad
      if ram_address == 0xFF00:
        state.set_memory_value(ram_address, value & 0b00110000) # only write select bits

      # is serial byte
      elif ram_address == 0xFF01:
        state.set_memory_value(ram_address, value)

      # is serial control
      elif ram_address == 0xFF02:
        state.set_memory_value(ram_address, value | 0b11111100) # only 2 lower bits are set

      # is clock divider
      elif ram_address == 0xFF04:
        state.set_memory_value(ram_address, 0) # write to clock devider allways sets it to 0

      # is timer value
      elif ram_address == 0xFF05:
        state.set_memory_value(ram_address, value) # TODO not specified by a doku, what to do by write

      # is timer reload
      elif ram_address == 0xFF06:
        state.set_memory_value(ram_address, value)

      # is timer control
      elif ram_address == 0xFF07:
        state.set_memory_value(ram_address, value | 0b11111000) # only 3 lower bits are set

      # is interrupts asserted
      elif ram_address == 0xFF0F:
        state.set_memory_value(ram_address, value | 0b11100000)

      # is audio
      elif memory_region.is_in_limits(ram_address, 0xFF10, 0xFF3F):
        # audio is not played, so just write into memory
        state.set_memory_value(ram_address, value)

      # is lcd control
      elif ram_address == 0xFF40:
        state.set_memory_value(ram_address, value) # value effects redering of image, so effects can be ignored here

      # is lcd status
      elif ram_address == 0xFF41:
        """
        0xFF41 is ment for read only.
        writing is possible, but triggers interrupts by side effects.
        Read more under the topic of "STAT writing IRQ"
        """
        state.set_memory_value(ram_address, value)

      # is background vertical scroll
      elif ram_address == 0xFF42:
        # this effects only the display, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is background horizontal scroll
      elif ram_address == 0xFF43:
        # this effects only the display, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is lcd Y coordinate
      elif ram_address == 0xFF44:
        # indicates the y coordinate of procces of the lcd
        # effects of writing are ignored here
        state.set_memory_value(ram_address, value)

      # is lcd y compare
      elif ram_address == 0xFF45:
        # defines the compare value agains y coordinate of lcd
        state.set_memory_value(ram_address, value)

      # is oam dma source address
      elif ram_address == 0xFF46:
        # start the oam dma transfer
        # copy 0xZZ00-0xZZ9F into 0xFE00-0xFE9F with value as ZZ
        upper = value << 8
        for i in range(0xA0):
          state.set_memory_value(0xFE00 | i, state.get_memory_value(upper | i))

      # is background palette
      elif ram_address == 0xFF47:
        # this effects only the display colors, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is obj 0 palette
      elif ram_address == 0xFF48:
        # this effects only the display colors, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is obj 1 palette
      elif ram_address == 0xFF49:
        # this effects only the display colors, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is window y coordinate
      elif ram_address == 0xFF4A:
        # this effects only the window of the display, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # is window x coordinate
      elif ram_address == 0xFF4B:
        # this effects only the window of the display, so the effect can be ignored here
        state.set_memory_value(ram_address, value)

      # end of write to hardware io
      return state

    # write to HRAM
    if memory_region.is_high_ram(ram_address):
      state.set_memory_value(ram_address, value)
      return state

    # write to interrupt flags
    if memory_region.is_interrupt_enable_flag(ram_address):
      # enable interrupts
      state.set_memory_value(ram_address, value | 0b11100000) # only bit 0-4 is effected

      # set flag for v blank
      if value & 0b00000001:
        state.set_interrupt_v_blank()
      else:
        state.reset_interrupt_v_blank()
        
      # set flag for lcd stat
      if value & 0b00000010:
        state.set_interrupt_lcd_stat()
      else:
        state.reset_interrupt_lcd_stat()

      # set flag for timer
      if value & 0b00000100:
        state.set_interrupt_timer()
      else:
        state.reset_interrupt_timer()

      # set flag for serial
      if value & 0b00001000:
        state.set_interrupt_serial()
      else:
        state.reset_interrupt_serial()

      # set flag for joypad
      if value & 0b00001000:
        state.set_interrupt_joypad()
      else:
        state.reset_interrupt_joypad()

      return state

    # all regions should be covert, so print a message to fix this code
    print("memory area in load_value_into_memory() was not covert. address: 0x{0:0{1}X}".format(ram_address, 4))
    return None

  @staticmethod
  def rotate_left(state, op_code_length, register_name, rotate_amount, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    """
        +-----------+
        |           |
    C<--+--[7..0]<--+
    """
    value = state.get_register_values()[register_name]
    # single register or register pair
    if register_name in ["A", "F", "B", "C", "D", "E", "H", "L"]:
      length = 8
      rotate_amount %= 8
    else:
      length = 16
      rotate_amount %= 16
    # shift left
    new_value = (value << rotate_amount) & (0xFF << rotate_amount)
    # wrap around
    new_value |= value >> (length-rotate_amount)
    state.set_register(register_name, new_value)

    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    # set carry flag
    state.set_carry_flag((new_value & 0x00000001) > 0)

    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def rotate_left_through_carry(state, op_code_length, register_name, rotate_amount, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    """
    +---------------+
    |               |
    +-C<---[7..0]<--+
    """
    value = state.get_register_values()[register_name]
    # single register or register pair
    if register_name in ["A", "F", "B", "C", "D", "E", "H", "L"]:
      length = 8
      rotate_amount %= 9
      mask = 0xFF
    else:
      length = 16
      rotate_amount %= 17
      mask = 0xFFFF
    # shift left
    new_value = (value << rotate_amount) & (0xFF << rotate_amount)
    # wrap around
    new_value |= (value | (state.get_carry_flag()<<8)) >> (length+1-rotate_amount)
    new_value &= mask
    state.set_register(register_name, new_value)

    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    # set carry flag
    state.set_carry_flag((value<<rotate_amount) & (1 << length))

    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def rotate_right(state, op_code_length, register_name, rotate_amount, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    """
    +-----------+
    |           |
    +-->[7..0]--+-->C
    """
    value = state.get_register_values()[register_name]
    rotate_amount %= 8
    # shift right
    new_value = (value >> rotate_amount) & (0xFF >> rotate_amount)
    # wrap around
    new_value |= value << (8-rotate_amount)
    if register_name == "M":
      state.set_memory_value(state.get_register_HL(), new_value)
    else:
      state.set_register(register_name, new_value)

    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    # set carry flag
    state.set_carry_flag((new_value & 0b10000000) > 0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def rotate_right_through_carry(state, op_code_length, register_name, rotate_amount, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    """
    +---------------+
    |               |
    +-->[7..0]--->C-+
    """
    value = state.get_register_values()[register_name]
    # single register or register pair
    rotate_amount %= 9
    # shift right
    new_value = (value >> rotate_amount) & (0xFF >> rotate_amount)
    # wrap around
    new_value |= (((value << 1) | state.get_carry_flag()) << (8-rotate_amount))&0xFF
    if register_name == "M":
      state.set_memory_value(state.get_register_HL(), new_value)
    else:
      state.set_register(register_name, new_value)

    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    # set carry flag
    state.set_carry_flag((value>>rotate_amount-1) & 1)
    
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def shift_left_arythmetic(state, op_code_length, register_name):
    """
    C<--[7..0]<--0
    """
    if register_name == "M":
      value = state.get_memory_value()[state.get_register_HL()]
      state.set_memory_value(state.get_register_HL(), value << 1)
    else:
      value = state.get_register_values()[register_name]
      state.set_register(register_name, value << 1)

    state.set_zero_flag((value << 1) == 0)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(0)
    state.set_carry_flag(value & 0x80)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def shift_right_arythmetic(state, op_code_length, register_name):
    """
    [7]-->[7..0]-->C
    """
    if register_name == "M":
      value = state.get_memory_value()[state.get_register_HL()]
      state.set_memory_value(state.get_register_HL(), (value >> 1) | (value & 0x80))
    else:
      value = state.get_register_values()[register_name]
      state.set_register(register_name, (value >> 1) | (value & 0x80))

    state.set_zero_flag((value >> 1) | (value & 0x80) == 0)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(0)
    state.set_carry_flag(value & 0x01)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def shift_right_logically(state, op_code_length, register_name):
    """
    0-->[7..0]-->C
    """
    if register_name == "M":
      value = state.get_memory_value()[state.get_register_HL()]
      state.set_memory_value(state.get_register_HL(), value >> 1)
    else:
      value = state.get_register_values()[register_name]
      state.set_register(register_name, value >> 1)

    state.set_zero_flag((value >> 1) == 0)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(0)
    state.set_carry_flag(value & 0x01)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def set_fixed_flags(state, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    if zero_flag == "0":
      state.set_zero_flag(False)
    elif zero_flag == "1":
      state.set_zero_flag(True)
    if subtract_flag == "0":
      state.set_subtract_flag(False)
    elif subtract_flag == "1":
      state.set_subtract_flag(True)
    if half_carry_flag == "0":
      state.set_half_carry_flag(False)
    elif half_carry_flag == "1":
      state.set_half_carry_flag(True)
    if carry_flag == "0":
      state.set_carry_flag(False)
    elif carry_flag == "1":
      state.set_carry_flag(True)

  @staticmethod
  def add(state, op_code_length, register_name, value, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    register_value = state.get_register_values()[register_name]
    result = register_value + value
    if register_name in ["A", "F", "B", "C", "D", "E", "H", "L"]:
      if zero_flag == "Z":
        state.set_zero_flag(result%0x100 == 0x00)
      if carry_flag == "C":
        state.set_carry_flag(result > 0xFF)
      if half_carry_flag == "H":
        state.set_half_carry_flag((((register_value&0x0F) + (value&0x0F)) & 0xF0) > 0)
      state.set_register(register_name, result % 0x100)
    else:
      if zero_flag == "Z":
        state.set_zero_flag(result%0x10000 == 0x0000)
      if carry_flag == "C":
        state.set_carry_flag(result > 0xFFFF)
      if half_carry_flag == "H":
        state.set_half_carry_flag((((register_value&0x0F) + (value&0x0F)) & 0xF0) > 0)
      state.set_register(register_name, result % 0x10000)

    # set flags to fixed values if op code demands it
    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def add_with_carry(state, op_code_length, register_name, value, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    # add carry as well
    if state.get_carry_flag():
      value += 1
    return command.add(state, op_code_length, register_name, value, zero_flag, subtract_flag, half_carry_flag, carry_flag)

  @staticmethod
  def add_value_to_memory(state, op_code_length, value, memory_address, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    memory_value = state.get_memory_value(memory_address)
    result = memory_value + value
    if zero_flag == "Z":
      state.set_zero_flag(result%0x100 == 0)
    if carry_flag == "C":
      state.set_carry_flag(result > 0xFF)
    if half_carry_flag == "H":
      state.set_half_carry_flag(((memory_value&0x0F) + (value&0x0F)) & 0xF0)
    command.load_value_into_memory(state, 0, result % 0x100, memory_address)
    
    # set flags to fixed values if op code demands it
    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def subtract(state, op_code_length, register_name, value, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    register_value = state.get_register_values()[register_name]
    result = register_value - value
    if register_name in ["A", "F", "B", "C", "D", "E", "H", "L"]:
      if zero_flag == "Z":
        state.set_zero_flag(result%0x100 == 0)
      if carry_flag == "C":
        state.set_carry_flag(result < 0)
      if half_carry_flag == "H":
        state.set_half_carry_flag((((register_value&0x0F) - (value&0x0F)) & 0xF0) > 0)
      state.set_register(register_name, result % 0x100)
    else:
      if zero_flag == "Z":
        state.set_zero_flag(result%0x10000 == 0)
      if carry_flag == "C":
        state.set_carry_flag(result < 0)
      if half_carry_flag == "H":
        state.set_half_carry_flag((((register_value&0x0F) - (value&0x0F)) & 0xF0) > 0)
      state.set_register(register_name, result % 0x10000)

    # set flags to fixed values if op code demands it
    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def subtract_with_carry(state, op_code_length, register_name, value, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    # subtract carry as well
    if state.get_carry_flag():
      value += 1
    return command.subtract(state, op_code_length, register_name, value, zero_flag, subtract_flag, half_carry_flag, carry_flag)

  @staticmethod
  def subtract_value_from_memory(state, op_code_length, value, memory_address, zero_flag="-", subtract_flag="-", half_carry_flag="-", carry_flag="-"):
    memory_value = state.get_memory_value(memory_address)
    result = memory_value - value
    if zero_flag == "Z":
      state.set_zero_flag(result%0x100 == 0)
    if carry_flag == "C":
      state.set_carry_flag(result < 0x00)
    if half_carry_flag == "H":
      state.set_half_carry_flag(((memory_value&0x0F) - (value&0x0F)) < 0x00)
    command.load_value_into_memory(state, 0, result % 0x100, memory_address)
    
    # set flags to fixed values if op code demands it
    command.set_fixed_flags(state, zero_flag, subtract_flag, half_carry_flag, carry_flag)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def and_value(state, op_code_length, value):
    result = state.get_register_A() & value
    state.set_register("A", result)
    # set flags
    state.set_zero_flag(not result)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(1)
    state.set_carry_flag(0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def or_value(state, op_code_length, value):
    result = state.get_register_A() | value
    state.set_register("A", result)
    # set flags
    state.set_zero_flag(not result)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(0)
    state.set_carry_flag(0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def xor_value(state, op_code_length, value, is_complement=False):
    result = state.get_register_A() ^ value
    state.set_register("A", result)
    # set flags
    if is_complement:
      state.set_subtract_flag(1)
      state.set_half_carry_flag(1)
    else:
      state.set_zero_flag(not result)
      state.set_subtract_flag(0)
      state.set_half_carry_flag(0)
      state.set_carry_flag(0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def compare_value(state, op_code_length, value):
    reg_A = state.get_register_A()
    result = reg_A - value
    state.set_zero_flag(result%0x100 == 0)
    state.set_subtract_flag(True)
    state.set_half_carry_flag((reg_A & 0x0F) - (value & 0x0F) < 0x00)
    state.set_carry_flag(result < 0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def complement_carry_flag(state, op_code_length):
    state.set_carry_flag(state.get_carry_flag() ^ 1)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def set_carry_flag(state, op_code_length):
    state.set_carry_flag(1)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def test_bit(state, op_code_length, register_name, position):
    if register_name == "M":
      state.set_zero_flag(state.get_memory_value(state.get_register_HL()) & (1<<position))  
    else:
      state.set_zero_flag(state.get_register_values[register_name] & (1<<position))
    state.set_subtract_flag(0)
    state.set_half_carry_flag(1)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def reset_bit(state, op_code_length, register_name, position):
    if register_name == "M":
      state.set_memory_value(state.get_register_HL(), state.get_memory_value(state.get_register_HL()) & ~(1<<position))
    else:
      state.set_register(register_name, state.get_register_values[register_name] & ~(1<<position))
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def set_bit(state, op_code_length, register_name, position):
    if register_name == "M":
      state.set_memory_value(state.get_register_HL(), state.get_memory_value(state.get_register_HL()) | (1<<position))
    else:
      state.set_register(register_name, state.get_register_values[register_name] | (1<<position))
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def exchange_nibble(state, op_code_length, register_name):
    if register_name == "M":
      value = state.get_memory_value(state.get_register_HL())
      new_value = ((value & 0xF0) >> 4) | ((value & 0x0F) << 4)
      state.set_memory_value(state.get_register_HL(), new_value)
    else:
      value = state.get_register_values()[register_name]
      new_value = ((value & 0xF0)  >> 4) | ((value & 0x0F) << 4)
      state.set_register(register_name, new_value)
    state.set_zero_flag(new_value == 0)
    state.set_subtract_flag(0)
    state.set_half_carry_flag(0)
    state.set_carry_flag(0)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def push_value(state, op_code_length, value):
    state.decrease_sp(2)
    state.set_memory_value(state.get_register_SP(), value)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def pop_into_register(state, op_code_length, register_name):
    state.set_register(register_name, state.get_memory_value(state.get_register_SP()))
    state.increase_sp(2)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def execute_cb(state, op_code_length):
    next_op_code = state.get_memory_value(state.get_register_PC()+1)
    return command.excute(next_op_code, state, is_cb_extension=True)

  @staticmethod
  def jump_to_address(state, op_code_length, memory_address, condition, is_relative=False):
    if is_relative:
      # jump forward or backwards implemented as two complement
      if memory_address >= (1<<7):
        memory_address -= 1<<8
      memory_address = state.get_register_PC() + op_code_length + memory_address
    if condition:
      state.set_register("PC", memory_address)
    else:
      state.increase_pc(op_code_length)
    return state

  @staticmethod
  def call(state, op_code_length, call_address, condition):
    state.increase_pc(op_code_length)
    if condition:
      state.decrease_sp(2)
      state.set_memory_value(state.get_register_SP(), state.get_register_PC)
      state.set_register("PC", call_address)
    return state

  @staticmethod
  def return_from_call(state, op_code_length, condition, enable_interrupt=False):
    state.set_register("PC", state.get_memory_value(state.get_register_SP()))
    state.increase_sp(2)
    if enable_interrupt:
      state.enable_master_interrupt()
    return state

  @staticmethod
  def interrupt(state, op_code_length, enable):
    if enable:
      print(state.get_register_PC())
      state.enable_master_interrupt()
    else:
      state.disable_master_interrupt()
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def halt(state, op_code_length):
    state.increase_pc(op_code_length)
    state.halt_state()
    return state

  @staticmethod
  def decimal_adjust_akku(state, op_code_length):
    value = state.get_register_A()
    if not state.get_subtract_flag():
      if state.get_carry_flag() or value > 0x99:
        value += 0x60
        state.set_carry_flag(1)
      if state.get_half_carry_flag() or (value & 0x0F) > 0x09:
        value += 0x06
        state.set_half_carry_flag(0)
    elif state.get_carry_flag() and state.get_half_carry_flag():
      value += 0x9A
      state.set_half_carry_flag(0)
    elif state.get_carry_flag():
      value += 0xA0
    elif state.get_half_carry_flag():
      value += 0xFA
      state.set_half_carry_flag(0)
    state.set_zero_flag(value == 0)
    state.set_register("A", value)
    state.increase_pc(op_code_length)
    return state

  @staticmethod
  def excute(op_code, state, parameter=0x00, is_cb_extension=False):
    # the solution with the dictionary is not the most beatifull -> specialy the arguments have to be a list
    # but it prevents a lot of if op_code == 0x__: and reduces therefor the needed space
    # so this solution is the one who provides the most overview of what is going on

    # add length and state to arguments
    if is_cb_extension:
      execute_op_code_cb = {
        0x00: [command.rotate_left, ["B", 1, "Z", "0", "0", "C"]], # RLC B 
        0x01: [command.rotate_left, ["C", 1, "Z", "0", "0", "C"]], # RLC C
        0x02: [command.rotate_left, ["D", 1, "Z", "0", "0", "C"]], # RLC D
        0x03: [command.rotate_left, ["E", 1, "Z", "0", "0", "C"]], # RLC E
        0x04: [command.rotate_left, ["H", 1, "Z", "0", "0", "C"]], # RLC H
        0x05: [command.rotate_left, ["L", 1, "Z", "0", "0", "C"]], # RLC L
        0x06: [command.rotate_left, ["M", 1, "Z", "0", "0", "C"]], # RLC (HL)
        0x07: [command.rotate_left, ["A", 1, "Z", "0", "0", "C"]], # RLC A
        0x08: [command.rotate_right, ["B", 1, "Z", "0", "0", "C"]], # RRC B
        0x09: [command.rotate_right, ["C", 1, "Z", "0", "0", "C"]], # RRC C
        0x0A: [command.rotate_right, ["D", 1, "Z", "0", "0", "C"]], # RRC D
        0x0B: [command.rotate_right, ["E", 1, "Z", "0", "0", "C"]], # RRC E
        0x0C: [command.rotate_right, ["H", 1, "Z", "0", "0", "C"]], # RRC H
        0x0D: [command.rotate_right, ["L", 1, "Z", "0", "0", "C"]], # RRC L
        0x0E: [command.rotate_right, ["M", 1, "Z", "0", "0", "C"]], # RRC (HL)
        0x0F: [command.rotate_right, ["A", 1, "Z", "0", "0", "C"]], # RRC A
        0x10: [command.rotate_left_through_carry, ["B", 1, "Z", "0", "0", "C"]], # RL B
        0x11: [command.rotate_left_through_carry, ["C", 1, "Z", "0", "0", "C"]], # RL C
        0x12: [command.rotate_left_through_carry, ["D", 1, "Z", "0", "0", "C"]], # RL D
        0x13: [command.rotate_left_through_carry, ["E", 1, "Z", "0", "0", "C"]], # RL E
        0x14: [command.rotate_left_through_carry, ["H", 1, "Z", "0", "0", "C"]], # RL H
        0x15: [command.rotate_left_through_carry, ["L", 1, "Z", "0", "0", "C"]], # RL L
        0x16: [command.rotate_left_through_carry, ["M", 1, "Z", "0", "0", "C"]], # RL (HL)
        0x17: [command.rotate_left_through_carry, ["A", 1, "Z", "0", "0", "C"]], # RL A
        0x18: [command.rotate_right_through_carry, ["B", 1, "Z", "0", "0", "C"]], #RR B
        0x19: [command.rotate_right_through_carry, ["C", 1, "Z", "0", "0", "C"]], #RR C
        0x1A: [command.rotate_right_through_carry, ["D", 1, "Z", "0", "0", "C"]], #RR D
        0x1B: [command.rotate_right_through_carry, ["E", 1, "Z", "0", "0", "C"]], #RR E
        0x1C: [command.rotate_right_through_carry, ["H", 1, "Z", "0", "0", "C"]], #RR H
        0x1D: [command.rotate_right_through_carry, ["L", 1, "Z", "0", "0", "C"]], #RR L
        0x1E: [command.rotate_right_through_carry, ["M", 1, "Z", "0", "0", "C"]], #RR (HL)
        0x1F: [command.rotate_right_through_carry, ["A", 1, "Z", "0", "0", "C"]], #RR A
        0x20: [command.shift_left_arythmetic, ["B"]], # SLA B
        0x21: [command.shift_left_arythmetic, ["C"]], # SLA C
        0x22: [command.shift_left_arythmetic, ["D"]], # SLA D
        0x23: [command.shift_left_arythmetic, ["E"]], # SLA E
        0x24: [command.shift_left_arythmetic, ["H"]], # SLA H
        0x25: [command.shift_left_arythmetic, ["L"]], # SLA L
        0x26: [command.shift_left_arythmetic, ["M"]], # SLA (HL)
        0x27: [command.shift_left_arythmetic, ["A"]], # SLA A
        0x28: [command.shift_right_arythmetic, ["B"]], # SRA B
        0x29: [command.shift_right_arythmetic, ["C"]], # SRA C
        0x2A: [command.shift_right_arythmetic, ["D"]], # SRA D
        0x2B: [command.shift_right_arythmetic, ["E"]], # SRA E
        0x2C: [command.shift_right_arythmetic, ["H"]], # SRA H
        0x2D: [command.shift_right_arythmetic, ["L"]], # SRA L
        0x2E: [command.shift_right_arythmetic, ["M"]], # SRA (HL)
        0x2F: [command.shift_right_arythmetic, ["A"]], # SRA A
        0x30: [command.exchange_nibble, ["B"]], # SWAP B
        0x31: [command.exchange_nibble, ["C"]], # SWAP C
        0x32: [command.exchange_nibble, ["D"]], # SWAP D
        0x33: [command.exchange_nibble, ["E"]], # SWAP E
        0x34: [command.exchange_nibble, ["H"]], # SWAP H
        0x35: [command.exchange_nibble, ["L"]], # SWAP L
        0x36: [command.exchange_nibble, ["M"]], # SWAP (HL)
        0x37: [command.exchange_nibble, ["A"]], # SWAP A
        0x38: [command.shift_right_logically, ["B"]], # SRL B
        0x39: [command.shift_right_logically, ["C"]], # SRL C
        0x3A: [command.shift_right_logically, ["D"]], # SRL D
        0x3B: [command.shift_right_logically, ["E"]], # SRL E
        0x3C: [command.shift_right_logically, ["H"]], # SRL H
        0x3D: [command.shift_right_logically, ["L"]], # SRL L
        0x3E: [command.shift_right_logically, ["M"]], # SRL (HL)
        0x3F: [command.shift_right_logically, ["A"]], # SRL A
        0x40: [command.test_bit, ["B", 0]], # BIT 0,B
        0x41: [command.test_bit, ["C", 0]], # BIT 0,C
        0x42: [command.test_bit, ["D", 0]], # BIT 0,D
        0x43: [command.test_bit, ["E", 0]], # BIT 0,E
        0x44: [command.test_bit, ["H", 0]], # BIT 0,H
        0x45: [command.test_bit, ["L", 0]], # BIT 0,L
        0x46: [command.test_bit, ["M", 0]], # BIT 0,(HL)
        0x47: [command.test_bit, ["A", 0]], # BIT 0,A
        0x48: [command.test_bit, ["B", 1]], #  BIT 1,B
        0x49: [command.test_bit, ["C", 1]], #  BIT 1,C
        0x4A: [command.test_bit, ["D", 1]], #  BIT 1,D
        0x4B: [command.test_bit, ["E", 1]], #  BIT 1,E
        0x4C: [command.test_bit, ["H", 1]], #  BIT 1,H
        0x4D: [command.test_bit, ["L", 1]], #  BIT 1,L
        0x4E: [command.test_bit, ["M", 1]], #  BIT 1,(HL)
        0x4F: [command.test_bit, ["A", 1]], #  BIT 1,A
        0x50: [command.test_bit, ["B", 2]], #  BIT 2,B
        0x51: [command.test_bit, ["C", 2]], #  BIT 2,C
        0x52: [command.test_bit, ["D", 2]], #  BIT 2,D
        0x53: [command.test_bit, ["E", 2]], #  BIT 2,E
        0x54: [command.test_bit, ["H", 2]], #  BIT 2,H
        0x55: [command.test_bit, ["L", 2]], #  BIT 2,L
        0x56: [command.test_bit, ["M", 2]], #  BIT 2,(HL)
        0x57: [command.test_bit, ["A", 2]], #  BIT 2,A
        0x58: [command.test_bit, ["B", 3]], # BIT 3,B
        0x59: [command.test_bit, ["C", 3]], # BIT 3,C
        0x5A: [command.test_bit, ["D", 3]], # BIT 3,D
        0x5B: [command.test_bit, ["E", 3]], # BIT 3,E
        0x5C: [command.test_bit, ["H", 3]], # BIT 3,H
        0x5D: [command.test_bit, ["L", 3]], # BIT 3,L
        0x5E: [command.test_bit, ["M", 3]], # BIT 3,(HL)
        0x5F: [command.test_bit, ["A", 3]], # BIT 3,A
        0x60: [command.test_bit, ["B", 4]], # BIT 4,B
        0x61: [command.test_bit, ["C", 4]], # BIT 4,C
        0x62: [command.test_bit, ["D", 4]], # BIT 4,D
        0x63: [command.test_bit, ["E", 4]], # BIT 4,E
        0x64: [command.test_bit, ["H", 4]], # BIT 4,H
        0x65: [command.test_bit, ["L", 4]], # BIT 4,L
        0x66: [command.test_bit, ["M", 4]], # BIT 4,(HL)
        0x67: [command.test_bit, ["A", 4]], # BIT 4,A
        0x68: [command.test_bit, ["B", 5]], # BIT 5,B
        0x69: [command.test_bit, ["C", 5]], # BIT 5,C
        0x6A: [command.test_bit, ["D", 5]], # BIT 5,D
        0x6B: [command.test_bit, ["E", 5]], # BIT 5,E
        0x6C: [command.test_bit, ["H", 5]], # BIT 5,H
        0x6D: [command.test_bit, ["L", 5]], # BIT 5,L
        0x6E: [command.test_bit, ["M", 5]], # BIT 5,(HL)
        0x6F: [command.test_bit, ["A", 5]], # BIT 5,A
        0x70: [command.test_bit, ["B", 6]], # BIT 6,B
        0x71: [command.test_bit, ["C", 6]], # BIT 6,C
        0x72: [command.test_bit, ["D", 6]], # BIT 6,D
        0x73: [command.test_bit, ["E", 6]], # BIT 6,E
        0x74: [command.test_bit, ["H", 6]], # BIT 6,H
        0x75: [command.test_bit, ["L", 6]], # BIT 6,L
        0x76: [command.test_bit, ["M", 6]], # BIT 6,(HL)
        0x77: [command.test_bit, ["A", 6]], # BIT 6,A
        0x78: [command.test_bit, ["B", 7]], # BIT 7,B
        0x79: [command.test_bit, ["C", 7]], # BIT 7,C
        0x7A: [command.test_bit, ["D", 7]], # BIT 7,D
        0x7B: [command.test_bit, ["E", 7]], # BIT 7,E
        0x7C: [command.test_bit, ["H", 7]], # BIT 7,H
        0x7D: [command.test_bit, ["L", 7]], # BIT 7,L
        0x7E: [command.test_bit, ["M", 7]], # BIT 7,(HL)
        0x7F: [command.test_bit, ["A", 7]], # BIT 7,A
        0x80: [command.reset_bit, ["B", 0]], # RES 0,B
        0x81: [command.reset_bit, ["C", 0]], # RES 0,C
        0x82: [command.reset_bit, ["D", 0]], # RES 0,D
        0x83: [command.reset_bit, ["E", 0]], # RES 0,E
        0x84: [command.reset_bit, ["H", 0]], # RES 0,H
        0x85: [command.reset_bit, ["L", 0]], # RES 0,L
        0x86: [command.reset_bit, ["M", 0]], # RES 0,(HL)
        0x87: [command.reset_bit, ["A", 0]], # RES 0,A
        0x88: [command.reset_bit, ["B", 1]], # RES 1,B
        0x89: [command.reset_bit, ["C", 1]], # RES 1,C
        0x8A: [command.reset_bit, ["D", 1]], # RES 1,D
        0x8B: [command.reset_bit, ["E", 1]], # RES 1,E
        0x8C: [command.reset_bit, ["H", 1]], # RES 1,H
        0x8D: [command.reset_bit, ["L", 1]], # RES 1,L
        0x8E: [command.reset_bit, ["M", 1]], # RES 1,(HL)
        0x8F: [command.reset_bit, ["A", 1]], # RES 1,A
        0x90: [command.reset_bit, ["B", 2]], # RES 2,B
        0x91: [command.reset_bit, ["C", 2]], # RES 2,C
        0x92: [command.reset_bit, ["D", 2]], # RES 2,D
        0x93: [command.reset_bit, ["E", 2]], # RES 2,E
        0x94: [command.reset_bit, ["H", 2]], # RES 2,H
        0x95: [command.reset_bit, ["L", 2]], # RES 2,L
        0x96: [command.reset_bit, ["M", 2]], # RES 2,(HL)
        0x97: [command.reset_bit, ["A", 2]], # RES 2,A
        0x98: [command.reset_bit, ["B", 3]], # RES 3,B
        0x99: [command.reset_bit, ["C", 3]], # RES 3,C
        0x9A: [command.reset_bit, ["D", 3]], # RES 3,D
        0x9B: [command.reset_bit, ["E", 3]], # RES 3,E
        0x9C: [command.reset_bit, ["H", 3]], # RES 3,H
        0x9D: [command.reset_bit, ["L", 3]], # RES 3,L
        0x9E: [command.reset_bit, ["M", 3]], # RES 3,(HL)
        0x9F: [command.reset_bit, ["A", 3]], # RES 3,A
        0xA0: [command.reset_bit, ["B", 4]], # RES 4,B
        0xA1: [command.reset_bit, ["C", 4]], # RES 4,C
        0xA2: [command.reset_bit, ["D", 4]], # RES 4,D
        0xA3: [command.reset_bit, ["E", 4]], # RES 4,E
        0xA4: [command.reset_bit, ["H", 4]], # RES 4,H
        0xA5: [command.reset_bit, ["L", 4]], # RES 4,L
        0xA6: [command.reset_bit, ["M", 4]], # RES 4,(HL)
        0xA7: [command.reset_bit, ["A", 4]], # RES 4,A
        0xA8: [command.reset_bit, ["B", 5]], # RES 5,B
        0xA9: [command.reset_bit, ["C", 5]], # RES 5,C
        0xAA: [command.reset_bit, ["D", 5]], # RES 5,D
        0xAB: [command.reset_bit, ["E", 5]], # RES 5,E
        0xAC: [command.reset_bit, ["H", 5]], # RES 5,H
        0xAD: [command.reset_bit, ["L", 5]], # RES 5,L
        0xAE: [command.reset_bit, ["M", 5]], # RES 5,(HL)
        0xAF: [command.reset_bit, ["A", 5]], # RES 5,A
        0xB0: [command.reset_bit, ["B", 6]], # RES 6,B
        0xB1: [command.reset_bit, ["C", 6]], # RES 6,C
        0xB2: [command.reset_bit, ["D", 6]], # RES 6,D
        0xB3: [command.reset_bit, ["E", 6]], # RES 6,E
        0xB4: [command.reset_bit, ["H", 6]], # RES 6,H
        0xB5: [command.reset_bit, ["L", 6]], # RES 6,L
        0xB6: [command.reset_bit, ["M", 6]], # RES 6,(HL)
        0xB7: [command.reset_bit, ["A", 6]], # RES 6,A
        0xB8: [command.reset_bit, ["B", 7]], # RES 7,B
        0xB9: [command.reset_bit, ["C", 7]], # RES 7,C
        0xBA: [command.reset_bit, ["D", 7]], # RES 7,D
        0xBB: [command.reset_bit, ["E", 7]], # RES 7,E
        0xBC: [command.reset_bit, ["H", 7]], # RES 7,H
        0xBD: [command.reset_bit, ["L", 7]], # RES 7,L
        0xBE: [command.reset_bit, ["M", 7]], # RES 7,(HL)
        0xBF: [command.reset_bit, ["A", 7]], # RES 7,A
        0xC0: [command.set_bit, ["B", 0]], # SET 0,B
        0xC1: [command.set_bit, ["C", 0]], # SET 0,C
        0xC2: [command.set_bit, ["D", 0]], # SET 0,D
        0xC3: [command.set_bit, ["E", 0]], # SET 0,E
        0xC4: [command.set_bit, ["H", 0]], # SET 0,H
        0xC5: [command.set_bit, ["L", 0]], # SET 0,L
        0xC6: [command.set_bit, ["M", 0]], # SET 0,(HL)
        0xC7: [command.set_bit, ["A", 0]], # SET 0,A
        0xC8: [command.set_bit, ["B", 1]], # SET 1,B
        0xC9: [command.set_bit, ["C", 1]], # SET 1,C
        0xCA: [command.set_bit, ["D", 1]], # SET 1,D
        0xCB: [command.set_bit, ["E", 1]], # SET 1,E
        0xCC: [command.set_bit, ["H", 1]], # SET 1,H
        0xCD: [command.set_bit, ["L", 1]], # SET 1,L
        0xCE: [command.set_bit, ["M", 1]], # SET 1,(HL)
        0xCF: [command.set_bit, ["A", 1]], # SET 1,A
        0xD0: [command.set_bit, ["B", 2]], # SET 2,B
        0xD1: [command.set_bit, ["C", 2]], # SET 2,C
        0xD2: [command.set_bit, ["D", 2]], # SET 2,D
        0xD3: [command.set_bit, ["E", 2]], # SET 2,E
        0xD4: [command.set_bit, ["H", 2]], # SET 2,H
        0xD5: [command.set_bit, ["L", 2]], # SET 2,L
        0xD6: [command.set_bit, ["M", 2]], # SET 2,(HL)
        0xD7: [command.set_bit, ["A", 2]], # SET 2,A
        0xD8: [command.set_bit, ["B", 3]], # SET 3,B
        0xD9: [command.set_bit, ["C", 3]], # SET 3,C
        0xDA: [command.set_bit, ["D", 3]], # SET 3,D
        0xDB: [command.set_bit, ["E", 3]], # SET 3,E
        0xDC: [command.set_bit, ["H", 3]], # SET 3,H
        0xDD: [command.set_bit, ["L", 3]], # SET 3,L
        0xDE: [command.set_bit, ["M", 3]], # SET 3,(HL)
        0xDF: [command.set_bit, ["A", 3]], # SET 3,A
        0xE0: [command.set_bit, ["B", 4]], # SET 4,B
        0xE1: [command.set_bit, ["C", 4]], # SET 4,C
        0xE2: [command.set_bit, ["D", 4]], # SET 4,D
        0xE3: [command.set_bit, ["E", 4]], # SET 4,E
        0xE4: [command.set_bit, ["H", 4]], # SET 4,H
        0xE5: [command.set_bit, ["L", 4]], # SET 4,L
        0xE6: [command.set_bit, ["M", 4]], # SET 4,(HL)
        0xE7: [command.set_bit, ["A", 4]], # SET 4,A
        0xE8: [command.set_bit, ["B", 5]], # SET 5,B
        0xE9: [command.set_bit, ["C", 5]], # SET 5,C
        0xEA: [command.set_bit, ["D", 5]], # SET 5,D
        0xEB: [command.set_bit, ["E", 5]], # SET 5,E
        0xEC: [command.set_bit, ["H", 5]], # SET 5,H
        0xED: [command.set_bit, ["L", 5]], # SET 5,L
        0xEE: [command.set_bit, ["M", 5]], # SET 5,(HL)
        0xEF: [command.set_bit, ["A", 5]], # SET 5,A
        0xF0: [command.set_bit, ["B", 6]], # SET 6,B
        0xF1: [command.set_bit, ["C", 6]], # SET 6,C
        0xF2: [command.set_bit, ["D", 6]], # SET 6,D
        0xF3: [command.set_bit, ["E", 6]], # SET 6,E
        0xF4: [command.set_bit, ["H", 6]], # SET 6,H
        0xF5: [command.set_bit, ["L", 6]], # SET 6,L
        0xF6: [command.set_bit, ["M", 6]], # SET 6,(HL)
        0xF7: [command.set_bit, ["A", 6]], # SET 6,A
        0xF8: [command.set_bit, ["B", 7]], # SET 7,B
        0xF9: [command.set_bit, ["C", 7]], # SET 7,C
        0xFA: [command.set_bit, ["D", 7]], # SET 7,D
        0xFB: [command.set_bit, ["E", 7]], # SET 7,E
        0xFC: [command.set_bit, ["H", 7]], # SET 7,H
        0xFD: [command.set_bit, ["L", 7]], # SET 7,L
        0xFE: [command.set_bit, ["M", 7]], # SET 7,(HL)
        0xFF: [command.set_bit, ["A", 7]], # SET 7,A
      }

      execute_op_code_cb[op_code][1].insert(0, 2) # all cb op codes have length 1, + 1 for CB
      execute_op_code_cb[op_code][1].insert(0, state)
    
    else:
      reg_A = state.get_register_A()
      reg_B = state.get_register_B()
      reg_C = state.get_register_C()
      reg_D = state.get_register_D()
      reg_E = state.get_register_E()
      reg_H = state.get_register_H()
      reg_L = state.get_register_L()
      reg_AF = state.get_register_AF()
      reg_BC = state.get_register_BC()
      reg_DE = state.get_register_DE()
      reg_HL = state.get_register_HL()
      reg_SP = state.get_register_SP()
      mem_HL = state.get_memory_value(reg_HL)
      flag_Z = state.get_zero_flag()
      flag_C = state.get_carry_flag()

      # convert parameter to two complement for 0xF8 op codestate.get_next_op_code()
      if op_code == 0xF8:
        if parameter >= 1<<8:
          parameter -= 1<<8
        command.set_fixed_flags(state, "0", "0", "-", "-")
        state.set_carry_flag((reg_SP + parameter > 0xFF))
        state.set_half_carry_flag(((reg_SP & 0x0F) + (parameter & 0x0F) > 0x0F))

      execute_op_code = {
        #op_ code: [function, [arguments]]
        0x00: [command.NOP, []], # NOP
        0x01: [command.load_value_into_register, [parameter, "BC"]], # LD BC,d16
        0x02: [command.load_register_into_memory, ["A", reg_BC]], # LD (BC),A
        0x03: [command.add, ["BC", 1, "-", "-", "-", "-"]], # INC BC
        0x04: [command.add, ["B", 1, "Z", "0", "H", "-"]], # INC B
        0x05: [command.subtract, ["B", 1, "Z", "1", "H", "-"]], # DEC B
        0x06: [command.load_value_into_register, [parameter, "B"]], # LD B,d8
        0x07: [command.rotate_left, ["A", 1, "0", "0", "0", "C"]], # RLCA
        0x08: [command.load_register_into_memory, ["SP", parameter]], # LD (a16),SP
        0x09: [command.add, ["HL", reg_BC, "-", "0", "H", "C"]], # ADD HL,BC
        0x0A: [command.load_memory_into_register, [reg_BC, "A"]], # LD A,(BC)
        0x0B: [command.subtract, ["BC", 1, "-", "-", "-", "-"]], # DEC BC
        0x0C: [command.add, ["C", 1, "Z", "0", "H", "-"]], # INC C"
        0x0D: [command.subtract, ["C", 1, "Z", "1", "H", "-"]], # DEC C
        0x0E: [command.load_value_into_register, [parameter, "C"]], # LD C,d8
        0x0F: [command.rotate_right, ["A", 1, "0", "0", "0", "C"]], # RRCA
        0x10: [command.halt, []], # STOP 0
        0x11: [command.load_value_into_register, [parameter, "DE"]], # LD DE,d16
        0x12: [command.load_register_into_memory, ["A", reg_DE]], # LD (DE),A
        0x13: [command.add, ["DE", 1, "-", "-", "-", "-"]], # INC DE
        0x14: [command.add, ["D", 1, "Z", "0", "H", "-"]], # INC D
        0x15: [command.subtract, ["D", 1, "Z", "1", "H", "-"]], # DEC D
        0x16: [command.load_value_into_register, [parameter, "D"]], # LD D,d8
        0x17: [command.rotate_left_through_carry, ["A", 1, "0", "0", "0", "C"]], # RLA
        0x18: [command.jump_to_address, [parameter, True, True]], # JR r8
        0x19: [command.add, ["HL", reg_DE, "-", "0", "H", "C"]], # ADD HL,DE
        0x1A: [command.load_memory_into_register, [reg_DE, "A"]], # LD A,(DE)
        0x1B: [command.subtract, ["DE", 1, "-", "-", "-", "-"]], # DEC DE
        0x1C: [command.add, ["E", 1, "Z", "0", "H", "-"]], # INC E  
        0x1D: [command.subtract, ["E", 1, "Z", "1", "H", "-"]], # DEC E
        0x1E: [command.load_value_into_register, [parameter, "E"]], # LD E,d8
        0x1F: [command.rotate_right_through_carry, ["A", 1, "0", "0", "0", "C"]], # RRA
        0x20: [command.jump_to_address, [parameter, not flag_Z, True]], # JR NZ,r8
        0x21: [command.load_value_into_register, [parameter, "HL"]], # LD HL,d16
        0x22: [command.load_register_into_memory, ["A", reg_HL, "increment"]], # LD (HL+),A",
        0x23: [command.add, ["HL", 1, "-", "-", "-", "-"]], # INC HL
        0x24: [command.add, ["H", 1, "Z", "0", "H", "-"]], # INC H 
        0x25: [command.subtract, ["H", 1, "Z", "1", "H", "-"]], # DEC H
        0x26: [command.load_value_into_register, [parameter, "H"]], # LD H,d8
        0x27: [command.decimal_adjust_akku, []], # DAA
        0x28: [command.jump_to_address, [parameter, flag_Z, True]], # JR Z,r8
        0x29: [command.add, ["HL", reg_HL, "-", "0", "H", "C"]], # ADD HL,HL
        0x2A: [command.load_memory_into_register, [reg_HL, "A", "increment"]], # LD A,(HL+)
        0x2B: [command.subtract, ["HL", 1, "-", "-", "-", "-"]], # DEC HL
        0x2C: [command.add, ["L", 1, "Z", "0", "H", "-"]], # INC L
        0x2D: [command.subtract, ["L", 1, "Z", "1", "H", "-"]], # DEC L
        0x2E: [command.load_value_into_register, [parameter, "L"]], # LD L,d8
        0x2F: [command.xor_value, [0xFF, True]], # CPL
        0x30: [command.jump_to_address, [parameter, not flag_C, True]], # JR NC,r8
        0x31: [command.load_value_into_register, [parameter, "SP"]], # LD SP,d16
        0x32: [command.load_register_into_memory, ["A", reg_HL, "decrement"]], # LD (HL-),A", 
        0x33: [command.add, ["SP", 1, "-", "-", "-", "-"]], # INC SP
        0x34: [command.add_value_to_memory, [1, reg_HL, "Z", "0", "H", "-"]], # INC (HL)
        0x35: [command.subtract_value_from_memory, [1, reg_HL, "Z", "1", "H", "-"]], # DEC (HL)
        0x36: [command.load_value_into_memory, [parameter, reg_HL]], # LD (HL),d8
        0x37: [command.set_carry_flag, []], # SCF 
        0x38: [command.jump_to_address, [parameter, flag_C, True]], # JR C,r8
        0x39: [command.add, ["HL", reg_SP, "-", "0", "H", "C"]], # ADD HL,SP
        0x3A: [command.load_memory_into_register, [reg_HL, "A", "decrement"]], # LD A,(HL-)
        0x3B: [command.subtract, ["SP", 1, "-", "-", "-", "-"]], # DEC SP
        0x3C: [command.add, ["A", 1, "Z", "0", "H", "-"]], # INC A
        0x3D: [command.subtract, ["A", 1, "Z", "1", "H", "-"]], # DEC A
        0x3E: [command.load_value_into_register, [parameter, "A"]], # LD A,d8
        0x3F: [command.complement_carry_flag, []], # CCF
        0x40: [command.load_value_into_register, [reg_B, "B"]], # LD B,B
        0x41: [command.load_value_into_register, [reg_C, "B"]], # LD B,C
        0x42: [command.load_value_into_register, [reg_D, "B"]], # LD B,D
        0x43: [command.load_value_into_register, [reg_E, "B"]], # LD B,E
        0x44: [command.load_value_into_register, [reg_H, "B"]], # LD B,H
        0x45: [command.load_value_into_register, [reg_L, "B"]], # LD B,L
        0x46: [command.load_value_into_register, [mem_HL, "B"]], # LD B,(HL)
        0x47: [command.load_value_into_register, [reg_A, "B"]], # LD B,A
        0x48: [command.load_value_into_register, [reg_B, "C"]], # LD C,B
        0x49: [command.load_value_into_register, [reg_C, "C"]], # LD C,C
        0x4A: [command.load_value_into_register, [reg_D, "C"]], # LD C,D
        0x4B: [command.load_value_into_register, [reg_E, "C"]], # LD C,E
        0x4C: [command.load_value_into_register, [reg_H, "C"]], # LD C,H
        0x4D: [command.load_value_into_register, [reg_L, "C"]], # LD C,L
        0x4E: [command.load_value_into_register, [mem_HL, "C"]], # LD C,(HL)
        0x4F: [command.load_value_into_register, [reg_A, "C"]], # LD C,A
        0x50: [command.load_value_into_register, [reg_B, "D"]], # LD D,B
        0x51: [command.load_value_into_register, [reg_C, "D"]], # LD D,C
        0x52: [command.load_value_into_register, [reg_D, "D"]], # LD D,D
        0x53: [command.load_value_into_register, [reg_E, "D"]], # LD D,E
        0x54: [command.load_value_into_register, [reg_H, "D"]], # LD D,H
        0x55: [command.load_value_into_register, [reg_L, "D"]], # LD D,L
        0x56: [command.load_value_into_register, [mem_HL, "D"]], # LD D,(HL)
        0x57: [command.load_value_into_register, [reg_A, "D"]], # LD D,A
        0x58: [command.load_value_into_register, [reg_B, "E"]], # LD E,B
        0x59: [command.load_value_into_register, [reg_C, "E"]], # LD E,C
        0x5A: [command.load_value_into_register, [reg_D, "E"]], # LD E,D
        0x5B: [command.load_value_into_register, [reg_E, "E"]], # LD E,E
        0x5C: [command.load_value_into_register, [reg_H, "E"]], # LD E,H
        0x5D: [command.load_value_into_register, [reg_L, "E"]], # LD E,L
        0x5E: [command.load_value_into_register, [mem_HL, "E"]], # LD E,(HL)
        0x5F: [command.load_value_into_register, [reg_A, "E"]], # LD E,A
        0x60: [command.load_value_into_register, [reg_B, "H"]], # LD H,B
        0x61: [command.load_value_into_register, [reg_C, "H"]], # LD H,C
        0x62: [command.load_value_into_register, [reg_D, "H"]], # LD H,D
        0x63: [command.load_value_into_register, [reg_E, "H"]], # LD H,E
        0x64: [command.load_value_into_register, [reg_H, "H"]], # LD H,H
        0x65: [command.load_value_into_register, [reg_L, "H"]], # LD H,L
        0x66: [command.load_value_into_register, [mem_HL, "H"]], # LD H,(HL)
        0x67: [command.load_value_into_register, [reg_A, "H"]], # LD H,A
        0x68: [command.load_value_into_register, [reg_B, "L"]], # LD L,B
        0x69: [command.load_value_into_register, [reg_C, "L"]], # LD L,C
        0x6A: [command.load_value_into_register, [reg_D, "L"]], # LD L,D
        0x6B: [command.load_value_into_register, [reg_E, "L"]], # LD L,E
        0x6C: [command.load_value_into_register, [reg_H, "L"]], # LD L,H
        0x6D: [command.load_value_into_register, [reg_L, "L"]], # LD L,L
        0x6E: [command.load_value_into_register, [mem_HL, "L"]], # LD L,(HL)
        0x6F: [command.load_value_into_register, [reg_A, "L"]], # LD L,A
        0x70: [command.load_register_into_memory, ["B", reg_HL]], # LD (HL),B
        0x71: [command.load_register_into_memory, ["C", reg_HL]], # LD (HL),C
        0x72: [command.load_register_into_memory, ["D", reg_HL]], # LD (HL),D
        0x73: [command.load_register_into_memory, ["E", reg_HL]], # LD (HL),E
        0x74: [command.load_register_into_memory, ["H", reg_HL]], # LD (HL),H
        0x75: [command.load_register_into_memory, ["L", reg_HL]], # LD (HL),L
        0x76: [command.halt, []], # HALT
        0x77: [command.load_register_into_memory, ["A", reg_HL]], # LD (HL),A
        0x78: [command.load_value_into_register, [reg_B, "A"]], # LD A,B
        0x79: [command.load_value_into_register, [reg_C, "A"]], # LD A,C
        0x7A: [command.load_value_into_register, [reg_D, "A"]], # LD A,D
        0x7B: [command.load_value_into_register, [reg_E, "A"]], # LD A,E
        0x7C: [command.load_value_into_register, [reg_H, "A"]], # LD A,H
        0x7D: [command.load_value_into_register, [reg_L, "A"]], # LD A,L
        0x7E: [command.load_value_into_register, [mem_HL, "A"]], # LD A,(HL)
        0x7F: [command.load_value_into_register, [reg_A, "A"]], # LD A,A
        0x80: [command.add, ["A", reg_B, "Z", "0", "H", "C"]], # ADD A,B
        0x81: [command.add, ["A", reg_C, "Z", "0", "H", "C"]], # ADD A,C
        0x82: [command.add, ["A", reg_D, "Z", "0", "H", "C"]], # ADD A,D
        0x83: [command.add, ["A", reg_E, "Z", "0", "H", "C"]], # ADD A,E
        0x84: [command.add, ["A", reg_H, "Z", "0", "H", "C"]], # ADD A,H
        0x85: [command.add, ["A", reg_L, "Z", "0", "H", "C"]], # ADD A,L
        0x86: [command.add, ["A", mem_HL, "Z", "0", "H", "C"]], # ADD A,(HL)
        0x87: [command.add, ["A", reg_A, "Z", "0", "H", "C"]], # ADD A,A
        0x88: [command.add_with_carry, ["A", reg_B, "Z", "0", "H", "C"]], # ADC A,B
        0x89: [command.add_with_carry, ["A", reg_C, "Z", "0", "H", "C"]], # ADC A,C
        0x8A: [command.add_with_carry, ["A", reg_D, "Z", "0", "H", "C"]], # ADC A,D
        0x8B: [command.add_with_carry, ["A", reg_E, "Z", "0", "H", "C"]], # ADC A,E
        0x8C: [command.add_with_carry, ["A", reg_H, "Z", "0", "H", "C"]], # ADC A,H
        0x8D: [command.add_with_carry, ["A", reg_L, "Z", "0", "H", "C"]], # ADC A,L
        0x8E: [command.add_with_carry, ["A", mem_HL, "Z", "0", "H", "C"]], # ADC A,(HL)
        0x8F: [command.add_with_carry, ["A", reg_A, "Z", "0", "H", "C"]], # ADC A,A
        0x90: [command.subtract, ["A", reg_B, "Z", "1", "H", "C"]], # SUB B
        0x91: [command.subtract, ["A", reg_C, "Z", "1", "H", "C"]], # SUB C
        0x92: [command.subtract, ["A", reg_D, "Z", "1", "H", "C"]], # SUB D
        0x93: [command.subtract, ["A", reg_E, "Z", "1", "H", "C"]], # SUB E
        0x94: [command.subtract, ["A", reg_H, "Z", "1", "H", "C"]], # SUB H
        0x95: [command.subtract, ["A", reg_L, "Z", "1", "H", "C"]], # SUB L
        0x96: [command.subtract, ["A", mem_HL, "Z", "1", "H", "C"]], # SUB (HL)
        0x97: [command.subtract, ["A", reg_A, "Z", "1", "H", "C"]], # SUB A
        0x98: [command.subtract_with_carry, ["A", reg_B, "Z", "1", "H", "C"]], # SBC A,B
        0x99: [command.subtract_with_carry, ["A", reg_C, "Z", "1", "H", "C"]], # SBC A,C
        0x9A: [command.subtract_with_carry, ["A", reg_D, "Z", "1", "H", "C"]], # SBC A,D
        0x9B: [command.subtract_with_carry, ["A", reg_E, "Z", "1", "H", "C"]], # SBC A,E
        0x9C: [command.subtract_with_carry, ["A", reg_H, "Z", "1", "H", "C"]], # SBC A,H
        0x9D: [command.subtract_with_carry, ["A", reg_L, "Z", "1", "H", "C"]], # SBC A,L
        0x9E: [command.subtract_with_carry, ["A", mem_HL, "Z", "1", "H", "C"]], # SBC A,(HL)
        0x9F: [command.subtract_with_carry, ["A", reg_A, "Z", "1", "H", "C"]], # SBC A,A
        0xA0: [command.and_value, [reg_B]], # 1 AND B
        0xA1: [command.and_value, [reg_C]], # 1 AND C
        0xA2: [command.and_value, [reg_D]], # 1 AND D
        0xA3: [command.and_value, [reg_E]], # 1 AND E
        0xA4: [command.and_value, [reg_H]], # 1 AND H
        0xA5: [command.and_value, [reg_L]], # 1 AND L
        0xA6: [command.and_value, [mem_HL]], # AND (HL)
        0xA7: [command.and_value, [reg_A]], # AND A
        0xA8: [command.xor_value, [reg_B]], # XOR B
        0xA9: [command.xor_value, [reg_C]], # XOR C
        0xAA: [command.xor_value, [reg_D]], # XOR D
        0xAB: [command.xor_value, [reg_E]], # XOR E
        0xAC: [command.xor_value, [reg_H]], # XOR H
        0xAD: [command.xor_value, [reg_L]], # XOR L
        0xAE: [command.xor_value, [mem_HL]], # XOR (HL)
        0xAF: [command.xor_value, [reg_A]], # XOR A
        0xB0: [command.or_value, [reg_B]], # OR B
        0xB1: [command.or_value, [reg_C]], # OR C
        0xB2: [command.or_value, [reg_D]], # OR D
        0xB3: [command.or_value, [reg_E]], # OR E
        0xB4: [command.or_value, [reg_H]], # OR H
        0xB5: [command.or_value, [reg_L]], # OR L
        0xB6: [command.or_value, [mem_HL]], # OR (HL)
        0xB7: [command.or_value, [reg_A]], # OR A
        0xB8: [command.compare_value, [reg_B]], # CP B
        0xB9: [command.compare_value, [reg_C]], # CP C
        0xBA: [command.compare_value, [reg_D]], # CP D
        0xBB: [command.compare_value, [reg_E]], # CP E
        0xBC: [command.compare_value, [reg_H]], # CP H
        0xBD: [command.compare_value, [reg_L]], # CP L
        0xBE: [command.compare_value, [mem_HL]], # CP (HL)
        0xBF: [command.compare_value, [reg_A]], # CP A
        0xC0: [command.return_from_call, [not flag_Z]], # RET NZ
        0xC1: [command.pop_into_register, ["BC"]], # POP BC
        0xC2: [command.jump_to_address, [parameter, not flag_Z]], # JP NZ,a16
        0xC3: [command.jump_to_address, [parameter, True]], # JP a16
        0xC4: [command.call, [parameter, not flag_Z]], # CALL NZ,a16
        0xC5: [command.push_value, [reg_BC]], # PUSH BC
        0xC6: [command.add, ["A", parameter, "Z", "0", "H", "C"]], # ADD A,d8
        0xC7: [command.call, [0x00, True]], # RST 00 H
        0xC8: [command.return_from_call, [flag_Z]], # RET Z
        0xC9: [command.return_from_call, [True]], # RET
        0xCA: [command.jump_to_address, [parameter, flag_Z]], # JP Z,a16
        0xCB: [command.execute_cb, []], # PREFIX CB
        0xCC: [command.call, [parameter, flag_Z]], # CALL Z,a16
        0xCD: [command.call, [parameter, True]], # CALL a16
        0xCE: [command.add_with_carry, ["A", parameter, "Z", "0", "H", "C"]], # ADC A,d8
        0xCF: [command.call, [0x08, True]], # RST 08 H
        0xD0: [command.return_from_call, [not flag_C]], # RET NC
        0xD1: [command.pop_into_register, ["DE"]], # POP DE
        0xD2: [command.jump_to_address, [parameter, not flag_C]], # JP NC,a16
        0xD3: None,
        0xD4: [command.call, [parameter, not flag_C]], # CALL NC,a16
        0xD5: [command.push_value, [reg_DE]], # PUSH DE
        0xD6: [command.subtract, ["A", parameter, "Z", "1", "H", "C"]], # SUB d8
        0xD7: [command.call, [0x10, True]], # RST 10 H
        0xD8: [command.return_from_call, [flag_C]], # RET C
        0xD9: [command.return_from_call, [True, True]], # RETI
        0xDA: [command.jump_to_address, [parameter, flag_C]], # JP C,a16
        0xDB: None,
        0xDC: [command.call, [parameter, flag_C]], # CALL C,a16
        0xDD: None, 
        0xDE: [command.subtract_with_carry, ["A", parameter, "Z", "1", "H", "C"]], # SBC A,d8
        0xDF: [command.call, [0x18, True]], # RST 18 H
        0xE0: [command.load_register_into_memory, ["A", 0xFF00|parameter]], # LDH (a8),A
        0xE1: [command.pop_into_register, ["HL"]], # POP HL
        0xE2: [command.load_register_into_memory, ["A", 0xFF00+reg_C]], # LD (C),A
        0xE3: None,
        0xE4: None,
        0xE5: [command.push_value, [reg_HL]], # PUSH HL
        0xE6: [command.and_value, [parameter]], # AND d8
        0xE7: [command.call, [0x20, True]], # RST 20 H
        0xE8: [command.add, ["SP", parameter, "0", "0", "H", "C"]], # ADD SP,r8
        0xE9: [command.jump_to_address, [reg_HL, True]], # JP (HL)
        0xEA: [command.load_register_into_memory, ["A", parameter]], # LD (a16),A
        0xEB: None,
        0xEC: None,
        0xED: None,
        0xEE: [command.xor_value, [parameter]], # XOR d8
        0xEF: [command.call, [0x28, True]], # RST 28 H
        0xF0: [command.load_memory_into_register, [0xFF00|parameter, "A"]], # LDH A,(a8)
        0xF1: [command.pop_into_register, ["AF"]], # POP AF
        0xF2: [command.load_register_into_memory, ["A", 0xFF00+reg_C]], # LD A,(C)
        0xF3: [command.interrupt, [False]], # DI
        0xF4: None,
        0xF5: [command.push_value, [reg_AF]], # PUSH AF
        0xF6: [command.or_value, [parameter]], # OR d8
        0xF7: [command.call, [0x30, True]], # RST 30 H
        0xF8: [command.load_value_into_register, [reg_SP+parameter % 0x100, "HL"]], # LD HL,SP+r8
        0xF9: [command.load_value_into_register, [reg_HL, "SP"]], # LD SP,HL
        0xFA: [command.load_memory_into_register, [parameter, "A"]], # LD A,(a16)
        0xFB: [command.interrupt, [True]], # EI
        0xFC: None,
        0xFD: None,
        0xFE: [command.compare_value, [parameter]], # CP d8
        0xFF: [command.call, [0x38, True]], # RST 38 H
      }

      # game crash if command does not exist
      if execute_op_code[op_code] == None:
        return None

      execute_op_code[op_code][1].insert(0, oc.op_codes[op_code]["length"])
      execute_op_code[op_code][1].insert(0, state)
    
    # execute op code
    execute_op_code[op_code][0](*execute_op_code[op_code][1])

    # if value was set to random, set it again to 0x00 to limit hash values of diffent states
    if state.get_reset_random_read() != 0:
      state.set_memory_value(state.get_reset_random_read(), 0x00)
      state.set_reset_random_read(0x00)
    
    return state
