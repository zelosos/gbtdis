# -*- coding: utf-8 -*-

from gbtdis.cartridge import rom as cart_rom
import gbtdis.op_codes as oc
from gbtdis.separation import separation as sep
from gbtdis.emulator import memory_region as mr
from gbtdis.backtracking import backtrack as bt
from gbtdis.options import options as opti
import time
import json
import sys

class code_section:
  """
  A code section is a continuous command sequence,
  where no jumps are in-/outcoming in the middle.
  Jumps will only enter at the start of the section and
  jump to a new section at the end.
  """

  def __init__(self, rom_image, bank, start_address, end_address):
    self.start_address = start_address # is absolute (if swapable bank, than 0x4000 + relative address in bank)
    self.end_address = end_address # is absolute
    self.bank = bank # where the start-end address is located
    self.commands = []
    address = start_address
    while address <= end_address:
      self.commands.append(address)
      if mr.is_rom_bank_swapable(address):
        op_code = rom_image.get_value(bank, address-0x4000)
      elif mr.is_rom_bank_0(address):  
        op_code = rom_image.get_value(bank, address)
      else:
        break
      if op_code == 0xCB:
        address += 2
      else:
        address += oc.op_codes[op_code]["length"]

  def add_command(self, address):
    self.commands.append(address)

  def get_commands(self):
    return self.commands

  def get_previous_command(self, address):
    """
    returns the address of the previous command
    if address does not exists in commands, than the next valide address
    before that will be returned
    if previous command is not in this section, None will be returned
    """
    # not in section
    if address <= self.start_address:
      return None
    
    if address in self.commands:
      # find current index
      index = self.commands.index(address)
      return self.commands[index-1] # can not be out of bounds, because start_address is at position 0
    return self.get_previous_command(address-1)

  def set_start_address(self, start_address):
    self.start_address = start_address

  def set_end_address(self, end_address):
    self.end_address = end_address

  def get_start_address(self):
    return self.start_address

  def get_end_address(self):
    return self.end_address

  def get_bank(self):
    return self.bank
  
  def increase_end_address(self, amount):
    self.end_address += amount

  def contains(self, bank, address):
    if bank != self.bank:
      return False
    return address >= self.start_address and address <= self.end_address

class jump_graph:
  """
  The jump graph will list all jumps and bank changes.
  In between are the code sections.
  This way the code flow can be reconstructed and
  moved backwards to backtack unknown values and origins.
  """
  
  def __init__(self, rom_image, options):
    """
    Create the a new object.
    Create all objekt variables and fill them with start values.

    Input:
    rom_image     = image that should be used for following the program flow
    options       = options default values or overritten by user parameter
    """
    self.rom_image = rom_image
    self.jumps = {} # contains (from_address, to_address)
    self.bank_changes = {} # contains ((bank_from, address_from), (bank_to, address_to))
    self.unfollowed_addresses = set(options.start_addresses)
    self.code_sections = set()
    self.manual_check_needed = set()
    self.max_depth = options.backtrack_recursion_depth
    self.options = options

  def get_code_sections(self):
    return self.code_sections

  def get_manual_list(self):
    return self.manual_check_needed

  def get_manual_list_copy(self):
    """
    Return a copy of manual list.
    All entries are linked to the same objects that manual list uses.
    """
    copy_list = set()
    for manual in self.manual_check_needed:
      copy_list.add(manual)
    return copy_list

  def get_jumps(self):
    return self.jumps

  def get_bank_changes(self):
    return self.bank_changes

  def get_rom_image(self):
    return self.rom_image

  def get_code_section_by_end_address(self, bank, end_address):
    """
    Return the section, that ends with end_address

    Input:
    bank        = bank of the start address
    end_address = end address of the section

    Output:
    section, thats ends with the givven address
    None, if no matching section could be found
    """
    for section in self.code_sections:
      # bank number does not match
      if section.get_bank != bank:
        continue
      if section.get_end_address() == end_address:
        return section
    return None

  def get_code_section_by_start_address(self, bank, start_address):
    """
    Return the section, that starts with start_address

    Input:
    bank          = bank of the start address
    start_address = start address of the section

    Output:
    section, thats starts with the givven address
    None, if no matching section could be found
    """
    for section in self.code_sections:
      # bank number does not match
      if section.get_bank != bank:
        continue
      if section.get_start_address() == start_address:
        return section
    return None

  def get_previous_code_sections(self, section):
    """
    Return all code sections that flows into this code section.

    Input:
    section = current section

    Output:
    all sections, that flowed into the current section
    if no section can be found an empty list is returned
    """
    start_address = section.get_start_address()
    previous_sections = []
    # add previous code sections by jumps
    for jump_from, jump_to in self.jumps.items():
      # jump goes from previous.end_address section to section.start_address
      if start_address == jump_to:
        previous_sections.append(self.get_code_section_by_end_address(section.get_bank(), jump_from))

    # add previous code sections by bank changes
    for bank_from, bank_to in self.bank_changes.items():
      # bank number does not match
      if bank_to[0] != section.get_bank():
        continue
      if start_address == bank_to[1]:
        previous_sections.append(self.get_code_section_by_end_address(bank_from[0], bank_from[1]))
    return previous_sections

  def get_existing_code_section(self, bank, destination):
    """
    Find the existing code section, where destination is part of.

    Input:
    bank        = bank of the address
    destination = address that should be part of the searched code section

    Ouput:
    None, if address is not part of a known section
    section, where the address is part of
    """
    for section in self.code_sections:
      if section.contains(bank, destination):
        return section
    return None

  def parse_guided_address_input(self, input_line):
    # read pipe input
    inputs = input_line.split(":")[1].rstrip().split("_")
    loaded_bank = int(inputs[0])
    address = int(inputs[1])
    op_code = int(inputs[2])
    return loaded_bank, address, op_code

  def exract_guided_op_code_infos(self, last_op_code):
    type_last_op_code = "default"
    last_op_conditional_split = False
    section_end_reason = ""
    is_on_hold = False
    if oc.is_static_jump(last_op_code):
      type_last_op_code = "static"
      section_end_reason = "static jump"
    elif oc.is_condition_jump(last_op_code):
      type_last_op_code = "static"
      last_op_conditional_split = True
      section_end_reason = "conditional jump"
    elif oc.is_dynamic_jump(last_op_code):
      type_last_op_code = "dynamic"
      section_end_reason = "dynamic jump"
    elif oc.is_static_call(last_op_code):
      type_last_op_code = "call"
      last_op_conditional_split = True
      section_end_reason = "static call"
    elif oc.is_condition_call(last_op_code):
      type_last_op_code = "call"
      last_op_conditional_split = True
      section_end_reason = "conditional call"
    elif oc.is_rst(last_op_code):
      type_last_op_code = "call"
      last_op_conditional_split = True
      section_end_reason = "RST command"
    elif oc.is_return(last_op_code):
      type_last_op_code = "return"
      section_end_reason = "return"
    elif oc.is_condition_return(last_op_code):
      type_last_op_code = "return"
      last_op_conditional_split = True
      section_end_reason = "conditional return"
    elif oc.is_halt(last_op_code):
      section_end_reason = "HALT command"
      is_on_hold = True
    return type_last_op_code, last_op_conditional_split, section_end_reason, is_on_hold

  def create_new_section_if_not_exist(self, bank, address):
    # if allready visited, do not create new section
    if self.is_section_start(bank, address):
      section = None
    # if allready visited, and jump is to the middle of known section, then split it and create no new one
    elif self.split_section(bank, address):
      section = None
    else:
      section = code_section(self.rom_image, bank, address, address)
    return section

  def create_graph_guided_by_sameboy(self):
    expected_address = 0x0100
    last_address = 0
    last_bank = 1
    last_op_code = 0
    interrupt_enabled = False
    skip_jump_next_command = False
    activated = False
    current_section = None
    section_is_new = True
    section_after_interrupt = None

    # read piped input from SameBoy
    try:
      for input_line in sys.stdin:
        # pipe input is streamed address of execution
        if input_line.startswith("address:"):
          # parse input
          loaded_bank, address, op_code = self.parse_guided_address_input(input_line)
          bank = loaded_bank if mr.is_rom_bank_swapable(address) else 0

          # wait for program start (skip boot loader)
          if not activated:
            if address == expected_address:
              activated = True
            else:
              continue
            
          if section_is_new:
            # continue with section if interrupted by interrupt
            if section_after_interrupt != None and not skip_jump_next_command:
              current_section = section_after_interrupt
              section_after_interrupt = None
            # normal new section
            else:
              current_section = self.create_new_section_if_not_exist(bank, address)
              section_is_new = False

          # get strings for the op code to label jumps correctly
          type_last_op_code, last_op_conditional_split, section_end_reason, is_on_hold = self.exract_guided_op_code_infos(last_op_code)

          # add jump if happened
          if expected_address != address and not skip_jump_next_command:
            # if jump is not an interrupt return
            if not oc.is_interrupt_return(last_op_code):
              self.add_to_graph(last_bank, loaded_bank, last_address, address, type_last_op_code)
              if last_op_conditional_split:
                self.add_to_graph(last_bank, loaded_bank, last_address, address, "continue")
            # if current section is tracked, than close it now
            if current_section != None:
              self.close_section(current_section, expected_address-1, section_end_reason)
            current_section = self.create_new_section_if_not_exist(bank, address)

          # add jump to contine code if conditional
          elif expected_address == address and last_op_conditional_split:
            self.add_to_graph(last_bank, loaded_bank, last_address, address, "continue")
            # TODO add jump target to unfollowed addresses
            # if current section is tracked, than close it now
            if current_section != None:
              self.close_section(current_section, expected_address-1, section_end_reason)
            current_section = self.create_new_section_if_not_exist(bank, address)

          # bank change happend
          elif last_bank != loaded_bank or bank != 0 and bank != loaded_bank:
            self.add_to_graph(last_bank, loaded_bank, last_address, address, "bank change")

          # normal command, so add to section
          else:
            # if current section is tracked, than add the current command
            if current_section != None:
              current_section.add_command(address)
            if is_on_hold:
              # if current section is tracked, than close it now
              if current_section != None:
                self.close_section(current_section, expected_address-1, section_end_reason)
              section_is_new = True

          # update expected address and last values
          expected_address = address + 2 if op_code == 0xCB else address + oc.op_codes[op_code]["length"]
          last_address = address
          last_bank = loaded_bank
          last_op_code = op_code
          skip_jump_next_command = False

        # do not mark execution of interrupt as jump
        elif input_line.startswith("interrupt called:"):
          skip_jump_next_command = True
          section_after_interrupt = current_section
          section_is_new = True

        elif input_line.startswith("interrupt now disabled:"):
          interrupt_enabled = False
        elif input_line.startswith("interrupt now enabled:"):
          interrupt_enabled = True

    except EOFError:
      # pipe collapsed
      pass

  def convert_double_complement_to_singed_int(self, value):
    """
    Converts a Byte value into a signed integer.
    For example:
    0x00 = 0
    0x01 = 1
    0x80 = -1
    0xFF = -128
    """
    # convert offset from double complement to signed int
    if value >= (1<<7):
      value -= 1<<8
    return value

  def get_static_jump_location(self, op_code, loaded_bank,  bank_of_jump_command, address_of_jump_command, section):
    """
    Find the detination address of the jump and return it.

    Input:
    op_code                 = operation code
    loaded_bank             = currently loaded bank
    bank_of_jump_command    = bank of the address of the command
    address_of_jump_command = address of the jump command
    section                 = section, where this command is part of

    Output:
    None, if no value found
    absolute address of the target 
    """
    if mr.is_rom_bank_swapable(address_of_jump_command):
      address_of_jump_command -= 0x4000
    # relative jump
    if op_code == 0x18:
      return self.get_address_relative_parameter(loaded_bank, bank_of_jump_command, address_of_jump_command, section)
    # absolute jump
    if op_code == 0xC3:
      return self.get_address_parameter(loaded_bank, bank_of_jump_command, address_of_jump_command, section)

  def get_condition_jump_location(self, op_code, loaded_bank, bank_of_jump_command, address_of_jump_command, section):
    """
    Find the detination address of the jump and return it.

    Input:
    op_code                 = operation code
    loaded_bank             = currently loaded bank
    bank_of_jump_command    = bank of the address of the command
    address_of_jump_command = address of the jump command
    section                 = section, where this command is part of

    Output:
    None, if no value found
    absolute address of the target 
    """
    if mr.is_rom_bank_swapable(address_of_jump_command):
      address_of_jump_command -= 0x4000
    # relative jump
    if op_code in [0x20, 0x28, 0x30, 0x38]:
      return self.get_address_relative_parameter(loaded_bank, bank_of_jump_command, address_of_jump_command, section)
    # absolute jump
    if op_code in [0xC2, 0xCA, 0xD2, 0xDA]:
      return self.get_address_parameter(loaded_bank, bank_of_jump_command, address_of_jump_command, section)

  def get_address_parameter(self, loaded_bank, bank_of_command, address_of_command, section):
    """
    Find the absolute value after the command and return it.

    Input:
    loaded_bank        = the currently loaded bank
    bank_of_command    = bank of the address
    address_of_command = address of the op_code
    section            = current section

    Output:
    None   = no value could be found
    offset = target address of the jump
    """
    if mr.is_rom_bank_0(address_of_command):
      high = self.rom_image.get_value(bank_of_command, address_of_command+2)
      low = self.rom_image.get_value(bank_of_command, address_of_command+1)
    elif mr.is_rom_bank_swapable(address_of_command):
      high = self.rom_image.get_value(bank_of_command, address_of_command+2-0x4000)
      low = self.rom_image.get_value(bank_of_command, address_of_command+1-0x4000)
    else:
      track = bt(self, loaded_bank, bank_of_command, address_of_command, section)
      m_values = track.memory_value(address_of_command+2, depth=self.max_depth)
      if len(m_values) <= 0:
        return None
      high = m_values.pop()
      m_values = track.memory_value(address_of_command+1, depth=self.max_depth)
      if len(m_values) <= 0:
        return None
      low = m_values.pop()
    return high << 8 | low

  def get_address_relative_parameter(self, loaded_bank, bank_of_command, address_of_command, section):
    """
    This functions reads the parameter of the op_code and returns the relative
    value.

    Input:
    loaded_bank        = the currently loaded bank
    bank_of_command    = bank of the address
    address_of_command = address of the op_code
    section            = current section

    Output:
    None   = no value could be found
    offset = offset as a relative address to the address of the (op_code + op_length)
    """
    if mr.is_rom(address_of_command):
      offset = self.rom_image.get_value(bank_of_command, address_of_command+1)
    elif mr.is_rom_bank_swapable(address_of_command):
      offset = self.rom_image.get_value(bank_of_command, address_of_command+1-0x4000)
    else:
      track = bt(self, loaded_bank, bank_of_command, address_of_command, section)
      m_values = track.memory_value(address_of_command+1, depth=self.max_depth)
      if len(m_values) <= 0:
        return None
      offset = m_values.pop()
    offset = self.convert_double_complement_to_singed_int(offset)
    return  address_of_command + 2 + offset # two for length of op_code

  def get_rst_location(self, op_code):
    """
    returns a the address of the RST op code.

    Input:
    op_code = operation code

    Output:
    address where this op_code will the program flow directs to
    """
    value = {
      0xC7: 0x00,
      0xCF: 0x08,
      0xD7: 0x10,
      0xDF: 0x18,
      0xE7: 0x20,
      0xEF: 0x28,
      0xF7: 0x30,
      0xFF: 0x38
    }
    return value[op_code]

  def save_jumps(self, filename):
    """
    Takes all jumps of this object and saves it as json in filename.

    Input:
    filename = name of the json file

    Output:
    None
    """
    jumps_string = []
    for fr, to in self.jumps.items():
      for to_add in to:
        if len(to_add) == 2:
          jumps_string.append([str(fr[0]), str(fr[1]), str(to_add[0]), str(to_add[1])])
        else:
          jumps_string.append([str(fr[0]), str(fr[1]), str(to_add[0]), str(to_add[1]), str(to_add[2])])
    json.dump(jumps_string, open(filename, "w"), sort_keys=True, indent=2)

  def save_manual_list(self, filename):
    """
    Takes the manual list of this object and saves it as a json in filename.

    Input:
    filename = name of the json file

    Output:
    None
    """
    manual_list = []
    for i in self.manual_check_needed:
      manual_list.append([str(i[0]), str(i[1]), i[2]])
    json.dump(manual_list, open(filename, "w"), sort_keys=True, indent=2)


  def close_section(self, section, end_address, reason):
    """
    Closes the section and set the end address.
    
    Input:
    section = section to be closed
    end_address = end address of this section

    Output:
    None
    """
    # print info if enabled
    if self.options.print_closing_section:
      print("closing program section at: 0x{:04X} due to {1}".format(end_address, reason))
    # remember code section
    section.set_end_address(end_address)
    self.code_sections.add(section)

  def split_section(self, target_bank, destination):
    """
    This functions checks if the destination address is in the middle of an
    existing section. If this is the case, then split the section at this
    destination address.

    Input:
    target_bank = bank of the destination address
    destination = address where the split would happen

    Output:
    True, if split was done
    False, no split has to be done
    """
    # get section, where destination address is part of
    colliding_section = self.get_existing_code_section(target_bank, destination)
    if colliding_section == None:
      return False

    # is destination allready visited, so split code section at destination
    if colliding_section.get_start_address() != destination:
      upper_section = code_section(self.rom_image, target_bank, colliding_section.get_start_address(), destination - 1)
      lower_section = code_section(self.rom_image, target_bank, destination, colliding_section.get_end_address())
      # replace colliding section with two new once splitted by destination address
      self.code_sections.add(upper_section)
      self.code_sections.add(lower_section)
      self.code_sections.remove(colliding_section)
      # add a artifical jump from upper to lower section
      self.add_jump(target_bank, upper_section.get_commands()[-1], target_bank, destination, "continue")

      # print message if enabled
      if self.options.print_section_split:
        if mr.is_rom(destination):
          print("section splitted at address 0x{:04X} in bank 0x{:02X}".format(destination, target_bank))
        else:
          print("section splitted at address 0x{:04X}".format(destination))
      return True
    return False

  def is_section_start(self, bank, address):
    """
    This functions checks if the givven address bank combination is a start
    of any known program section in the flow graph.
    Sections are not overlapping, so the beginning of a colliding section
    could only be the start address.

    Input:
    bank = bank of the address
    address = address that should be the start of a section

    Output:
    True, if address is start of a known section
    False, if address is not start of a address
    """
    colliding_section = self.get_existing_code_section(bank, address)
    if colliding_section == None: # no sections contains this address, so could not be start of one
      return False
    return colliding_section.get_start_address() == address

  def add_jump(self, current_bank, current_address, target_bank, target_address, jump_type="unknown"):
    """
    This function inserts a given jump into the jumps table.
    The jumps table is made of sets, so the new value will be not double.
    If the key of the origin of the jump does not exist, an empty set will
    be created before inserting the target location.

    Input:
    current_bank = bank of the origin address
    current_address = origin address
    target_bank = bank of the target address
    target_address = destination of the jump
    jump_type = string that names the type of the jump (static, dynamic, relative, call, continue, bank)

    Output:
    None
    """
    if (current_bank, current_address) not in self.jumps:
      self.jumps[(current_bank, current_address)] = set()
    self.jumps[(current_bank, current_address)].add((target_bank, target_address, jump_type))
  
  def add_to_graph(self, loaded_bank, destination_bank, destination_address, current_address, jump_type):
    """
    This function inserts the given address into the flow graph.
    For this the jump from to is insert into the jumps table.
    If the jumps goes in the middle of a existing section, then this section is
    split at the jump target position.
    """
    current_bank = 0
    if mr.is_rom_bank_swapable(current_address):
      current_bank = loaded_bank
    target_bank = 0
    target_address = destination_address
    if mr.is_rom_bank_swapable(destination_address):
      # jump to a swappable bank
      target_bank = destination_bank

    # remember jump
    self.add_jump(current_bank, current_address, target_bank, target_address, jump_type)

    # if jump to middle of allready visited section, split section in half at jump address
    # and not add into unfollowed address list
    if not self.split_section(target_bank, target_address):
      # add new address to continue building next section
      self.unfollowed_addresses.add((loaded_bank, target_address))
    # not splited, but could be jump to start of section
    elif not self.is_section_start(target_bank, target_address):
      self.unfollowed_addresses.add((loaded_bank, target_address))

  def add_new_unfollowed_address(self, loaded_bank, address):
    """
    Given a loaded bank and address, this function adds this value pair to the
    unfollowed_address list.
    When the address is allready listed in the flow graph, it is not added to
    the unfollowed_address list. Thus preventing an infinite loop.
    """
    if self.options.print_new_unfollowed_addresses:
      print("remember address 0x{:04X} with loaded bank 0x{:02X} to follow later".format(address, loaded_bank))
    if mr.is_rom_bank_swapable:
      if (loaded_bank, address) not in self.jumps.values():
        self.unfollowed_addresses.add((loaded_bank, address))
        return
    if (0, address) not in self.jumps.values():
      self.unfollowed_addresses.add((loaded_bank, address))

  def mark_program_sections(self, separation):
    """
    Given a separation, this function adds all known program sections of the created
    flow graph to the section as "program".
    The address is converted to be relative to the beginning of the bank.
    """
    for section in self.code_sections:
      bank = section.get_bank()
      start = section.get_start_address()
      # only mark if code section is rom
      if mr.is_rom_bank_0(start):
        separation.set_section_type(bank, section.get_start_address(), section.get_end_address(), "program")
      # make addresses relativ to bank start if swapable bank
      if mr.is_rom_bank_swapable(start):
        separation.set_section_type(bank, section.get_start_address()-0x4000, section.get_end_address()-0x4000, "program")

    for manual in self.manual_check_needed:
      bank = manual[0]
      address = manual[1]
      # only mark if address is rom
      if mr.is_rom_bank_0(address):
        separation.set_type(bank, address, "program")
      # make addresses relativ to bank start if swapable bank
      if mr.is_rom_bank_swapable(start):
        separation.set_type(bank, address-0x4000, "program")

  def build_until_no_change(self):
    """
    This functions creates the flow graph like discriped in self.build().
    Then it tries again for all manuall addresses in the hope a valid value
    can be found now with additional paths in the flow graph.
    This is repeated until for all manual marked addresses there was no success.
    """
    # continue trying to build graph, until manual list does not change anymore
    old_manual_list = self.get_manual_list_copy()
    while True:
      for manual in old_manual_list:
        self.add_new_unfollowed_address(manual[0], manual[1])
      self.build()
      new_manual_list = self.get_manual_list_copy()
      if len(old_manual_list.difference(new_manual_list)) == 0:
        break
      old_manual_list = new_manual_list

  def process_static_jump(self, op_code, loaded_bank, current_bank, address, section, end_address):
    """
    Current op code is a jump to an explicit address.
    So current section will be closed and the start address of the jump
    destionation will be remembered to follow later.

    Input:
    op_code      = currently processed op code
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    address      = address of currently processed op code
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    destination = self.get_static_jump_location(op_code, loaded_bank, current_bank, address, section)
    self.close_section(section, end_address, "static jump")
    if destination == None:
      self.manual_check_needed.add((current_bank, address, "unknown jump destination at 0x{:04X}".format(address)))
    else:
      self.add_to_graph(loaded_bank, loaded_bank, destination, address, "static")

  def process_condition_jump(self, op_code, loaded_bank, current_bank, address, section, end_address):
    """
    Current op code is a conditional jump to an explicit address.
    So current section will be closed and the start address of the jump
    destination and after this command will be remembered to follow later.

    Input:
    op_code      = currently processed op code
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    address      = address of currently processed op code
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    destination = self.get_condition_jump_location(op_code, loaded_bank, current_bank, address, section)
    self.close_section(section, end_address, "conditional jump")
    if destination == None:
      self.manual_check_needed.add((current_bank, address, "unknown jump destination at 0x{:04X}".format(address)))
    else:
      self.add_to_graph(loaded_bank, loaded_bank, destination, address, "static")
    self.add_to_graph(loaded_bank, loaded_bank, end_address +1, address, "continue")

  def process_dynamic_jump(self, loaded_bank, current_bank, address, section, end_address):
    """
    Current op code is a jump to a dynamicly calculated address.
    So current section will be closed and the start address of the jump
    destination will be remembered to follow later. It could be that the
    destination can not be found recursivly. Then the address of the current
    op code is marked for manual inspection.

    Input:
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    address      = address of currently processed op code
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    # backtrack from here to determin if bank change happen
    track = bt(self, loaded_bank, current_bank, address, section)
    hl_values = track.register_value("HL", depth=self.max_depth)
    if len(hl_values) == 0:
      # backtrack has max depth limit reached therefore no result found
      # so add for manual inspection
      self.manual_check_needed.add((current_bank, address, "unknown value of HL at 0x{:04X}".format(address)))
    else:
      for hl in hl_values:
        self.add_to_graph(loaded_bank, loaded_bank, hl, address, "dynamic")
    self.close_section(section, end_address, "dynamic calculated jump")

  def process_static_call(self, address, loaded_bank, current_bank, section, end_address):
    """
    Current op code is a call to an explicit address.
    So current section will be closed and the start address of the call
    destination and after this command will be remembered to follow later.

    Input:
    address      = address of currently processed op code
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    if mr.is_rom_bank_swapable(address):
      destination = self.get_address_parameter(loaded_bank, current_bank, address-0x4000, section)
    else:
      destination = self.get_address_parameter(loaded_bank, current_bank, address, section)
    self.close_section(section, end_address, "static call")
    if destination == None:
      self.manual_check_needed.add((current_bank, address, "unknown call destination at 0x{:04X}".format(address)))
    else:
      self.add_to_graph(loaded_bank, loaded_bank, destination, address, "call")
    self.add_to_graph(loaded_bank, loaded_bank, end_address +1, address, "continue")

  def process_conditional_call(self, loaded_bank, current_bank, address, section, end_address):
    """
    Current op code is a conditional call to an explicit address.
    So current section will be closed and the start address of the call
    destination and after this command will be remembered to follow later.

    Input:
    address      = address of currently processed op code
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    destination = self.get_address_parameter(loaded_bank, current_bank, address, section)
    self.close_section(section, end_address, "conditional call")
    if destination == None:
      self.manual_check_needed.add((current_bank, address, "unknown call destination at 0x{:04X}".format(address)))
    else:
      self.add_to_graph(loaded_bank, loaded_bank, destination, address, "call")
    self.add_to_graph(loaded_bank, loaded_bank, end_address +1, address, "continue")

  def process_unconditional_return(self, end_address, section):
    """
    Current op code is unconditional return.
    So current section will be closed.

    Input:
    section      = section of the current op code
    end_address  = address of the last byte of the currently processed op code

    Output:
    None
    """
    self.close_section(section, end_address, "return")

  def process_conditional_return(self, end_address, section, loaded_bank, address):
    """
    Current op code is a conditional return.
    So current section will be closed and the address after this command will be
    remembered to follow later.

    Input:
    end_address  = address of the last byte of the currently processed op code
    section      = section of the current op code
    loaded_bank  = currently loaded bank
    current_bank = bank of the processed op code/address
    address      = address of currently processed op code

    Output:
    None
    """
    self.close_section(section, end_address, "conditional return")
    self.add_to_graph(loaded_bank, loaded_bank, end_address +1, address, "continue")

  def process_rst(self, op_code, end_address, section, loaded_bank, address):
    """
    Current op code is a unconditional call to address 0x00, 0x08, 0x10, 0x18,
    0x20, 0x28, 0x30 or 0x38.
    So current section will be closed and the start address of the call
    destination and after this command will be remembered to follow later.

    Input:
    op_code      = currently proccessed op code
    end_address  = address of the last byte of the currently processed op code
    section      = section of the current op code
    loaded_bank  = currently loaded bank
    address      = address of currently processed op code

    Output:
    None
    """
    destination = self.get_rst_location(op_code)
    self.close_section(section, end_address, "RST command")
    self.add_to_graph(loaded_bank, loaded_bank, destination, address, "call")
    self.add_to_graph(loaded_bank, loaded_bank, end_address +1, address, "continue")

  def process_halt(self, end_address, section):
    """
    Current op code is HALT or STOP command.
    So current section will be closed.

    Input:
    end_address  = address of the last byte of the currently processed op code
    section      = section of the current op code

    Output:
    None
    """
    self.close_section(section, end_address, "HALT command")

  def process_bank_change(self, loaded_bank, current_bank, address, section, end_address, op_code):
    """
    The current op code could trigger a bank change. This functions finds
    recursivly the new loaded bank number.

    Input:
    loaded_bank  = currently loaded rom bank
    current_bank = rom bank of the currently processed op code
    address      = address of the currently processed op code
    section      = section of the currently processed op code
    end_address  = address of the last byte of the op code
    op_code      = currently processed op code

    Output:
    True if a bank change did happen
    False if not

    Output:
    None
    """
    # backtrack from here to determin if bank change happen
    track = bt(self, loaded_bank, current_bank, address, section)
    new_bank = track.loaded_bank_number(depth=self.max_depth)
    # no bank change happen
    if new_bank == None:
      return False

    if len(new_bank) == 0:
      # rucursion has max depth limit reached therefore no result found
      # so add for manual inspection
      self.manual_check_needed.add((current_bank, address, "unknown new bank value at 0x{:04X}".format(address)))
      if self.options.bank_change_not_manual:
        return False
    else:
      destination = address + oc.op_codes[op_code]["length"]
      for b in new_bank:
        self.add_to_graph(b, b, destination, address, "bank")
    self.close_section(section, end_address, "bank change")
    return True

  def process_flow_into_swappable_rom(self, address, section, loaded_bank):
    """
    The last op code was located on rom bank 0.
    Now the program flowed into the swappable rom bank.
    This is marked as a jump from rom bank 0 to loaded rom bank

    Input:
    address     = current address
    section     = current section
    loaded_bank = currently loaded section

    Output:
    None
    """
    # execution from bank 0 flowed into swapable bank
    self.close_section(section, 0x3FFF, "flow into swappable rom")
    # continue programm flow on the loaded bank
    self.add_to_graph(loaded_bank, loaded_bank, address, 0x3FFF, "bank")

  def process_flow_into_vram(self, section):
    """
    The last op code was located on the swappable rom bank.
    Now the program flowed into the vram. This is most likely not wanted.
    Therefore the current section is closed here.

    Input:
    section     = current section

    Output:
    None
    """
    # execution from loaded bank flowed from loaded swapable rom bank into video ram
    # so close section and continue with next section
    self.close_section(section, 0x7FFF, "flow into vram")

  def get_next_op_code(self, address, current_bank, loaded_bank, section, end_address):
    """
    Read the next op code, that would be executed.
    If the next op code could not be red, the section is closed and the address
    where the next op code would be red is marked for manual inspection.

    Input:
    address      = address of the 
    current_bank = bank of the address
    loaded_bank  = currently loaded bank
    section      = section of the last op code / current section
    end_address  = address of the last byte of the previous op code

    Output:
    None, if op code could not be found
    op code of the next command as integer
    """
    # read op code from swappable bank
    if mr.is_rom_bank_swapable(address):
      op_code = self.rom_image.get_value(current_bank, address-0x4000)
    # read op code from bank 0
    elif mr.is_rom_bank_0(address):
      op_code = self.rom_image.get_value(current_bank, address)
    # read op code from ram
    else:
      track = bt(self, loaded_bank, current_bank, address, section)
      m_values = track.memory_value(address, depth=self.max_depth)
      if len(m_values) <= 0:
        self.close_section(section, end_address, "op code not found")
        self.manual_check_needed.add((current_bank, address, "unknown memory value at 0x{:04X}".format(address)))
        return None
      op_code = m_values.pop()
    return op_code

  def loop_until_section_end(self, start_address, current_bank, loaded_bank, section):
    """
    Continue adding addresses to this section until an command would end this
    section or branche it into new program sections.

    Input:
    start_address = address where the program section starts
    current_bank  = bank of the start address
    loaded_bank   = currently loaded rom bank
    section       = section, thats end should be found

    Output:
    None
    """
    address = start_address
    end_address = start_address
    code_section_end_reached = False
    while not code_section_end_reached:
      code_section_end_reached = True
      # handle overflow in next memory region
      if mr.is_rom_bank_swapable(address) and current_bank == 0:
        self.process_flow_into_swappable_rom(address, section, loaded_bank)
        continue
      if mr.is_video_ram(address) and current_bank == loaded_bank:
        self.process_flow_into_vram(section)
        continue

      # remember this command at address in section for recursive resolving
      section.add_command(address)
      # get next op_code
      op_code = self.get_next_op_code(address, current_bank, loaded_bank, section, end_address)
      if op_code == None:
        # continue with next section of op_code could not be found
        continue
      if self.options.print_processed_op_code:
        print("processing op code: 0x{:02X}".format(op_code))
      if op_code == 0xCB:
        end_address = address + 1
      else:
        end_address = address + oc.op_codes[op_code]["length"] -1

      # if current op code is static jump
      if oc.is_static_jump(op_code):
        self.process_static_jump(op_code, loaded_bank, current_bank, address, section, end_address)
      # if current op code is condition jump
      elif oc.is_condition_jump(op_code):
        self.process_condition_jump(op_code, loaded_bank, current_bank, address, section, end_address)
      # if current op code is dynamic calculated jump
      elif oc.is_dynamic_jump(op_code):
        self.process_dynamic_jump(loaded_bank, current_bank, address, section, end_address)
      # if current op code is a call
      elif oc.is_static_call(op_code):
        self.process_static_call(address, loaded_bank, current_bank, section, end_address)
      # if current op code is a conditional call
      elif oc.is_condition_call(op_code):
        self.process_conditional_call(loaded_bank, current_bank, address, section, end_address)
      # if current op code is a nonconditional return
      elif oc.is_return(op_code):
        self.process_unconditional_return(end_address, section)
      # if current op code is a conditional return
      elif oc.is_condition_return(op_code):
        self.process_conditional_return(end_address, section, loaded_bank, address)
      # if current op code is a call to predefined address
      elif oc.is_rst(op_code):
        self.process_rst(op_code, end_address, section, loaded_bank, address)
      # if current op code is a halt/stop command
      elif oc.is_halt(op_code):
        self.process_halt(end_address, section)
      # if current op code could lead to a bank change
      elif self.rom_image.bank_amount > 2 and oc.bank_change_possible(op_code) and not self.options.bank_change_ignored:
        code_section_end_reached = self.process_bank_change(loaded_bank, current_bank, address, section, end_address, op_code)
      else:
        # any other command does not close this section, so continue
        code_section_end_reached = False

      # update address for the next op code to process
      address = end_address +1

  def build(self):
    """
    This functions follows the program flow of all addresses in unfollowed_addresses.
    Doing this a program flow graph is created.
    Checks for bank change are made for every read.
    Dynamic jumps with calculated jumps are resolved by backtracking the jump address.
    There the address / memory_value of jump / bank is recursivly backwards calculated,
    by following the flow graph allready created.

    Input:
    None (attributes of this object are be used)

    Output:
    None (flow graph and sections are saved as attributes of this object)
    """
    # as long as there are addresses to follow continue
    while len(self.unfollowed_addresses) > 0:
      tmp = self.unfollowed_addresses.pop()
      loaded_bank, start_address = tmp[0], tmp[1]

      # current bank is bank of address
      current_bank = 0
      if mr.is_rom_bank_swapable(start_address):
        current_bank = loaded_bank

      # address was process already
      if self.get_existing_code_section(current_bank, start_address) != None:
        continue

      # print the current visited address and bank to show some progress
      if self.options.print_new_section_start_address:
        if mr.is_rom(start_address):
          print("Following code at bank: {:02X} | address: 0x{:04X}. Loaded bank is 0x{:02X}".format(current_bank, start_address, loaded_bank))
        else:
          print("Following code at address: 0x{:04X}. Loaded bank is 0x{:02X}".format(start_address, loaded_bank))

      section = code_section(self.rom_image, current_bank, start_address, start_address)
      self.loop_until_section_end(start_address, current_bank, loaded_bank, section)
    # all addresses have been followed as far as possible
        




  