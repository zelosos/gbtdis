# -*- coding: utf-8 -*-
import sys
import gbtdis.op_codes as opc
from os import path

class rom:
  BANK_SIZE = 16*1024 # 16KB
  BANK_LIMIT = 256

  def __init__(self):
    """
    init with an empty rom
    """
    self.banks = [[0x00]*self.BANK_SIZE]*self.BANK_LIMIT
    self.rom_size_type = 0x00
    self.bank_controller = "None"
    self.ram_size_type = 0x00
    self.bank_amount = self.BANK_LIMIT

  def get_bank(self, bank_number):
    bank_number = self.BANK_LIMIT-1 if bank_number > self.BANK_LIMIT else bank_number
    return self.banks[bank_number]

  def get_size_type(self):
    return self.rom_size_type

  def get_bank_controller_type(self):
    return self.bank_controller

  def get_ram_size_type(self):
    return self.ram_size_type

  def set_ram_size_type(self, value):
    self.ram_size_type = value

  def set_value(self, bank, address, value):
    self.banks[bank][address] = value

  def get_value(self, bank, address):
    return self.banks[bank][address]
    
  def load_rom(self, file_name):
    """
    Given a file name of a rom, it is loaded.
    """
    if not path.exists(file_name):
      return False
    with open(file_name, 'rb+') as f:
      # read rom file
      rom_bytes = bytearray(f.read())

    left_length = rom_bytes.__len__()
    if left_length >= self.BANK_SIZE:
      # determine controller type
      controller = {
        0x01 : "MBC1",
        0x02 : "MBC1",
        0x03 : "MBC1",
        0x05 : "MBC2",
        0x06 : "MBC2",
        0x0B : "MMM01",
        0x0C : "MMM01",
        0x0D : "MMM01",
        0x0F : "MBC3",
        0x10 : "MBC3",
        0x11 : "MBC3",
        0x12 : "MBC3",
        0x13 : "MBC3",
        0x15 : "MBC4",
        0x16 : "MBC4",
        0x17 : "MBC4",
        0x18 : "MBC5",
        0x19 : "MBC5",
        0x1A : "MBC5",
        0x1B : "MBC5",
        0x1C : "MBC5",
        0x1D : "MBC5",
        0x1E : "MBC5",
        0xFC : "POCKET CAMERA",
        0xFD : "BANDAI TAMA5",
        0xFE : "HuC3",
        0xFF : "HuC1",
      }
      cartdrige_type = rom_bytes[0x0147]
      if cartdrige_type in controller:
        self.bank_controller = controller[cartdrige_type]

      # determine rom size type
      self.rom_size_type = rom_bytes[0x0148]
      bank_amount = {
        0x00:2,
        0x01:4,
        0x02:8,
        0x03:16,
        0x04:32,
        0x05:64,
        0x06:128,
        0x07:256,
        0x52:72,
        0x53:80,
        0x54:96
      }
      self.bank_amount = bank_amount[self.rom_size_type]

      # determine ram size
      self.ram_size_type = rom_bytes[0x0149]

    current_bank = 0
    # copy data to rom banks
    while left_length >= self.BANK_SIZE:
      self.banks[current_bank] = rom_bytes[current_bank*self.BANK_SIZE:(current_bank+1)*self.BANK_SIZE]
      """
        rom bank 0x20, 0x40, 0x60 exist not under type 0x05 and 0x06
        instead when addressed they return the data from 0x21, 0x41, 0x61
        so here the same values are loaded into 0x20, 0x40, 0x60
      """
      if self.rom_size_type in [0x05, 0x06] and current_bank in [0x20, 0x40, 0x60]:
        current_bank += 1
        self.banks[current_bank] = self.banks[current_bank-1]
      current_bank += 1
      left_length -= self.BANK_SIZE

    # copy left bytes, if less than a full bank
    if left_length > 0 and current_bank < self.BANK_LIMIT:
      self.banks[current_bank] = rom_bytes[current_bank*self.BANK_SIZE:(current_bank+1)*self.BANK_SIZE]
    return True

  def print_op_codes(self, bank, start_address, end_address):
    """
    Naivly print the values of a given bank.
    Takes the next byte and print it as it would be a op code.
    Therefore it could happen, that the offset is wrong and it prints different
    op codes, than the code flow would see.
    Data is printed as commands as well.

    Example of the offset problematik:
    Naivly print:       reality:
    jr 1                jr 1
    LD DE, 0xB1B0       db 0x11
                        OR C
                        OR B
    """
    end_address = self.BANK_SIZE if end_address > self.BANK_SIZE else end_address
    start_address = 0 if start_address < 0 else start_address
    
    rom = iter(self.banks[bank][start_address:end_address])
    address = start_address

    CB_extension = False
    # iterate over the rom
    for c in rom:    
      # add address to line
      line = "${0:0{1}X} ".format(address,4) 
      address += 1

      # add op code mnemonic to line
      if CB_extension:
        command = opc.op_codes_cb[c]  
        CB_extension = False 
      else:
        command = opc.op_codes[c] 
        if c == 0xCB:
          CB_extension = True
      line += " 0x{0:0{1}X} ".format(c,2)
      if command["mnemonic"] == None:
        line += "Data"
      else:
        line += command["mnemonic"]

      # add hex values if comand uses them
      l=1
      while l < command["length"] and address < end_address:
        c = next(rom)  
        line += " 0x{0:0{1}X}".format(c,2)
        l+=1
        address += 1
      print(line)

  def print_op_codes_bank(self, bank_number):
    """
    Print a complete bank of bank_number.
    """
    self.print_op_codes(bank_number, 0, self.BANK_SIZE)

