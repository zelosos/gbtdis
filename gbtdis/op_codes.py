#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# OP Codes by https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
# formed into a nested Dictionary

op_codes = {
  0x00: {"mnemonic":"NOP",         "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x01: {"mnemonic":"LD BC,d16",   "length": 3, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x02: {"mnemonic":"LD (BC),A",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x03: {"mnemonic":"INC BC",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x04: {"mnemonic":"INC B",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x05: {"mnemonic":"DEC B",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x06: {"mnemonic":"LD B,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x07: {"mnemonic":"RLCA",        "length": 1, "duration":  4, "zf":"0", "nf":"0", "hcf":"0", "cf":"C"},
  0x08: {"mnemonic":"LD (a16),SP", "length": 3, "duration": 20, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x09: {"mnemonic":"ADD HL,BC",   "length": 1, "duration":  8, "zf":"-", "nf":"0", "hcf":"H", "cf":"C"},
  0x0A: {"mnemonic":"LD A,(BC)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x0B: {"mnemonic":"DEC BC",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x0C: {"mnemonic":"INC C",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x0D: {"mnemonic":"DEC C",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x0E: {"mnemonic":"LD C,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x0F: {"mnemonic":"RRCA",        "length": 1, "duration":  4, "zf":"0", "nf":"0", "hcf":"0", "cf":"C"},
  0x10: {"mnemonic":"STOP 0",      "length": 2, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"}, # is length 2, because its 0x10 0x00
  0x11: {"mnemonic":"LD DE,d16",   "length": 3, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x12: {"mnemonic":"LD (DE),A",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x13: {"mnemonic":"INC DE",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x14: {"mnemonic":"INC D",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x15: {"mnemonic":"DEC D",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x16: {"mnemonic":"LD D,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x17: {"mnemonic":"RLA",         "length": 1, "duration":  4, "zf":"0", "nf":"0", "hcf":"0", "cf":"C"},
  0x18: {"mnemonic":"JR r8",       "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x19: {"mnemonic":"ADD HL,DE",   "length": 1, "duration":  8, "zf":"-", "nf":"0", "hcf":"H", "cf":"C"},
  0x1A: {"mnemonic":"LD A,(DE)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x1B: {"mnemonic":"DEC DE",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x1C: {"mnemonic":"INC E",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x1D: {"mnemonic":"DEC E",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x1E: {"mnemonic":"LD E,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x1F: {"mnemonic":"RRA",         "length": 1, "duration":  4, "zf":"0", "nf":"0", "hcf":"0", "cf":"C"},
  0x20: {"mnemonic":"JR NZ,r8",    "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x21: {"mnemonic":"LD HL,d16",   "length": 3, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x22: {"mnemonic":"LD (HL+),A",  "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x23: {"mnemonic":"INC HL",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x24: {"mnemonic":"INC H",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x25: {"mnemonic":"DEC H",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x26: {"mnemonic":"LD H,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x27: {"mnemonic":"DAA",         "length": 1, "duration":  4, "zf":"Z", "nf":"-", "hcf":"0", "cf":"C"},
  0x28: {"mnemonic":"JR Z,r8",     "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x29: {"mnemonic":"ADD HL,HL",   "length": 1, "duration":  8, "zf":"-", "nf":"0", "hcf":"H", "cf":"C"},
  0x2A: {"mnemonic":"LD A,(HL+)",  "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x2B: {"mnemonic":"DEC HL",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x2C: {"mnemonic":"INC L",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x2D: {"mnemonic":"DEC L",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x2E: {"mnemonic":"LD L,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x2F: {"mnemonic":"CPL",         "length": 1, "duration":  4, "zf":"-", "nf":"1", "hcf":"1", "cf":"-"},
  0x30: {"mnemonic":"JR NC,r8",    "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x31: {"mnemonic":"LD SP,d16",   "length": 3, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x32: {"mnemonic":"LD (HL-),A",  "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x33: {"mnemonic":"INC SP",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x34: {"mnemonic":"INC (HL)",    "length": 1, "duration": 12, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x35: {"mnemonic":"DEC (HL)",    "length": 1, "duration": 12, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x36: {"mnemonic":"LD (HL),d8",  "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x37: {"mnemonic":"SCF",         "length": 1, "duration":  4, "zf":"-", "nf":"0", "hcf":"0", "cf":"1"},
  0x38: {"mnemonic":"JR C,r8",     "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x39: {"mnemonic":"ADD HL,SP",   "length": 1, "duration":  8, "zf":"-", "nf":"0", "hcf":"H", "cf":"C"},
  0x3A: {"mnemonic":"LD A,(HL-)",  "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x3B: {"mnemonic":"DEC SP",      "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x3C: {"mnemonic":"INC A",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"-"},
  0x3D: {"mnemonic":"DEC A",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"-"},
  0x3E: {"mnemonic":"LD A,d8",     "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x3F: {"mnemonic":"CCF",         "length": 1, "duration":  4, "zf":"-", "nf":"0", "hcf":"0", "cf":"C"},
  0x40: {"mnemonic":"LD B,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x41: {"mnemonic":"LD B,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x42: {"mnemonic":"LD B,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x43: {"mnemonic":"LD B,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x44: {"mnemonic":"LD B,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x45: {"mnemonic":"LD B,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x46: {"mnemonic":"LD B,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x47: {"mnemonic":"LD B,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x48: {"mnemonic":"LD C,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x49: {"mnemonic":"LD C,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4A: {"mnemonic":"LD C,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4B: {"mnemonic":"LD C,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4C: {"mnemonic":"LD C,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4D: {"mnemonic":"LD C,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4E: {"mnemonic":"LD C,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x4F: {"mnemonic":"LD C,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x50: {"mnemonic":"LD D,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x51: {"mnemonic":"LD D,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x52: {"mnemonic":"LD D,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x53: {"mnemonic":"LD D,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x54: {"mnemonic":"LD D,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x55: {"mnemonic":"LD D,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x56: {"mnemonic":"LD D,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x57: {"mnemonic":"LD D,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x58: {"mnemonic":"LD E,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x59: {"mnemonic":"LD E,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5A: {"mnemonic":"LD E,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5B: {"mnemonic":"LD E,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5C: {"mnemonic":"LD E,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5D: {"mnemonic":"LD E,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5E: {"mnemonic":"LD E,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x5F: {"mnemonic":"LD E,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x60: {"mnemonic":"LD H,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x61: {"mnemonic":"LD H,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x62: {"mnemonic":"LD H,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x63: {"mnemonic":"LD H,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x64: {"mnemonic":"LD H,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x65: {"mnemonic":"LD H,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x66: {"mnemonic":"LD H,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x67: {"mnemonic":"LD H,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x68: {"mnemonic":"LD L,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x69: {"mnemonic":"LD L,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6A: {"mnemonic":"LD L,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6B: {"mnemonic":"LD L,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6C: {"mnemonic":"LD L,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6D: {"mnemonic":"LD L,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6E: {"mnemonic":"LD L,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x6F: {"mnemonic":"LD L,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x70: {"mnemonic":"LD (HL),B",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x71: {"mnemonic":"LD (HL),C",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x72: {"mnemonic":"LD (HL),D",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x73: {"mnemonic":"LD (HL),E",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x74: {"mnemonic":"LD (HL),H",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x75: {"mnemonic":"LD (HL),L",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x76: {"mnemonic":"HALT",        "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x77: {"mnemonic":"LD (HL),A",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x78: {"mnemonic":"LD A,B",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x79: {"mnemonic":"LD A,C",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7A: {"mnemonic":"LD A,D",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7B: {"mnemonic":"LD A,E",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7C: {"mnemonic":"LD A,H",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7D: {"mnemonic":"LD A,L",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7E: {"mnemonic":"LD A,(HL)",   "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x7F: {"mnemonic":"LD A,A",      "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0x80: {"mnemonic":"ADD A,B",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x81: {"mnemonic":"ADD A,C",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x82: {"mnemonic":"ADD A,D",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x83: {"mnemonic":"ADD A,E",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x84: {"mnemonic":"ADD A,H",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x85: {"mnemonic":"ADD A,L",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x86: {"mnemonic":"ADD A,(HL)",  "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x87: {"mnemonic":"ADD A,A",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x88: {"mnemonic":"ADC A,B",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x89: {"mnemonic":"ADC A,C",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8A: {"mnemonic":"ADC A,D",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8B: {"mnemonic":"ADC A,E",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8C: {"mnemonic":"ADC A,H",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8D: {"mnemonic":"ADC A,L",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8E: {"mnemonic":"ADC A,(HL)",  "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x8F: {"mnemonic":"ADC A,A",     "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0x90: {"mnemonic":"SUB B",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x91: {"mnemonic":"SUB C",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x92: {"mnemonic":"SUB D",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x93: {"mnemonic":"SUB E",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x94: {"mnemonic":"SUB H",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x95: {"mnemonic":"SUB L",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x96: {"mnemonic":"SUB (HL)",    "length": 1, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x97: {"mnemonic":"SUB A",       "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x98: {"mnemonic":"SBC A,B",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x99: {"mnemonic":"SBC A,C",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9A: {"mnemonic":"SBC A,D",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9B: {"mnemonic":"SBC A,E",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9C: {"mnemonic":"SBC A,H",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9D: {"mnemonic":"SBC A,L",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9E: {"mnemonic":"SBC A,(HL)",  "length": 1, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0x9F: {"mnemonic":"SBC A,A",     "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xA0: {"mnemonic":"AND B",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA1: {"mnemonic":"AND C",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA2: {"mnemonic":"AND D",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA3: {"mnemonic":"AND E",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA4: {"mnemonic":"AND H",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA5: {"mnemonic":"AND L",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA6: {"mnemonic":"AND (HL)",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA7: {"mnemonic":"AND A",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xA8: {"mnemonic":"XOR B",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xA9: {"mnemonic":"XOR C",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAA: {"mnemonic":"XOR D",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAB: {"mnemonic":"XOR E",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAC: {"mnemonic":"XOR H",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAD: {"mnemonic":"XOR L",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAE: {"mnemonic":"XOR (HL)",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xAF: {"mnemonic":"XOR A",       "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB0: {"mnemonic":"OR B",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB1: {"mnemonic":"OR C",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB2: {"mnemonic":"OR D",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB3: {"mnemonic":"OR E",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB4: {"mnemonic":"OR H",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB5: {"mnemonic":"OR L",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB6: {"mnemonic":"OR (HL)",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB7: {"mnemonic":"OR A",        "length": 1, "duration":  4, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xB8: {"mnemonic":"CP B",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xB9: {"mnemonic":"CP C",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBA: {"mnemonic":"CP D",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBB: {"mnemonic":"CP E",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBC: {"mnemonic":"CP H",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBD: {"mnemonic":"CP L",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBE: {"mnemonic":"CP (HL)",     "length": 1, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xBF: {"mnemonic":"CP A",        "length": 1, "duration":  4, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xC0: {"mnemonic":"RET NZ",      "length": 1, "duration": 20, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC1: {"mnemonic":"POP BC",      "length": 1, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC2: {"mnemonic":"JP NZ,a16",   "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC3: {"mnemonic":"JP a16",      "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC4: {"mnemonic":"CALL NZ,a16", "length": 3, "duration": 24, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC5: {"mnemonic":"PUSH BC",     "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC6: {"mnemonic":"ADD A,d8",    "length": 2, "duration":  8, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0xC7: {"mnemonic":"RST 00 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC8: {"mnemonic":"RET Z",       "length": 1, "duration": 20, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xC9: {"mnemonic":"RET",         "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xCA: {"mnemonic":"JP Z,a16",    "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xCB: {"mnemonic":"PREFIX CB",   "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xCC: {"mnemonic":"CALL Z,a16",  "length": 3, "duration": 24, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xCD: {"mnemonic":"CALL a16",    "length": 3, "duration": 24, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xCE: {"mnemonic":"ADC A,d8",    "length": 2, "duration":  8, "zf":"Z", "nf":"0", "hcf":"H", "cf":"C"},
  0xCF: {"mnemonic":"RST 08 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD0: {"mnemonic":"RET NC",      "length": 1, "duration": 20, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD1: {"mnemonic":"POP DE",      "length": 1, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD2: {"mnemonic":"JP NC,a16",   "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD3: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD4: {"mnemonic":"CALL NC,a16", "length": 3, "duration": 24, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD5: {"mnemonic":"PUSH DE",     "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD6: {"mnemonic":"SUB d8",      "length": 2, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xD7: {"mnemonic":"RST 10 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD8: {"mnemonic":"RET C",       "length": 1, "duration": 20, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xD9: {"mnemonic":"RETI",        "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xDA: {"mnemonic":"JP C,a16",    "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xDB: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xDC: {"mnemonic":"CALL C,a16",  "length": 3, "duration": 24, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xDD: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"}, 
  0xDE: {"mnemonic":"SBC A,d8",    "length": 2, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xDF: {"mnemonic":"RST 18 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE0: {"mnemonic":"LDH (a8),A",  "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE1: {"mnemonic":"POP HL",      "length": 1, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE2: {"mnemonic":"LD (C),A",    "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE3: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE4: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE5: {"mnemonic":"PUSH HL",     "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE6: {"mnemonic":"AND d8",      "length": 2, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"0"},
  0xE7: {"mnemonic":"RST 20 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xE8: {"mnemonic":"ADD SP,r8",   "length": 2, "duration": 16, "zf":"0", "nf":"0", "hcf":"H", "cf":"C"},
  0xE9: {"mnemonic":"JP (HL)",     "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xEA: {"mnemonic":"LD (a16),A",  "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xEB: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xEC: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xED: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xEE: {"mnemonic":"XOR d8",      "length": 2, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xEF: {"mnemonic":"RST 28 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF0: {"mnemonic":"LDH A,(a8)",  "length": 2, "duration": 12, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF1: {"mnemonic":"POP AF",      "length": 1, "duration": 12, "zf":"Z", "nf":"N", "hcf":"H", "cf":"C"},
  0xF2: {"mnemonic":"LD A,(C)",    "length": 2, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF3: {"mnemonic":"DI",          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF4: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF5: {"mnemonic":"PUSH AF",     "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF6: {"mnemonic":"OR d8",       "length": 2, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0"},
  0xF7: {"mnemonic":"RST 30 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xF8: {"mnemonic":"LD HL,SP+r8", "length": 2, "duration": 12, "zf":"0", "nf":"0", "hcf":"H", "cf":"C"},
  0xF9: {"mnemonic":"LD SP,HL",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xFA: {"mnemonic":"LD A,(a16)",  "length": 3, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xFB: {"mnemonic":"EI",          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xFC: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xFD: {"mnemonic":None,          "length": 1, "duration":  4, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"},
  0xFE: {"mnemonic":"CP d8",       "length": 2, "duration":  8, "zf":"Z", "nf":"1", "hcf":"H", "cf":"C"},
  0xFF: {"mnemonic":"RST 38 H",    "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-"}
}

op_codes_cb = {
  0x00: {"mnemonic": "RLC B",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x01: {"mnemonic": "RLC C",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x02: {"mnemonic": "RLC D",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x03: {"mnemonic": "RLC E",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x04: {"mnemonic": "RLC H",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x05: {"mnemonic": "RLC L",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x06: {"mnemonic": "RLC (HL)",   "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x07: {"mnemonic": "RLC A",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x08: {"mnemonic": "RRC B",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x09: {"mnemonic": "RRC C",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0A: {"mnemonic": "RRC D",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0B: {"mnemonic": "RRC E",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0C: {"mnemonic": "RRC H",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0D: {"mnemonic": "RRC L",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0E: {"mnemonic": "RRC (HL)",   "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x0F: {"mnemonic": "RRC A",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x10: {"mnemonic": "RL B",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x11: {"mnemonic": "RL C",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x12: {"mnemonic": "RL D",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x13: {"mnemonic": "RL E",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x14: {"mnemonic": "RL H",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x15: {"mnemonic": "RL L",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x16: {"mnemonic": "RL (HL)",    "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x17: {"mnemonic": "RL A",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x18: {"mnemonic": "RR B",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x19: {"mnemonic": "RR C",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1A: {"mnemonic": "RR D",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1B: {"mnemonic": "RR E",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1C: {"mnemonic": "RR H",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1D: {"mnemonic": "RR L",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1E: {"mnemonic": "RR (HL)",    "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x1F: {"mnemonic": "RR A",       "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x20: {"mnemonic": "SLA B",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x21: {"mnemonic": "SLA C",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x22: {"mnemonic": "SLA D",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x23: {"mnemonic": "SLA E",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x24: {"mnemonic": "SLA H",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x25: {"mnemonic": "SLA L",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x26: {"mnemonic": "SLA (HL)",   "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x27: {"mnemonic": "SLA A",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x28: {"mnemonic": "SRA B",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x29: {"mnemonic": "SRA C",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2A: {"mnemonic": "SRA D",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2B: {"mnemonic": "SRA E",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2C: {"mnemonic": "SRA H",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2D: {"mnemonic": "SRA L",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2E: {"mnemonic": "SRA (HL)",   "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x2F: {"mnemonic": "SRA A",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x30: {"mnemonic": "SWAP B",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x31: {"mnemonic": "SWAP C",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x32: {"mnemonic": "SWAP D",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x33: {"mnemonic": "SWAP E",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x34: {"mnemonic": "SWAP H",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x35: {"mnemonic": "SWAP L",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x36: {"mnemonic": "SWAP (HL)",  "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x37: {"mnemonic": "SWAP A",     "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"0" },
  0x38: {"mnemonic": "SRL B",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x39: {"mnemonic": "SRL C",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3A: {"mnemonic": "SRL D",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3B: {"mnemonic": "SRL E",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3C: {"mnemonic": "SRL H",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3D: {"mnemonic": "SRL L",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3E: {"mnemonic": "SRL (HL)",   "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x3F: {"mnemonic": "SRL A",      "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"0", "cf":"C" },
  0x40: {"mnemonic": "BIT 0,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x41: {"mnemonic": "BIT 0,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x42: {"mnemonic": "BIT 0,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x43: {"mnemonic": "BIT 0,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x44: {"mnemonic": "BIT 0,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x45: {"mnemonic": "BIT 0,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x46: {"mnemonic": "BIT 0,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x47: {"mnemonic": "BIT 0,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x48: {"mnemonic": "BIT 1,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x49: {"mnemonic": "BIT 1,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4A: {"mnemonic": "BIT 1,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4B: {"mnemonic": "BIT 1,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4C: {"mnemonic": "BIT 1,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4D: {"mnemonic": "BIT 1,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4E: {"mnemonic": "BIT 1,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x4F: {"mnemonic": "BIT 1,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x50: {"mnemonic": "BIT 2,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x51: {"mnemonic": "BIT 2,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x52: {"mnemonic": "BIT 2,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x53: {"mnemonic": "BIT 2,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x54: {"mnemonic": "BIT 2,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x55: {"mnemonic": "BIT 2,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x56: {"mnemonic": "BIT 2,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x57: {"mnemonic": "BIT 2,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x58: {"mnemonic": "BIT 3,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x59: {"mnemonic": "BIT 3,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5A: {"mnemonic": "BIT 3,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5B: {"mnemonic": "BIT 3,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5C: {"mnemonic": "BIT 3,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5D: {"mnemonic": "BIT 3,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5E: {"mnemonic": "BIT 3,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x5F: {"mnemonic": "BIT 3,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x60: {"mnemonic": "BIT 4,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x61: {"mnemonic": "BIT 4,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x62: {"mnemonic": "BIT 4,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x63: {"mnemonic": "BIT 4,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x64: {"mnemonic": "BIT 4,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x65: {"mnemonic": "BIT 4,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x66: {"mnemonic": "BIT 4,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x67: {"mnemonic": "BIT 4,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x68: {"mnemonic": "BIT 5,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x69: {"mnemonic": "BIT 5,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6A: {"mnemonic": "BIT 5,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6B: {"mnemonic": "BIT 5,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6C: {"mnemonic": "BIT 5,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6D: {"mnemonic": "BIT 5,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6E: {"mnemonic": "BIT 5,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x6F: {"mnemonic": "BIT 5,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x70: {"mnemonic": "BIT 6,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x71: {"mnemonic": "BIT 6,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x72: {"mnemonic": "BIT 6,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x73: {"mnemonic": "BIT 6,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x74: {"mnemonic": "BIT 6,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x75: {"mnemonic": "BIT 6,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x76: {"mnemonic": "BIT 6,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x77: {"mnemonic": "BIT 6,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x78: {"mnemonic": "BIT 7,B",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x79: {"mnemonic": "BIT 7,C",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7A: {"mnemonic": "BIT 7,D",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7B: {"mnemonic": "BIT 7,E",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7C: {"mnemonic": "BIT 7,H",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7D: {"mnemonic": "BIT 7,L",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7E: {"mnemonic": "BIT 7,(HL)", "length": 1, "duration": 16, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x7F: {"mnemonic": "BIT 7,A",    "length": 1, "duration":  8, "zf":"Z", "nf":"0", "hcf":"1", "cf":"-" },
  0x80: {"mnemonic": "RES 0,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x81: {"mnemonic": "RES 0,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x82: {"mnemonic": "RES 0,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x83: {"mnemonic": "RES 0,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x84: {"mnemonic": "RES 0,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x85: {"mnemonic": "RES 0,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x86: {"mnemonic": "RES 0,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x87: {"mnemonic": "RES 0,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x88: {"mnemonic": "RES 1,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x89: {"mnemonic": "RES 1,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8A: {"mnemonic": "RES 1,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8B: {"mnemonic": "RES 1,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8C: {"mnemonic": "RES 1,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8D: {"mnemonic": "RES 1,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8E: {"mnemonic": "RES 1,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x8F: {"mnemonic": "RES 1,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x90: {"mnemonic": "RES 2,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x91: {"mnemonic": "RES 2,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x92: {"mnemonic": "RES 2,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x93: {"mnemonic": "RES 2,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x94: {"mnemonic": "RES 2,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x95: {"mnemonic": "RES 2,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x96: {"mnemonic": "RES 2,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x97: {"mnemonic": "RES 2,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x98: {"mnemonic": "RES 3,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x99: {"mnemonic": "RES 3,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9A: {"mnemonic": "RES 3,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9B: {"mnemonic": "RES 3,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9C: {"mnemonic": "RES 3,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9D: {"mnemonic": "RES 3,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9E: {"mnemonic": "RES 3,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0x9F: {"mnemonic": "RES 3,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA0: {"mnemonic": "RES 4,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA1: {"mnemonic": "RES 4,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA2: {"mnemonic": "RES 4,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA3: {"mnemonic": "RES 4,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA4: {"mnemonic": "RES 4,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA5: {"mnemonic": "RES 4,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA6: {"mnemonic": "RES 4,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA7: {"mnemonic": "RES 4,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA8: {"mnemonic": "RES 5,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xA9: {"mnemonic": "RES 5,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAA: {"mnemonic": "RES 5,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAB: {"mnemonic": "RES 5,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAC: {"mnemonic": "RES 5,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAD: {"mnemonic": "RES 5,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAE: {"mnemonic": "RES 5,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xAF: {"mnemonic": "RES 5,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB0: {"mnemonic": "RES 6,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB1: {"mnemonic": "RES 6,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB2: {"mnemonic": "RES 6,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB3: {"mnemonic": "RES 6,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB4: {"mnemonic": "RES 6,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB5: {"mnemonic": "RES 6,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB6: {"mnemonic": "RES 6,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB7: {"mnemonic": "RES 6,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB8: {"mnemonic": "RES 7,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xB9: {"mnemonic": "RES 7,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBA: {"mnemonic": "RES 7,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBB: {"mnemonic": "RES 7,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBC: {"mnemonic": "RES 7,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBD: {"mnemonic": "RES 7,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBE: {"mnemonic": "RES 7,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xBF: {"mnemonic": "RES 7,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC0: {"mnemonic": "SET 0,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC1: {"mnemonic": "SET 0,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC2: {"mnemonic": "SET 0,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC3: {"mnemonic": "SET 0,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC4: {"mnemonic": "SET 0,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC5: {"mnemonic": "SET 0,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC6: {"mnemonic": "SET 0,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC7: {"mnemonic": "SET 0,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC8: {"mnemonic": "SET 1,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xC9: {"mnemonic": "SET 1,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCA: {"mnemonic": "SET 1,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCB: {"mnemonic": "SET 1,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCC: {"mnemonic": "SET 1,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCD: {"mnemonic": "SET 1,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCE: {"mnemonic": "SET 1,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xCF: {"mnemonic": "SET 1,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD0: {"mnemonic": "SET 2,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD1: {"mnemonic": "SET 2,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD2: {"mnemonic": "SET 2,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD3: {"mnemonic": "SET 2,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD4: {"mnemonic": "SET 2,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD5: {"mnemonic": "SET 2,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD6: {"mnemonic": "SET 2,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD7: {"mnemonic": "SET 2,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD8: {"mnemonic": "SET 3,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xD9: {"mnemonic": "SET 3,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDA: {"mnemonic": "SET 3,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDB: {"mnemonic": "SET 3,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDC: {"mnemonic": "SET 3,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDD: {"mnemonic": "SET 3,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDE: {"mnemonic": "SET 3,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xDF: {"mnemonic": "SET 3,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE0: {"mnemonic": "SET 4,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE1: {"mnemonic": "SET 4,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE2: {"mnemonic": "SET 4,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE3: {"mnemonic": "SET 4,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE4: {"mnemonic": "SET 4,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE5: {"mnemonic": "SET 4,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE6: {"mnemonic": "SET 4,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE7: {"mnemonic": "SET 4,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE8: {"mnemonic": "SET 5,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xE9: {"mnemonic": "SET 5,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xEA: {"mnemonic": "SET 5,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xEB: {"mnemonic": "SET 5,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xEC: {"mnemonic": "SET 5,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xED: {"mnemonic": "SET 5,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xEE: {"mnemonic": "SET 5,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xEF: {"mnemonic": "SET 5,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF0: {"mnemonic": "SET 6,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF1: {"mnemonic": "SET 6,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF2: {"mnemonic": "SET 6,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF3: {"mnemonic": "SET 6,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF4: {"mnemonic": "SET 6,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF5: {"mnemonic": "SET 6,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF6: {"mnemonic": "SET 6,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF7: {"mnemonic": "SET 6,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF8: {"mnemonic": "SET 7,B",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xF9: {"mnemonic": "SET 7,C",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFA: {"mnemonic": "SET 7,D",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFB: {"mnemonic": "SET 7,E",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFC: {"mnemonic": "SET 7,H",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFD: {"mnemonic": "SET 7,L",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFE: {"mnemonic": "SET 7,(HL)", "length": 1, "duration": 16, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" },
  0xFF: {"mnemonic": "SET 7,A",    "length": 1, "duration":  8, "zf":"-", "nf":"-", "hcf":"-", "cf":"-" }
}

def is_static_jump(op_code):
  return op_code in [0x18, 0xC3]

def is_condition_jump(op_code):
  return op_code in [0x20, 0x28, 0x30, 0x38, 0xC2, 0xCA, 0xD2, 0xDA]

def is_dynamic_jump(op_code):
  return op_code == 0xE9

def is_static_call(op_code):
  return op_code == 0xCD

def is_condition_call(op_code):
  return op_code in [0xC4, 0xCC, 0xD4, 0xDC]

def is_return(op_code):
  return op_code in [0xC9, 0xD9]

def is_interrupt_return(op_code):
  return op_code == 0xD9

def is_condition_return(op_code):
  return op_code in [0xC0, 0xC8, 0xD0, 0xD8]

def is_rst(op_code):
  return op_code in [0xC7, 0xCF, 0xD7, 0xDF, 0xE7, 0xEF, 0xF7, 0xFF]

def is_halt(op_code):
  return op_code in [0x76, 0x10]

def is_increment(op_code):
  pass

def is_decrement(op_code):
  pass

def bank_change_possible(op_code):
  return op_code in [0x02, 0x08, 0x12, 0x22, 0x32, 0x36, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77, 0xEA]

def is_write_to_bc_location(op_code):
  return op_code == 0x02

def is_write_to_de_location(op_code):
  return op_code == 0x12

def is_write_to_hl_location(op_code):
  return op_code in [0x22, 0x32, 0x36, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77]

def is_write_to_address(op_code):
  # not complete
  return op_code in [0x08, 0xEA]

def is_register_A_input(op_code):
  return op_code in [0x02, 0x12, 0x22, 0x23, 0x3C, 0x3D, 0x47, 0x4F, 0x57, 0x5F, 0x67, 0x6F, 0x77, 0x7F, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x98, 0x99, 0x9A, 0x9B, 0x9C, 0x9D, 0x9E, 0x9F, 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBE, 0xBF, 0xCE, 0xCF, 0xD6, 0xDE, 0xE0, 0xE2, 0xE6, 0xEA, 0xEE, 0xF6]

def is_register_F_input(op_code):
  return op_code in []

def is_register_B_input(op_code):
  return op_code in [0x03, 0x04, 0x05, 0x0A, 0x0B, 0x09, 0x0A, 0x40, 0x48, 0x50, 0x58, 0x60, 0x68, 0x70, 0x78, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xA8, 0xB0, 0xB8, 0xC5]

def is_register_C_input(op_code):
  return op_code in [0x03, 0x04, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x41, 0x49, 0x51, 0x59, 0x61, 0x69, 0x71, 0x79, 0x81, 0x89, 0x91, 0x99, 0xA1, 0xA9, 0xB1, 0xB9, 0xC5, 0xF2]

def is_register_D_input(op_code):
  return op_code in [0x13, 0x14, 0x15, 0x19, 0x1A, 0x1B, 0x42, 0x4A, 0x52, 0x5A, 0x62, 0x6A, 0x72, 0x7A, 0x82, 0x8A, 0x92, 0x9A, 0xA2, 0xAA, 0xB2, 0xBA, 0xD5]

def is_register_E_input(op_code):
  return op_code in [0x13, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x43, 0x4B, 0x53, 0x5B, 0x63, 0x6B, 0x73, 0x7B, 0x83, 0x8B, 0x93, 0x9B, 0xA3, 0xAB, 0xB3, 0xBB, 0xD5]

def is_register_H_input(op_code):
  return op_code in [0x09, 0x19, 0x22, 0x23, 0x24, 0x25, 0x29, 0x2A, 0x2B, 0x32, 0x34, 0x35, 0x36, 0x39, 0x3A, 0x44, 0x46, 0x4C, 0x4E, 0x54, 0x56, 0x5C, 0x5E, 0x64, 0x66, 0x6C, 0x6E, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77, 0x7C, 0x7E, 0x84, 0x86, 0x8C, 0x8E, 0x94, 0x96, 0x9C, 0x9E, 0xA4, 0xA6, 0xAC, 0xAE, 0xB4, 0xB6, 0xBC, 0xBE, 0xE5, 0xE9, 0xF9]

def is_register_L_input(op_code):
  return op_code in [0x09, 0x19, 0x22, 0x23, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x32, 0x34, 0x35, 0x36, 0x39, 0x3A, 0x45, 0x46, 0x4D, 0x4E, 0x55, 0x56, 0x5D, 0x5E, 0x65, 0x66, 0x6D, 0x6E, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77, 0x7D, 0x7E, 0x85, 0x86, 0x8D, 0x8E, 0x95, 0x96, 0x9D, 0x9E, 0xA5, 0xA6, 0xAD, 0xAE, 0xB5, 0xB6, 0xBD, 0xBE, 0xE5, 0xE9, 0xF9]

def is_memory_at_BC_input(op_code):
  return op_code == 0x0A

def is_memory_at_DE_input(op_code):
  return op_code == 0x1A

def is_memory_at_HL_input(op_code):
  return op_code in [0x34, 0x35, 0x2A, 0x3A, 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E, 0x86, 0x8E, 0x96, 0x9E, 0xA6, 0xAE, 0xB6, 0xBE, 0xE9]

def is_high_memory_at_C_input(op_code):
  # (C)
  return op_code == 0xF2 

def is_two_byte_memory_input(op_code):
  # (a16) or d16
  return op_code in [0x01, 0x11, 0x21, 0x31, 0xFA]

def is_one_byte_memory_input(op_code):
  # (a8) or d8
  return op_code in [0x06, 0x0E, 0x16, 0x1E, 0x26, 0x2E, 0x36, 0x3E, 0xC6, 0xCE, 0xD6, 0xDE, 0xE6, 0xEE, 0xF0, 0xF6, 0xFE]

def has_register_A_changed(op_code):
  if op_code in range(0x78, 0xB8): # all LD A, ADD, SUB, OR, AND, XOR
    if op_code in [0x7F, 0xA7, 0xB7]: # LD A,A / AND A / OR A does not change A
      return False
    return True
  return op_code in [0x07, 0x0A, 0x0F, 0x17, 0x1A, 0x1F, 0x27, 0x2A, 0x2F, 0x3A, 0x3C, 0x3D, 0x3E, 0xC6, 0xCE, 0xD6, 0xDE, 0xE6, 0xEE, 0xF0, 0xF1, 0xF2, 0xF6, 0xFA]

def has_register_B_changed(op_code):
  return op_code in [0x01, 0x03, 0x04, 0x05, 0x06, 0x0B, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0xC1]

def has_register_C_changed(op_code):
  return op_code in [0x01, 0x03, 0x0B, 0x0C, 0x0D, 0x0E, 0x48, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0xC1]

def has_register_D_changed(op_code):
  return op_code in [0x11, 0x13, 0x14, 0x15, 0x16, 0x1B, 0x50, 0x51, 0x53, 0x54, 0x55, 0x56, 0x57, 0xD1]

def has_register_E_changed(op_code):
  return op_code in [0x11, 0x13, 0x1B, 0x1C, 0x1D, 0x1E, 0x58, 0x59, 0x5A, 0x5C, 0x5D, 0x5E, 0x5F, 0xD1]

def has_register_H_changed(op_code):
  return op_code in [0x09, 0x19, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x29, 0x2A, 0x2B, 0x32, 0x39, 0x3A, 0x60, 0x61, 0x62, 0x63, 0x65, 0x66, 0x67, 0xE1, 0xF8]

def has_register_L_changed(op_code):
  return op_code in [0x09, 0x19, 0x21, 0x22, 0x23, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x32, 0x39, 0x3A, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6E, 0x6F, 0xE1, 0xF8]

def has_register_SP_changed(op_code):
  return op_code in [0x31, 0x33, 0x3B, 0xC0, 0xC1, 0xC4, 0xC5, 0xC7, 0xC8, 0xC9, 0xCC, 0xCD, 0xCF, 0xD0, 0xD1, 0xD4, 0xD5, 0xD7, 0xD8, 0xD9, 0xDC, 0xDF, 0xE1, 0xE5, 0xE7, 0xE8, 0xEF, 0xF1, 0xF5, 0xF7, 0xF9, 0xFF]

def has_memory_changed(op_code):
  return op_code in [0x02, 0x08, 0x12, 0x22, 0x32, 0x34, 0x35, 0x36, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77, 0xC5, 0xD5, 0xE0, 0xE2, 0xEA, 0xE5, 0xF5]