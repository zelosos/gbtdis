#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, getopt
import json

class options:
  def __init__(self):
    self.print_new_section_start_address = False
    self.print_closing_section = False
    self.print_processed_op_code = False
    self.print_new_unfollowed_addresses = False
    self.print_section_split = False
    self.backtrack_recursion_depth = 200
    self.separation_file_name = "separation.json"
    self.jumps_file_name = "jumps.json"
    self.manual_file_name = "manual.json"
    self.separation_file_output_enabled = True
    self.jumps_file_output_enabled = True
    self.manual_file_output_enabled = True
    self.rom_file_name = "fourWayBreakout.gb"
    self.bank_change_ignored = False
    self.bank_change_not_manual = False
    self.start_addresses = []
    self.vblank_interrupt_ignored = False
    self.lcdstat_interrupt_ignored = False
    self.timer_interrupt_ignored = False
    self.serial_interrupt_ignored = False
    self.joypad_interrupt_ignored = False
    self.loaded_bank_during_vblank_interrupt = 1
    self.loaded_bank_during_lcdstat_interrupt = 1
    self.loaded_bank_during_timer_interrupt = 1
    self.loaded_bank_during_serial_interrupt = 1
    self.loaded_bank_during_joypad_interrupt = 1
    self.guided_trace_enabled = False

  def add_start_address(self, bank, address):
    if address not in self.start_addresses:
      self.start_addresses.append((bank, address)) # add bank address tuple

  def load_start_address_file(self, file_name):
    with open(file_name) as f:
      addresses = json.load(f)
    for address in addresses:
      self.add_start_address(address[0], address[1])

  def print_help_text(self):
    print("""How to use the program:
-h, --help:
    Print this help text.

-n, --print-new-section:
    Print the start of a new section, that is currently followed.

-c, --print-closing-section:
    Print a message, when a section is closed. The cause for the closing is also
    mentioned.

-o, --print-op-code:
    Print the currently processed op-code with its mnemonic and location.

-u, --print-new-unfollowed:
    Print the start address of a new discovered section, that will be followed
    later.

--print-section-split:
    Print a message, when a jump splits a known section in half.

-g, --guided
    Use the modified version of SameBoy to build up the flow graph by guided
    execution. Afterwards gbtdis followes all not visited site branches of the
    program. This way more program areas are reached and you do not have to
    visit all possible areas by yourself.

-d, --recursion-depth <value>:
    Defines the maximum depth that the programm will use, when it recursivly
    searches for values.
    The recursive search is done for dynamic jumps like "J (HL)" and to decide,
    if a bank change is happening.
    The default value is {default_recursion_depth}.

-S, --separation-file <file name>:
    The value of <file name> defines the file name in which the data/programm
    sections are listed as a JSON format.
    The output lists all banks of the ROM and in each bank the section types,
    followed by the beginning and ending of each section. For example:
    {{
      "0": {{
        "data": [
          [
            0,
            9
          ],
          [
            20,
            29
          ]
        ],
        "program": [
          [
            10,
            19
          ],
          [
            30,
            39
          ]
        ]
      }}
    }}
    are 10 bytes of data, 10 bytes of program,
        10 bytes of data, 10 bytes of program
    The default file name is {default_separation_file_name}.

-J, --jumps-file <file name>:
    The value of <file name> defines the file name in which the jumps are listed
    as a JSON format. The formated output there is a list of:
    [
      from bank,
      from address,
      to bank,
      to address,
    ]
    The addresses are relative to the beginning of the bank.
    The default file name is {default_jumps_file_name}.

-M, --manual-file <file name>:
    The value of <file name> defines the file name in which the addresses are
    listed where the program could not recursivly find a value. Therefore no
    decission of the continuation of the program flow could be made.
    These addresses have to be manualy inspected by the reverse engineer.
    Alternative try to rise the recursive depth and try your luck once more,
    by starting the program again.
    The formated output there is a list of:
    [
      currently loaded bank,
      address of program execution (value of PC),
      comment what need to figured out manually
    ]
    The default file name is {default_manual_file_name}.

--disable-separation-output:
    Disable the writing of the file with a listing of all data and program
    sections.

--disable-jumps-output:
    Disable the writing of the file with the list of all jumps and bank changes.

--disable-manual-output:
    Disable the writing of the file with the list of addresses for manual
    inspection.

-R, --rom-file <file name>:
    Define the file name of the ROM to separate into data and program sections.
    The default ROM file is {default_rom_file_name}.

-b, --ignore-bank-change:
    Ignore writes to ROM, which could trigger a ROM bank change.

-B, --bank-change-not-manual:
    If the value or address of a write to memory can not be found by recursive
    value tracking, than continue, as no bank change would happen.
    This rely on the assumption that if a bank change would happen the value and
    address could be found quickly.
    But be causion: this could lead to a wrong data/program separation.
    So double check the output! The address can be found in the manual.json.
    But never the less this can save you a lot of manual checking.
    Jumps from bank 0 to bank XX are not affected by this.

-a, --start-address = <value>:
    Give the program a start address where the flow graph starts.
    Notice that the default loaded bank at the start, that is used is bank 1.
    By default it is the address, where every Game Boy program starts: 0x{default_start_address:04X}

--start-addresses-json-file <file name>:
    The value of <file name> defines the JSON file name in which all start
    addresses are. The format has to be:
    [
      [
        loaded bank for address 1,
        address 1
      ],
      [
        loaded bank for address 2,
        address 2
      ]
      ...
    ]
    Important is, that the loaded bank is specified! This has not necessary be
    the bank, where the address is located. For example the default start of
    Game Boy execution starts at 0x0100, but the loaded bank is 1.

-v, --ignore-vblank-interrupt:
    Disable to follow the flow of the program at 0x40.
    Use this option, if the vblank interrupt is not used in the program.

-l, --ignore-lcdstat-interrupt:
    Disable to follow the flow of the program at 0x48.
    Use this option, if the lcdstat/hblank interrupt is not used in the program.

-t, --ignore-timer-interrupt:
    Disable to follow the flow of the program at 0x50.
    Use this option, if the timer interrupt is not used in the program.

-s, --ignore-serial-interrupt:
    Disable to follow the flow of the program at 0x58.
    Use this option, if the serial interrupt is not used in the program.

-j, --ignore-joypad-interrupt:
    Disable to follow the flow of the program at 0x60.
    Use this option, if the joypad interrupt is not used in the program.

--loaded-bank-vblank-interrupt <value>:
    The value defines the loaded ROM bank, at the moment of vblank interrupt
    triggering.
    The default value is bank {default_loaded_bank_vblank}.

--loaded-bank-lcdstat-interrupt <value>:
    The value defines the loaded ROM bank, at the moment of lcdstat/hblank
    interrupt triggering.
    The default value is bank {default_loaded_bank_lcdstat}.

--loaded-bank-timer-interrupt <value>:
    The value defines the loaded ROM bank, at the moment of timer interrupt
    triggering.
    The default value is bank {default_loaded_bank_timer}.

--loaded-bank-serial-interrupt <value>:
    The value defines the loaded ROM bank, at the moment of serial interrupt
    triggering.
    The default value is bank {default_loaded_bank_serial}.

--loaded-bank-joypad-interrupt <value>:
    The value defines the loaded ROM bank, at the moment of joypad interrupt
    triggering.
    The default value is bank {default_loaded_bank_joypad}.

-V, --verbose:
    Print a lot more of, what the program is doing at the moment.
    Is equal to:
      --print-new-section
      --print-closing-section
      --print-op-code
      --print-new-unfollowed
      --split-section-split

--version:
    Print the version of the program you are using.
""".format(
  default_recursion_depth = self.backtrack_recursion_depth,
  default_separation_file_name = self.separation_file_name,
  default_jumps_file_name = self.jumps_file_name,
  default_manual_file_name = self.manual_file_name,
  default_rom_file_name = self.rom_file_name,
  default_start_address = 0x100,
  default_loaded_bank_vblank = self.loaded_bank_during_vblank_interrupt,
  default_loaded_bank_lcdstat = self.loaded_bank_during_lcdstat_interrupt,
  default_loaded_bank_timer = self.loaded_bank_during_timer_interrupt,
  default_loaded_bank_serial = self.loaded_bank_during_serial_interrupt,
  default_loaded_bank_joypad = self.loaded_bank_during_joypad_interrupt
))

  def exit_with_error_message(self, message):
    print(message)
    sys.exit(2)
  
  def parse_args(self, args):
    """
    Parse the given arguments.
    Save the configuration in options object
  
    Input:
    args = arguments of command lind
    """
  
    # all possible long args, = expects a value
    long_args = [
      "help",
      "print-new-section",
      "print-closing-section",
      "print-op-code",
      "print-new-unfollowed",
      "print-section-split",
      "guided"
      "recursion-depth=",
      "separation-file=",
      "jumps-file=",
      "manual-file=",
      "disable-separation-output",
      "disable-jumps-output",
      "disable-manual-output",
      "rom-file=",
      "ignore-bank-change",
      "bank-change-not-manual",
      "start-address=",
      "start-addresses-json-file=",
      "ignore-vblank-interrupt",
      "ignore-lcdstat-interrupt",
      "ignore-timer-interrupt",
      "ignore-serial-interrupt",
      "ignore-joypad-interrupt",
      "loaded-bank-vblank-interrupt",
      "loaded-bank-lcdstat-interrupt",
      "loaded-bank-timer-interrupt",
      "loaded-bank-serial-interrupt",
      "loaded-bank-joypad-interrupt",
      "verbose",
      "version"
      ]
    # all possible short args, : expects a value
    short_args = "hpgd:S:J:M:R:bBa:vltsVncou"
  
    # put arguments into dictionary
    try:
      opts, _ = getopt.getopt(args, short_args, long_args)
    except getopt.GetoptError:
      self.exit_with_error_message("Provided argument is unknown.")

    # if one argument is help, then just print the help text
    if ("-h", "") in opts or ("-help", "") in opts:
      self.print_help_text()
      sys.exit()

    # if one argument is version, then just print the version
    if ("--version", "") in opts:
      print("Version: gbtdis 0.1")
      sys.exit()

    # check the provided arguments and save configuration in options
    for opt, value in opts:
      if opt in ("-n", "--print-new-section"):
        self.print_new_section_start_address = True
      elif opt in ("-c", "--print-closing-section"):
        self.print_closing_section = True
      elif opt in ("-o", "--print-op-code"):
        self.print_processed_op_code = True
      elif opt in ("-u", "--print-new-unfollowed"):
        self.print_new_unfollowed_addresses = True
      elif opt == "--print-section-split":
        self.print_section_split = True
      elif opt in ("-g", "--guided"):
        self.guided_trace_enabled = True
      elif opt in ("-d", "--recursion-depth"):
        try:
          value = int(value)
        except:
          self.exit_with_error_message("Recursion depth has to be an integer.")
        self.backtrack_recursion_depth = value
      elif opt in ("-S", "--separation-file"):
        self.separation_file_name = value
      elif opt in ("-J", "--jumps-file"):
        self.jumps_file_name = value
      elif opt in ("-M", "--manual-file"):
        self.manual_file_name = value
      elif opt == "disable-separation-output":
        self.separation_file_output_enabled = False
      elif opt == "disable-jumps-output":
        self.jumps_file_output_enabled = False
      elif opt == "disable-manual-output":
        self.manual_file_output_enabled = False
      elif opt in ("-R", "--rom-file"):
        self.rom_file_name = value
      elif opt in ("-b", "--ignore-bank-change"):
        self.bank_change_ignored = True
      elif opt in ("-B", "--bank-change-not-manual"):
        self.bank_change_not_manual = True
      elif opt in ("-a", "--start-address"):
        try:
          value = int(value, 0) # input can be int or hex
          if value >= 0x8000:
            self.exit_with_error_message("Start address is not a rom address.")
        except:
          self.exit_with_error_message("Start address has to be an integer or hex value.")
        self.add_start_address(1, value)
      elif opt == "--start-addresses-json-file":
        self.load_start_address_file(value)
      elif opt in ("-v", "--ignore-vblank-interrupt"):
        self.vblank_interrupt_ignored = True
      elif opt in ("-l", "--ignore-lcdstat-interrupt"):
        self.lcdstat_interrupt_ignored = True
      elif opt in ("-t", "--ignore-timer-interrupt"):
        self.timer_interrupt_ignored = True
      elif opt in ("-s", "--ignore-serial-interrupt"):
        self.serial_interrupt_ignored = True
      elif opt in ("-j", "--ignore-joypad-interrupt"):
        self.joypad_interrupt_ignored = True
      elif opt == "loaded-bank-vblank-interrupt":
        try:
          value = int(value, 0)
        except:
          self.exit_with_error_message("Loaded bank has to be an interger or hex value.")
        self.loaded_bank_during_vblank_interrupt = value
      elif opt == "loaded-bank-lcdstat-interrupt":
        try:
          value = int(value, 0)
        except:
          self.exit_with_error_message("Loaded bank has to be an interger or hex value.")
        self.loaded_bank_during_lcdstat_interrupt = value
      elif opt == "loaded-bank-timer-interrupt":
        try:
          value = int(value, 0)
        except:
          self.exit_with_error_message("Loaded bank has to be an interger or hex value.")
        self.loaded_bank_during_timer_interrupt = value
      elif opt == "loaded-bank-serial-interrupt":
        try:
          value = int(value, 0)
        except:
          self.exit_with_error_message("Loaded bank has to be an interger or hex value.")
        self.loaded_bank_during_serial_interrupt = value
      elif opt == "loaded-bank-joypad-interrupt":
        try:
          value = int(value, 0)
        except:
          self.exit_with_error_message("Loaded bank has to be an interger or hex value.")
        self.loaded_bank_during_joypad_interrupt = value
      elif opt in ("-V", "--verbose"):
        self.print_new_section_start_address = True
        self.print_closing_section = True
        self.print_processed_op_code = True
        self.print_new_unfollowed_addresses = True