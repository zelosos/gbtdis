# -*- coding: utf-8 -*-

import time 
import psutil
import copy
import multiprocessing
from collections import deque
import queue
import gbtdis.op_codes as op_codes
import gbtdis.emulator as emulator
from gbtdis.separation import separation
from gbtdis.cartridge import rom as cart_rom

class track:

  def __init__(self, origin_state, time_limit, processor_amount):
    self.active_states = deque([origin_state])
    self.no_time_limit = False
    if time_limit <= 0:
      self.time_limit_enabled = False
    else:
      self.time_limit_enabled = True
    self.end_time = time.time()+time_limit
    self.visited_states = {}
    # dictionary pc => hash of state
    for i in range(65536):
      self.visited_states[i] = set([])
    self.jump_table = {}
    self.call_table = {}
    self.rom_separation = separation(256)
    self.processor_amount = processor_amount

  def load_progress(self, file_name):
    pass

  def save_rogress(self, file_name):
    pass

  def get_separation(self):
    return self.rom_separation

  def input_branche_possible(self, state, op_code, parameter):
    # read with [0xFF00 + a8]
    if op_code == 0xF0:
      return parameter == 0
    
    # read with [0xFF0 + C]:
    if op_code == 0xF2:
      return state.get_register_C() == 0

    # read with [BC]
    if op_code == 0x0A:
      return state.get_register_BC() == 0

    # read with [DE]
    if op_code == 0x0A:
      return state.get_register_DE() == 0

    # read with [HL]
    if op_code in [0x2A, 0x36, 0x3A, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77]:
      return state.get_register_HL() == 0

    # read with address
    if op_code in [0x01, 0x11, 0x21, 0x31, 0xFA]:
      return parameter == 0xFF00

    return False

  def branche_with_input(self, state, processed_queue):
    # create a new state for all posible inputs
    for input in range(16): # bit (0-3 of 0xFF00 is read, so 16 possible combinations)
      new_state = copy.deepcopy(state)
      new_state.set_rom_image(state.get_rom_image()) # replace rom image with same object to save ram
      memory_value = new_state.get_memory_value(0xFF00)
      memory_value = (memory_value & 0xF0) | input # upper bits does not change, by pressing buttons
      new_state.set_memory_value(0xFF00, memory_value)
      self.add_to_output_queue(processed_queue, new_state, new_state.get_register_PC(), new_state.hash(), True, True)

  def add_interrupt_branche(self, state, pc, processed_queue):
    new_state = copy.deepcopy(state)
    new_state.set_rom_image(state.get_rom_image()) # replace rom image with same object to save ram
    new_state.set_register("PC", pc)
    # disable IF Flag, IE is still set
    if pc == 0x0040:
      new_state.reset_interrupt_v_blank_flag()
    elif pc == 0x0048:
      new_state.reset_interrupt_lcd_stat_flag()
    elif pc == 0x0050:
      new_state.reset_interrupt_timer_flag()
    elif pc == 0x0058:
      new_state.reset_interrupt_serial_flag()
    elif pc == 0x0060:
      new_state.reset_interrupt_joypad_flag()
    new_state.disable_master_interrupt()
    self.add_to_output_queue(processed_queue, new_state, new_state.get_register_PC(), new_state.hash(), True, True)
      
  def branche_with_interrupt(self, state, processed_queue):
    if state.interrupt_v_blank_enabled():
      self.add_interrupt_branche(state, 0x0040, processed_queue)
      print("v_blank")
    if state.interrupt_lcd_stat_enabled():
      self.add_interrupt_branche(state, 0x0048, processed_queue)
      print("lcd_state")
    if state.interrupt_timer_enabled():
      self.add_interrupt_branche(state, 0x0050, processed_queue)
      print("timer")
    if state.interrupt_serial_enabled():
      self.add_interrupt_branche(state, 0x0058, processed_queue)
      print("serial")
    if state.interrupt_joypad_enabled():
      self.add_interrupt_branche(state, 0x0060, processed_queue)
      print("joypad")

  def random_read_branche_possible(self, state, op_code, parameter):
    # op_codes, that return true:
    # 0x0A = LD A,(BC)
    if op_code == 0x0A:
      return emulator.memory_region.is_random_read(state.get_register_BC())

    # 0x1A = LD A,(DE)
    if op_code == 0x1A:
      return emulator.memory_region.is_random_read(state.get_register_DE())

    # 0x2A = LD A,(HL+)
    # 0x3A = LD A,(HL-)
    # 0x46 = LD B,(HL)
    # 0x4E = LD C,(HL)
    # 0x56 = LD D,(HL)
    # 0x5E = LD E,(HL)
    # 0x66 = LD H,(HL)
    # 0x6E = LD L,(HL)
    # 0x7E = LD A,(HL)
    # 0x86 = ADD A,(HL)
    # 0x8E = ADC A,(HL)
    # 0x96 = SUB A,(HL)
    # 0x9E = SBC A,(HL)
    # 0xA6 = AND (HL)
    # 0xAE = XOR (HL)
    # 0xB6 = OR (HL)
    # 0xBE = CP (HL)
    if op_code in [0x2A, 0x3A, 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E, 0x86, 0x8E, 0x96, 0x9E, 0xA6, 0xAE, 0xB6, 0xBE]:
      return emulator.memory_region.is_random_read(state.get_register_HL())

    # 0xF0 = LD A,(a8)
    if op_code == 0xF0:
      return emulator.memory_region.is_random_read(0xFF00 | parameter)

    # 0xF2 = LD A,(C)
    if op_code == 0xF2:
      return emulator.memory_region.is_random_read(0xFF00 | state.get_register_C())
    
    # 0xFA = LD X,a16
    if op_code == 0xFA:
      return emulator.memory_region.is_random_read(parameter)

    # 0xCB_0x46 = BIT 0,(HL)
    # 0xCB_0x4E = BIT 1,(HL)
    # 0xCB_0x56 = BIT 2,(HL)
    # 0xCB_0x5E = BIT 3,(HL)
    # 0xCB_0x66 = BIT 4,(HL)
    # 0xCB_0x6E = BIT 5,(HL)
    # 0xCB_0x76 = BIT 6,(HL)
    # 0xCB_0x7E = BIT 7,(HL)
    if op_code == 0xCB:
      cb_op_code = state.get_memory_value(state.get_register_PC()+1)
      return cb_op_code in [0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x76, 0x7E]

    # read is not random value
    return False

  def branche_bit_test(self, state, output_queue, address, bit):
    # add branche with bit set
    new_state = copy.deepcopy(state)
    new_state.set_rom_image(state.get_rom_image()) # replace rom image with same object to save ram
    new_state.set_memory_value(address, 1 << bit) # every other bit is also overritten, but it is done anyway by next random read
    new_state.set_reset_random_read(address) # remember address to reset to 0x00 after read
    self.add_to_output_queue(output_queue, new_state, new_state.get_register_PC(), new_state.hash(), True, True)

    # add branche with bit not set
    new_state = copy.deepcopy(state)
    new_state.set_rom_image(state.get_rom_image()) # replace rom image with same object to save ram
    new_state.set_memory_value(address, 0) # every other bit is also overritten, but it is done anyway by next random read
    new_state.set_reset_random_read(address) # remember address to reset to 0x00 after read
    self.add_to_output_queue(output_queue, new_state, new_state.get_register_PC(), new_state.hash(), True, True)

  def branche_with_random_read(self,state, op_code, parameter, processed_queue):    
    # 0xCB_0x46 = BIT 0,(HL)
    # 0xCB_0x4E = BIT 1,(HL)
    # 0xCB_0x56 = BIT 2,(HL)
    # 0xCB_0x5E = BIT 3,(HL)
    # 0xCB_0x66 = BIT 4,(HL)
    # 0xCB_0x6E = BIT 5,(HL)
    # 0xCB_0x76 = BIT 6,(HL)
    # 0xCB_0x7E = BIT 7,(HL)
    if op_code == 0xCB:
      cb_op_code = state.get_memory_value(state.get_register_PC()+1)
      address = state.get_register_HL()
      if cb_op_code == 0x46:
        self.branche_bit_test(state, processed_queue, address, 0)
      elif cb_op_code == 0x4E:
        self.branche_bit_test(state, processed_queue, address, 1)
      elif cb_op_code == 0x56:
        self.branche_bit_test(state, processed_queue, address, 2)
      elif cb_op_code == 0x5E:
        self.branche_bit_test(state, processed_queue, address, 3)
      elif cb_op_code == 0x66:
        self.branche_bit_test(state, processed_queue, address, 4)
      elif cb_op_code == 0x6E:
        self.branche_bit_test(state, processed_queue, address, 5)
      elif cb_op_code == 0x76:
        self.branche_bit_test(state, processed_queue, address, 6)
      elif cb_op_code == 0x7E:
        self.branche_bit_test(state, processed_queue, address, 7)
      return

    address = 0xFF8A

    # 0x0A = LD A,(BC)
    if op_code == 0x0A:
      address = state.get_register_BC()

    # 0x1A = LD A,(DE)
    elif op_code == 0x1A:
      address = state.get_register_DE()

    # 0x2A = LD A,(HL+)
    # 0x3A = LD A,(HL-)
    # 0x46 = LD B,(HL)
    # 0x4E = LD C,(HL)
    # 0x56 = LD D,(HL)
    # 0x5E = LD E,(HL)
    # 0x66 = LD H,(HL)
    # 0x6E = LD L,(HL)
    # 0x7E = LD A,(HL)
    # 0x86 = ADD A,(HL)
    # 0x8E = ADC A,(HL)
    # 0x96 = SUB A,(HL)
    # 0x9E = SBC A,(HL)
    # 0xA6 = AND (HL)
    # 0xAE = XOR (HL)
    # 0xB6 = OR (HL)
    # 0xBE = CP (HL)
    elif op_code in [0x2A, 0x3A, 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E, 0x86, 0x8E, 0x96, 0x9E, 0xA6, 0xAE, 0xB6, 0xBE]:
      address = state.get_register_HL()

    # 0xF0 = LD A,(a8)
    elif op_code == 0xF0:
      address = 0xFF00 | parameter

    # 0xF2 = LD A,(C)
    elif op_code == 0xF2:
      address = 0xFF00 | state.get_register_C()

    # 0xFA = LD X,a16
    elif op_code == 0xFA:
      address = parameter

    # create a new state for all posible values
    for random_value in range(0x100):
      new_state = copy.deepcopy(state)
      new_state.set_rom_image(state.get_rom_image()) # replace rom image with same object to save ram
      new_state.set_memory_value(address, random_value)
      new_state.set_reset_random_read(address) # remember address to reset to 0x00 after read
      self.add_to_output_queue(processed_queue, new_state, new_state.get_register_PC(), new_state.hash(), True, True)

  def add_to_output_queue(self, processed_queue, new_state, pc, state_hash, has_changed, is_branche):
    has_added = False
    while not has_added:
      try:
        processed_queue.put_nowait((new_state, pc, state_hash, has_changed, is_branche))
        has_added = True
      except queue.Full:
        # queue is full, so try again after 1 sec
        print("output queue is full, has to wait")
        time.sleep(1)
        pass

  def mark_as_program(self, state):
    _, op_length, _ = state.get_next_op_code()
    pc = state.get_register_PC()
    if emulator.memory_region.is_rom_bank_0(pc):
      for i in range(op_length):
        self.rom_separation.set_type(0, pc + i, "program")
    elif emulator.memory_region.is_rom_bank_swapable(pc):
      for i in range(op_length):
        self.rom_separation.set_type(state.get_loaded_rom_bank_number(), pc - 0x4000 + i, "program")

  def enough_memory_to_branche(self):
    return (200*1024*1024 < psutil.virtual_memory()[4]) # 200MB or more free = True

  def process_state(self, unprocessed_queue, processed_queue, kill_queue):
    # infinitly get state and process them
    # ends if no new state is available
    while True:
      try:
        kill_queue.get_nowait()
        # stop the process if kill signal received
        return
      except queue.Empty:
        pass

      try:
        state = unprocessed_queue.get(timeout=1)
      except queue.Empty:
        # try again until input
        continue
    
      # state was never seen before, so mark it as code
      op_code, _, parameter = state.get_next_op_code()

      if not self.enough_memory_to_branche():
        self.add_to_output_queue(processed_queue, state, 0, -1, False, False) # hash get ignored anyway, if state does not change, so use it as a memory shortage indicator
        continue
      # branche with interrupts, if they are enabled
      if state.master_interrupt_enabled():
        print("branche with interrupt")
        self.branche_with_interrupt(state, processed_queue)  

      # branche with input, if they are read
      if self.input_branche_possible(state, op_code, parameter):
        print("branche with input")
        self.branche_with_input(state, processed_queue)

      # branche with random value, if read from memory with random returns
      if self.random_read_branche_possible(state, op_code, parameter):
        print("branche with random read")
        self.branche_with_random_read(state, op_code, parameter, processed_queue)
    
      # execute command and put the result on output queue
      state = emulator.command.excute(op_code, state, parameter)
      self.add_to_output_queue(processed_queue, state, state.get_register_PC(), state.hash(), True, False)

  def start_tracking(self):
    processed_queue_size = self.processor_amount*256 # each worker can return N states
    unprocessed_queue_size = self.processor_amount*4 # each worker can have N states in queue
    processed_queue = multiprocessing.Queue(maxsize=unprocessed_queue_size)
    unprocessed_queue = multiprocessing.Queue(maxsize=processed_queue_size)
    kill_queue = multiprocessing.Queue()
    executed_commands = [0x00]
    step_counter = 0

    # recrute workers
    worker_pool = []
    for i in range(self.processor_amount):
      worker_pool.append(multiprocessing.Process(target=self.process_state, args=(unprocessed_queue, processed_queue, kill_queue)))
      worker_pool[i].start()

    # add stating state to visited states
    state = self.active_states[0]
    self.visited_states[state.get_register_PC()].add(state.hash())

    # mark first command as
    self.mark_as_program(state)

    not_enough_memory_counter = 0
    states_to_be_processed = 0 # counts how many states have been given to workers, that are not returned yet
    # while time not over continue
    while not self.time_limit_enabled or time.time() < self.end_time:
      #input("Press Enter to continue...") #  enable for manual stepping

      # give workers next state to process
      try:
        while True:
          next_state = self.active_states.popleft()
          unprocessed_queue.put_nowait(next_state)
          states_to_be_processed += 1 # workers have been given one more state to process
      except queue.Full:
        # output queue to workers is full. workers have to empty it
        self.active_states.append(next_state)
        pass
      except IndexError:
        # active state queue is empty
        pass
      
      # get processed state from worker
      try:
        while True:
          result = processed_queue.get_nowait()
          new_state, register_pc, state_hash, state_changed, is_branche = result[0], result[1], result[2], result[3], result[4]
          if not is_branche:
            states_to_be_processed -= 1 # workers have processed one state, so they have to do one less. (important to exit the main loop => when have all workers finished and no state is left to process?)
          # if state not visited add it to active states and remember it  

          if state_changed and state_hash not in self.visited_states[register_pc]:
            step_counter += 1
            if step_counter % 100 == 0:
              print(step_counter)
            self.visited_states[register_pc].add(state_hash)
            self.active_states.append(new_state)
            self.mark_as_program(new_state)

            if new_state.get_memory_value(new_state.get_register_PC()) not in executed_commands:
              executed_commands.append(new_state.get_memory_value(new_state.get_register_PC()))

          if state_changed:
            not_enough_memory_counter = 0
          else:
            if state_hash == -1: # state did not branche due to memory shortage, but is still a active one
              self.active_states.append(new_state)
            not_enough_memory_counter += 1
          if not_enough_memory_counter >= len(self.visited_states) + processed_queue_size + unprocessed_queue_size:
            # all active states would take to much memory to continue, so exit
            break
      except queue.Empty:
        # len of active_states has to bee checked as well. could be, that output queue (input for workers) is full and therefore states are still in active_states
        if len(self.active_states) == 0 and states_to_be_processed == 0:
          break # end the tracking
        # workers have nothing done yet so continue

    # get the result of all workers, to empty the queues
    while states_to_be_processed > 0:
      try:
        result = processed_queue.get()
        states_to_be_processed -= 1 # workers have processed one state
        new_state, register_pc, state_hash, state_changed = result[0], result[1], result[2], result[3]
        # if state not visited add it to active states and remember it
        if state_changed and state_hash not in self.visited_states[register_pc]:
          self.visited_states[register_pc].add(state_hash)
          self.active_states.append(new_state)
          self.mark_as_program(new_state)
        else:
          if state_hash == 0: # state did not branche, but is still a active one
            self.active_states.append(new_state)
      except queue.Empty:
        # workers have nothing to do so exit loop
        break
        
    # let all workers go home
    for _ in range(len(worker_pool)):
      try:
        kill_queue.put((0))
      except queue.Full:
        pass
    for i in range(len(worker_pool)):
      worker_pool[i].terminate()
    sum = 0
    for i in range(len(self.visited_states)):
      sum += len(self.visited_states[i])
    print("visited unique states: " + str(sum))
    print("steps between trigger and stop: " + str(step_counter))
    #print("executed command codes: ")
    #executed_commands.sort()
    #for i in executed_commands:
    #  print("0x{0:0{1}X}".format(i, 2))


