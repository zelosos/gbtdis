#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from gbtdis.separation import separation
from gbtdis.cartridge import rom as r

# separtion for four way breakout by stefan
# https://github.com/tslanina/Retro-GameBoyColor-StefaN
# separation is done by hand
four_way_separation = separation(r.BANK_LIMIT)
four_way_separation.set_section_type(0, 0x0000, 0x00A6, "program")
four_way_separation.set_section_type(0, 0x00A7, 0x00F2, "data")
four_way_separation.set_section_type(0, 0x00F3, 0x00FB, "program")
four_way_separation.set_section_type(0, 0x00FC, 0x00FF, "data")
four_way_separation.set_section_type(0, 0x0100, 0x0103, "program")
four_way_separation.set_section_type(0, 0x0104, 0x014F, "data")
four_way_separation.set_section_type(0, 0x0150, 0x039B, "program")
four_way_separation.set_section_type(0, 0x039C, 0x03F5, "data")
four_way_separation.set_section_type(0, 0x03F6, 0x7FFF, "unknown") # padding with 0
four_way_separation.save_separation_file("fourWayBreakoutSeparation.json")