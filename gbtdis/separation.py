# -*- coding: utf-8 -*-

import json
from gbtdis.cartridge import rom as r

class separation:
  def __init__(self, bank_amount):
    self.data_classification = []
    # fill with unknown
    for bank_number in range(bank_amount):
      self.data_classification.append({})
      for i in range(r.BANK_SIZE):
        self.data_classification[bank_number][i] = "data"

  def load_separation_file(self, file):
    # TODO check for correct file format
    data = json.load(open(file))
    for bank in data:
      for t in data[bank]:
        for section in t:
          self.set_section_type(bank, section[0], section[1], t)
    
  def save_separation_file(self, file):
    data = {}
    for bank in range(len(self.data_classification)):
      types = {}
      i = 0
      while i < r.BANK_SIZE:
        section = self.get_section(bank, i)
        start_address, end_address, classification = section[0], section[1], section[2]
        i = end_address +1
        if classification not in types:
          types[classification] = [] 
        types[classification].append([start_address, end_address])
      data[bank] = types
    json.dump(data, open(file, "w"), sort_keys=True, indent=2)

  def set_type(self, bank, address, classification):
    self.data_classification[bank][address] = classification

  def set_section_type(self, bank, start_address, end_address, classification):
    for i in range(start_address, end_address + 1):
      self.set_type(bank, i, classification)

  def get_section(self, bank, start_address):
    classification = self.data_classification[bank][start_address]
    end_address = start_address
    while self.data_classification[bank][end_address] == classification:
      if end_address == r.BANK_SIZE-1:
        break
      end_address += 1 # advance one step, and check if it is still the same classification type
    if not end_address == r.BANK_SIZE-1:
      end_address -= 1 # last step was one to much
    return (start_address, end_address, classification)

  def get_type(self, bank, address):
    return self.data_classification[bank][address]

  def get_rom_separation(self):
    return self.data_classification

  def print_bank(self, bank_number):
    for i in range(r.BANK_SIZE):
      print("0x{0:0{1}X}_".format(bank_number, 2) + "0x{0:0{1}X} : ".format(i, 4) + self.data_classification[bank_number][i])
      
  def compare(self, sep2):
    sep1_rom = self.get_rom_separation()
    sep2_rom = sep2.get_rom_separation()

    false_result = {"program": 0, "data": 0, None: 0}
    false_all = 0
    correct = 0
    for bank in range(1):
      for i in range(0x0000, 0x4000):
        if sep1_rom[bank][i] == sep2_rom[bank][i]:
          correct += 1
        else:
          false_result[sep1_rom[bank][i]] += 1
          false_all += 1
  
    print("Correct classified: " + str(correct))
    print("Total false classified: " + str(false_all))
    print("Program false classified:" + str(false_result["program"]))
    print("Data false classified:" + str(false_result["data"]))
    print("positiv rate: " + str(correct/(correct+false_all)))
                
