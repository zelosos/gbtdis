# What is gbtdis?
This programm aids to separate data from command sections of a Game Boy ROM.
For a given GB ROM as Input it generates a JSON file with the data and command sections listed.
Furthermore a list of all known jumps and bank changes is saved in an additional JSON file.
When this program can not resolve the continuation of the programflow of the GB application
the address of execution is listed in another JSON.

The program was created in the context of a bachelor thesis at the University of applied Science in Mittweida.
Futher development may not continue.

# How to execute the application?
Open the main folder in the terminal. Then run the command
```
python3 gbtdis
```

# Available options:
### -h, --help:
Print the help text.

### -n, --print-new-section:
Print the start of a new section, that is currently followed.

### -c, --print-closing-section:
Print a message, when a section is closed. The cause for the closing is also mentioned.

### -o, --print-op-code:
Print the currently processed op-code with its mnemonic and location.

### -u, --print-new-unfollowed:
Print the start address of a new discovered section, that will be followed later.

### --print-section-split:
Print a message, when a jump splits a known section in half.

### -g, --guided
Use the modified version of SameBoy to build up the flow graph by guided execution. Afterwards gbtdis followes all not visited site branches of the program. This way more program areas are reached and you do not have to visit all possible areas by yourself.
With this option piped input is expected. So start both programs with the command:
```
/path/to/the/modified/sameboy <ROM>.gb  | python3 gbtdis -g --<more Parameter>
```

### -d, --recursion-depth <value>:
Defines the maximum depth that the programm will use, when it recursivly searches for values. The recursive search is done for dynamic jumps like "JP (HL)" and to decide, if a bank change is happening. The default value is **200**.

### -S, --separation-file <file name>:
The value of <file name> defines the file name in which the data/programm sections are listed as a JSON format. The output lists all banks of the ROM and in each bank the section types, followed by the beginning and ending of each section. For example:

```
{{
    "0": {{
        "data": [
            [
                0,
                9
            ],
            [
                20,
                29
            ]
        ],
        "program": [
            [
                10,
                19
            ],
            [
                30,
                39
            ]
        ]
    }}
}}
```

are 10 bytes of data, 10 bytes of program, 10 bytes of data, 10 bytes of program
The default file name is **separation.json**.

### -J, --jumps-file <file name>:
The value of <file name> defines the file name in which the jumps are listed
as a JSON format. The formated output there is a list of:
```
[
  from bank,
  from address,
  to bank,
  to address,
  jump_type
]
```
The addresses are relative to the beginning of the bank. Possible values for jump_type are:
- static
- dynamic
- relative
- call
- continue
- bank
The default file name is **jumps.json**.

### -M, --manual-file <file name>:
The value of <file name> defines the file name in which the addresses are listed where the program could not recursivly find a value. Therefore no decission of the continuation of the program flow could be made. These addresses have to be manualy inspected by the reverse engineer. Alternative try to rise the recursive depth and try your luck once more, by starting the program again.<br>
The formated output there is a list of:
```
[
  currently loaded bank,
  address of program execution (value of PC),
  comment what need to figured out manually
]
```
The default file name is **manual.json**.

### --disable-separation-output:
Disable the writing of the file with a listing of all data and program sections.

### --disable-jumps-output:
Disable the writing of the file with the list of all jumps and bank changes.

### --disable-manual-output:
Disable the writing of the file with the list of addresses for manual inspection.

### -R, --rom-file <file name>:
Define the file name of the ROM to separate into data and program sections.
The default ROM file is **fourWayBreakout.gb**.

### -b, --ignore-bank-change:
Ignore writes to ROM, which could trigger a ROM bank change.

### -B, --bank-change-not-manual:
If the value or address of a write to memory can not be found by recursive value tracking, than continue, as no bank change would happen.
This rely on the assumption that if a bank change would happen the value and address could be found quickly.<br><br>
**But be causion**: this could lead to a wrong data/program separation.
So double check the output! The address can be found in the manual.json. But never the less this can save you a lot of manual checking. Jumps from bank 0 to bank XX are **not** affected by this.

### -a, --start-address = <value>:
Give the program a start address where the flow graph starts. Notice that the default loaded bank at the start, that is used is bank **1**.
By default it is the address, where every Game Boy program starts: **0x0100**

### --start-addresses-json-file <file name>:
The value of <file name> defines the JSON file name in which all start addresses are. The format has to be:
```
[
    [
        loaded bank for address 1,
        address 1
    ],
    [
        loaded bank for address 2,
        address 2
    ],
    ...
]
```
Important is, that the loaded bank is specified! This has not necessary be the bank, where the address is located. For example the default start of Game Boy execution starts at **0x0100**, but the loaded bank is **1**.

### -v, --ignore-vblank-interrupt:
Disable to follow the flow of the program at **0x40**. Use this option, if the vblank interrupt is not used in the program.

### -l, --ignore-lcdstat-interrupt:
Disable to follow the flow of the program at **0x48**. Use this option, if the lcdstat/hblank interrupt is not used in the program.

### -t, --ignore-timer-interrupt:
Disable to follow the flow of the program at **0x50**. Use this option, if the timer interrupt is not used in the program.

### -s, --ignore-serial-interrupt:
Disable to follow the flow of the program at **0x58**. Use this option, if the serial interrupt is not used in the program.

### -j, --ignore-joypad-interrupt:
Disable to follow the flow of the program at **0x60**. Use this option, if the joypad interrupt is not used in the program.

### --loaded-bank-vblank-interrupt <value>:
The value defines the loaded ROM bank, at the moment of vblank interrupt triggering. The default value is bank **1**.

### --loaded-bank-lcdstat-interrupt <value>:
The value defines the loaded ROM bank, at the moment of lcdstat/hblank interrupt triggering. The default value is bank **1**.

### --loaded-bank-timer-interrupt <value>:
The value defines the loaded ROM bank, at the moment of timer interrupt triggering. The default value is bank **1**.

### --loaded-bank-serial-interrupt <value>:
The value defines the loaded ROM bank, at the moment of serial interrupt triggering. The default value is bank **1**.

### --loaded-bank-joypad-interrupt <value>:
The value defines the loaded ROM bank, at the moment of joypad interrupt triggering. The default value is bank **1**.

### -V, --verbose:
Print a lot more of, what the program is doing at the moment.<br>
Is equal to:
```
--print-new-section
--print-closing-section
--print-op-code
--print-new-unfollowed
```

### --version:
Print the version of the program you are using.