
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import unittest
import sys

sys.path.append('../gbtdis')

loader = unittest.TestLoader()
testSuite = loader.discover('test')
testRunner = unittest.TextTestRunner(verbosity=2)
testRunner.run(testSuite)