# -*- coding: utf-8 -*-
import unittest
from gbtdis.emulator import machine_state as ms
from gbtdis.emulator import command as cmd
from gbtdis.cartridge import rom

class test_machine_state(unittest.TestCase):

  def setUp(self):
    self.rom = rom()
    self.state = ms(self.rom)

  def test_get_register_values(self):
    register_value = {
      "A":0x01,
      "F":0xB0,
      "B":0x00,
      "C":0x13,
      "D":0x00,
      "E":0xD8,
      "H":0x01,
      "L":0x4D,
      "SP":0xFFFE,
      "PC":0x0100,
      "AF":0x01B0,
      "BC":0x0013,
      "DE":0x00D8,
      "HL":0x014D
    }
    self.assertEqual(self.state.get_register_values()["A"], register_value["A"])
    self.assertEqual(self.state.get_register_values()["F"], register_value["F"])
    self.assertEqual(self.state.get_register_values()["B"], register_value["B"])
    self.assertEqual(self.state.get_register_values()["C"], register_value["C"])
    self.assertEqual(self.state.get_register_values()["D"], register_value["D"])
    self.assertEqual(self.state.get_register_values()["E"], register_value["E"])
    self.assertEqual(self.state.get_register_values()["H"], register_value["H"])
    self.assertEqual(self.state.get_register_values()["L"], register_value["L"])
    self.assertEqual(self.state.get_register_values()["PC"], register_value["PC"])
    self.assertEqual(self.state.get_register_values()["SP"], register_value["SP"])
    self.assertEqual(self.state.get_register_values()["AF"], register_value["AF"])
    self.assertEqual(self.state.get_register_values()["BC"], register_value["BC"])
    self.assertEqual(self.state.get_register_values()["DE"], register_value["DE"])
    self.assertEqual(self.state.get_register_values()["HL"], register_value["HL"])

  def test_get_register_A(self):
    self.assertEqual(self.state.get_register_A(), 0x01)

  def test_get_register_F(self):
    self.assertEqual(self.state.get_register_F(), 0xB0)

  def test_get_register_B(self):
    self.assertEqual(self.state.get_register_B(), 0x00)

  def test_get_register_C(self):
    self.assertEqual(self.state.get_register_C(), 0x13)
  
  def test_get_register_D(self):
    self.assertEqual(self.state.get_register_D(), 0x00)

  def test_get_register_E(self):
    self.assertEqual(self.state.get_register_E(), 0xD8)

  def test_get_register_H(self):
    self.assertEqual(self.state.get_register_H(), 0x01)

  def test_get_register_L(self):
    self.assertEqual(self.state.get_register_L(), 0x4D)

  def test_get_register_SP(self):
    self.assertEqual(self.state.get_register_SP(), 0xFFFE)

  def test_get_register_PC(self):
    self.assertEqual(self.state.get_register_PC(), 0x0100)

  def test_get_register_AF(self):
    self.assertEqual(self.state.get_register_AF(), 0x01B0)

  def test_get_register_BC(self):
    self.assertEqual(self.state.get_register_BC(), 0x0013)
  
  def test_get_register_DE(self):
    self.assertEqual(self.state.get_register_DE(), 0x00D8)

  def test_get_register_HL(self):
    self.assertEqual(self.state.get_register_HL(), 0x014D)

  def test_register_concatenated(self):
    self.assertEqual(self.state.register_concatenated(), 0x0100FFFE4D01D8001300B001)

  def test_get_zero_flag(self):
    self.assertTrue(self.state.get_zero_flag())

  def test_get_subtract_flag(self):
    self.assertFalse(self.state.get_subtract_flag())

  def test_get_half_carry_flag(self):
    self.assertTrue(self.state.get_half_carry_flag())

  def test_get_carry_flag(self):
    self.assertTrue(self.state.get_carry_flag())

  def test_set_zero_flag(self):
    self.state.set_zero_flag(True)
    self.assertTrue(self.state.get_zero_flag())
    self.state.set_zero_flag(False)
    self.assertFalse(self.state.get_zero_flag())
    self.state.set_zero_flag(True)
    self.assertTrue(self.state.get_zero_flag())

  def test_set_subtract_flag(self):
    self.state.set_subtract_flag(True)
    self.assertTrue(self.state.get_subtract_flag())
    self.state.set_subtract_flag(False)
    self.assertFalse(self.state.get_subtract_flag())
    self.state.set_subtract_flag(True)
    self.assertTrue(self.state.get_subtract_flag())

  def test_set_half_carry_flag(self):
    self.state.set_half_carry_flag(True)
    self.assertTrue(self.state.get_half_carry_flag())
    self.state.set_half_carry_flag(False)
    self.assertFalse(self.state.get_half_carry_flag())
    self.state.set_half_carry_flag(True)
    self.assertTrue(self.state.get_half_carry_flag())

  def test_set_register(self):
    for reg in ["A", "B", "C", "D", "E", "H", "L", "BC", "DE", "HL", "SP", "PC"]:
      for value in range(0, 0x100, 31):
        with self.subTest(reg=reg, value=value):
          self.state.set_register(reg, value)
          self.assertEqual(self.state.get_register_values()[reg], value)

    for reg in ["BC", "DE", "HL", "SP", "PC"]:
      for value in range(0, 0x10000, 31):
        with self.subTest(reg=reg, value=value):
          self.state.set_register(reg, value)
          self.assertEqual(self.state.get_register_values()[reg], value)
    
    reg = "F"
    for value in range(0, 0x100, 5):
      with self.subTest(reg=reg, value=value):
        self.state.set_register(reg, value)
        value &= 0xF0
        self.assertEqual(self.state.get_register_values()[reg], value)

    reg = "AF"
    for value in range(0, 0x10000, 5):
      with self.subTest(reg=reg, value=value):
        self.state.set_register(reg, value)
        value &= 0xFFF0
        self.assertEqual(self.state.get_register_values()[reg], value)

  def test_increase_pc(self):
    pc = 0x0100
    for i in range(3):
      pc += i
      with self.subTest(i=i, pc=pc):
        self.state.increase_pc(i)
        self.assertEqual(self.state.get_register_PC(), pc)
      
  def test_increase_sp(self):
    sp = 0xFF80
    self.state.set_register("SP", sp)
    for i in range(3):
      sp += i
      with self.subTest(i=i, sp=sp):
        self.state.increase_sp(i)
        self.assertEqual(self.state.get_register_SP(), sp)
  
  def test_decrease_sp(self):
    sp = 0xFFFE
    for i in range(3):
      sp -= i
      with self.subTest(i=i, sp=sp):
        self.state.decrease_sp(i)
        self.assertEqual(self.state.get_register_SP(), sp)

  def test_get_memory(self):
    values = {
      0x0000: 0x00,
      0x0010: 0x00,
      0x0020: 0x00,
      0x0100: 0x00,
      0xFF11: 0xBf,
      0xFF12: 0xF3,
      0xFF14: 0xBf,
      0xFF16: 0x3F,
      0xFF19: 0xBF,
      0xFF1A: 0x7F,
      0xFF1B: 0xFF,
      0xFF1C: 0x9F,
      0xFF1E: 0xBF,
      0xFF20: 0xFF,
      0xFF23: 0xBF,
      0xFF24: 0x77,
      0xFF25: 0xF3,
      0xFF26: 0xF1,
      0xFF40: 0x91,
      0xFF47: 0xFC,
      0xFF48: 0xFF,
      0xFF49: 0xFF
    }
    for i in values:
      with self.subTest(i=i):
        self.assertEqual(self.state.get_memory()[i], values[i])

  def test_get_memory_value(self):
    values = {
      0x0000: 0x00,
      0x0010: 0x00,
      0x0020: 0x00,
      0x0100: 0x00,
      0xFF11: 0xBf,
      0xFF12: 0xF3,
      0xFF14: 0xBf,
      0xFF16: 0x3F,
      0xFF19: 0xBF,
      0xFF1A: 0x7F,
      0xFF1B: 0xFF,
      0xFF1C: 0x9F,
      0xFF1E: 0xBF,
      0xFF20: 0xFF,
      0xFF23: 0xBF,
      0xFF24: 0x77,
      0xFF25: 0xF3,
      0xFF26: 0xF1,
      0xFF40: 0x91,
      0xFF47: 0xFC,
      0xFF48: 0xFF,
      0xFF49: 0xFF
    }
    for i in values:
      with self.subTest(i=i):
        self.assertEqual(self.state.get_memory_value(i), values[i])

  def test_set_memory_value(self):
    values = {
      0xC000: 0x02,
      0xC010: 0xF3,
      0xCFFF: 0xAB
    }
    for i in values:
      with self.subTest(i=i):
        self.state.set_memory_value(i, values[i])
        self.assertEqual(self.state.get_memory_value(i), values[i])

  def test_master_interrupt_enabled(self):
    self.assertFalse(self.state.master_interrupt_enabled())

  def test_enable_master_interrupt(self):
    self.state.disable_master_interrupt()
    self.state.enable_master_interrupt()
    self.assertTrue(self.state.master_interrupt_enabled())

  def test_disable_master_interrupt(self):
    self.state.enable_master_interrupt()
    self.state.disable_master_interrupt()
    self.assertFalse(self.state.master_interrupt_enabled())

  def test_interrupt_v_blank_enabled(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.state.set_memory_value(0xFFFF, 0b00000001)
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_interrupt_lcd_stat_enabled(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.state.set_memory_value(0xFFFF, 0b00000010)
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())
  
  def test_interrupt_timer_enabled(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.state.set_memory_value(0xFFFF, 0b00000100)
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_interrupt_serial_enabled(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.state.set_memory_value(0xFFFF, 0b00001000)
    self.assertTrue(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_interrupt_joypad_enabled(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.assertFalse(self.state.interrupt_joypad_enabled())
    self.state.set_memory_value(0xFFFF, 0b00010000)
    self.assertTrue(self.state.interrupt_joypad_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())

  def test_set_interrupt_v_blank(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.state.set_interrupt_v_blank()
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_set_interrupt_lcd_stat(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.state.set_interrupt_lcd_stat()
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_set_interrupt_timer(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.state.set_interrupt_timer()
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_set_interrupt_serial(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.state.set_interrupt_serial()
    self.assertTrue(self.state.interrupt_serial_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_joypad_enabled())

  def test_set_interrupt_joypad(self):
    self.state.set_memory_value(0xFFFF, 0)
    self.state.set_interrupt_joypad()
    self.assertTrue(self.state.interrupt_joypad_enabled())
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertFalse(self.state.interrupt_serial_enabled())
  
  def test_reset_interrupt_v_blank(self):
    self.state.set_memory_value(0xFFFF, 0xFF)
    self.state.reset_interrupt_v_blank()
    self.assertFalse(self.state.interrupt_v_blank_enabled())
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertTrue(self.state.interrupt_serial_enabled())
    self.assertTrue(self.state.interrupt_joypad_enabled())

  def test_reset_interrupt_lcd_stat(self):
    self.state.set_memory_value(0xFFFF, 0xFF)
    self.state.reset_interrupt_lcd_stat()
    self.assertFalse(self.state.interrupt_lcd_stat_enabled())
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertTrue(self.state.interrupt_serial_enabled())
    self.assertTrue(self.state.interrupt_joypad_enabled())

  def test_reset_interrupt_timer(self):
    self.state.set_memory_value(0xFFFF, 0xFF)
    self.state.reset_interrupt_timer()
    self.assertFalse(self.state.interrupt_timer_enabled())
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertTrue(self.state.interrupt_serial_enabled())
    self.assertTrue(self.state.interrupt_joypad_enabled())

  def test_reset_interrupt_serial(self):
    self.state.set_memory_value(0xFFFF, 0xFF)
    self.state.reset_interrupt_serial()
    self.assertFalse(self.state.interrupt_serial_enabled())
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertTrue(self.state.interrupt_joypad_enabled())

  def test_reset_interrupt_joypad(self):
    self.state.set_memory_value(0xFFFF, 0xFF)
    self.state.reset_interrupt_joypad()
    self.assertFalse(self.state.interrupt_joypad_enabled())
    self.assertTrue(self.state.interrupt_v_blank_enabled())
    self.assertTrue(self.state.interrupt_lcd_stat_enabled())
    self.assertTrue(self.state.interrupt_timer_enabled())
    self.assertTrue(self.state.interrupt_serial_enabled())

  def test_is_in_halt(self):
    self.assertFalse(self.state.is_in_halt())

  def test_halt_state(self):
    self.state.halt_state()
    self.assertTrue(self.state.is_in_halt())

  def test_continue_state(self):
    self.state.halt_state()
    self.state.continue_state()
    self.assertFalse(self.state.is_in_halt())

  def test_get_rom_image(self):
    self.assertEqual(self.state.get_rom_image(), self.rom)

  def test_set_rom_image(self):
    new_rom = rom()
    self.state.set_rom_image(new_rom)
    self.assertNotEqual(self.state.get_rom_image(), self.rom)
    self.assertEqual(self.state.get_rom_image(), new_rom)

  def test_get_loaded_rom_bank_number(self):
    self.assertEqual(self.state.get_loaded_rom_bank_number(), 0x01)

  def test_load_rom_bank(self):
    for i in range(0x01, 0xFF, 5):
      with self.subTest(i=i):
        self.state.load_rom_bank(i)
        self.assertEqual(self.state.get_loaded_rom_bank_number(), i)

  def test_get_loaded_ram_bank_number(self):
    self.assertEqual(self.state.get_loaded_ram_bank_number(), 0x00)

  def test_is_in_rom_mode(self):
    self.assertTrue(self.state.is_in_rom_mode())

  def test_set_rom_ram_mode(self):
    self.state.set_rom_ram_mode(1)
    self.assertFalse(self.state.is_in_rom_mode())
    self.state.set_rom_ram_mode(0)
    self.assertTrue(self.state.is_in_rom_mode())

  def test_is_ram_enabled(self):
    self.assertFalse(self.state.is_ram_enabled())

  def test_enabled_ram(self):
    self.state.enable_ram(True)
    self.assertTrue(self.state.is_ram_enabled())

  def test_load_ram_bank(self):
    self.assertEqual(self.state.get_loaded_ram_bank_number(), 0)
    for i in range(0x01, 0x03):
      with self.subTest(i=i):
        self.state.load_ram_bank(i)
        self.assertEqual(self.state.get_loaded_ram_bank_number(), 0)
    new_rom = rom()
    new_rom.set_ram_size_type(0x04)
    self.state = ms(new_rom)
    for i in range(0x01, 0x03):
      with self.subTest(i=i):
        self.state.load_ram_bank(i)
        self.assertEqual(self.state.get_loaded_ram_bank_number(), i)
    self.state.load_ram_bank(0)
    self.assertEqual(self.state.get_loaded_ram_bank_number(), 0)

  def test_get_next_op_code(self):
    new_rom = rom()
    new_rom.set_value(0, 0x0100, 0x01)
    new_rom.set_value(0, 0x0101, 0x34)
    new_rom.set_value(0, 0x0102, 0x12)
    new_state = ms(new_rom)
    op_code, op_length, op_parameter = new_state.get_next_op_code()
    self.assertEqual(op_code, 0x01)
    self.assertEqual(op_length, 0x03)
    self.assertEqual(op_parameter, 0x1234)

  def test_hash(self):
    self.assertEqual(self.state.hash(), b'\x02\xc2\x99\x8a\t\x9e\x97q+\xa6T\x8a\x96e\xa0\x9d\xa0\x85\xdf\xea\xdd4_\x95%\xf6\xe0\x13)X_\xc6')

class test_command(unittest.TestCase):

  def setUp(self):
    self.rom = rom()
    self.state = ms(self.rom)

  def load_command(self, command_bytes):
    new_rom = rom()
    for i in range(len(command_bytes)):
      new_rom.set_value(0, 0x0100 + i, command_bytes[i])
    return ms(new_rom)

  def check_unchanged_register(self, new_state, register):
    for reg in register:
      if reg == "A":
        self.assertEqual(new_state.get_register_A(), self.state.get_register_A())
      elif reg == "F":
        self.assertEqual(new_state.get_register_F(), self.state.get_register_F())
      elif reg == "B":
        self.assertEqual(new_state.get_register_B(), self.state.get_register_B())
      elif reg == "C":
        self.assertEqual(new_state.get_register_C(), self.state.get_register_C())
      elif reg == "D":
        self.assertEqual(new_state.get_register_D(), self.state.get_register_D())
      elif reg == "E":
        self.assertEqual(new_state.get_register_E(), self.state.get_register_E())
      elif reg == "H":
        self.assertEqual(new_state.get_register_H(), self.state.get_register_H())
      elif reg == "L":
        self.assertEqual(new_state.get_register_L(), self.state.get_register_L())
      elif reg == "PC":
        self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC())
      elif reg == "SP":
        self.assertEqual(new_state.get_register_SP(), self.state.get_register_SP())


  def test_0x00(self):
    # NOP
    commands = [0x00]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x01(self):
    # LD BC,d16
    commands = [0x01, 0x34, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_B(), 0x12)
    self.assertEqual(new_state.get_register_C(), 0x34)
    self.check_unchanged_register(new_state, ["A", "F", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x02_write_to_rom(self):
    # LD (BC), A
    commands = [0x02]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_BC()), 0x00)

  def test_0x02_write_to_ram(self):
    # LD (BC), A
    commands = [0x02]
    new_state = self.load_command(commands)
    new_state.set_register("BC", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_BC()), new_state.get_register_A())
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)

  def test_0x03(self):
    # INC BC
    commands = [0x03]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_C()+1)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x04(self):
    # INC B
    commands = [0x04]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_B(), self.state.get_register_B()+1)
    self.check_unchanged_register(new_state, ["A", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x05(self):
    # DEC B
    commands = [0x05]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_B(), (self.state.get_register_B()-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x06(self):
    # LD B,d8
    commands = [0x06, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_B(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x07(self):
    # RLCA
    commands = [0x07]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), 0b00000010)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    for _ in range(7):
      new_state.set_register("PC", 0x0100)
      cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x08_write_to_rom(self):
    # LD (a16), SP
    commands = [0x08, 0x34, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(0x1234), 0x00)
    self.assertEqual(new_state.get_memory_value(0x1235), 0x00)
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)

  def test_0x08_write_to_ram(self):
    # LD (a16), SP
    commands = [0x08, 0x00, 0xC0]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(0xC000), new_state.get_register_SP()&0xFF)
    self.assertEqual(new_state.get_memory_value(0xC001), new_state.get_register_SP()>>8)
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)
  
  def test_0x09(self):
    # ADD HL, BC
    commands = [0x09]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), 0x0160)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x0A(self):
    # LD A, (BC)
    commands = [0x0A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), new_state.get_memory_value(new_state.get_register_BC()))

  def test_0x0B(self):
    # DEC BC
    commands = [0x0B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_BC(), self.state.get_register_BC() -1)

  def test_0x0C(self):
    # INC C
    commands = [0x0C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_C(), self.state.get_register_C()+1)

  def test_0x0D(self):
    # DEC C
    commands = [0x0D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_C(), self.state.get_register_C()-1)
  
  def test_0x0E(self):
    # LD C,d8
    commands = [0x0E, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_C(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x0F(self):
    # RRCA
    commands = [0x0F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), 0b10000000)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    new_state.set_register("PC", 0x0100)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x10(self):
    # STOP 0
    commands = [0x10]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.is_in_halt())

  def test_0x11(self):
    # LD DE,d16
    commands = [0x11, 0x34, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_D(), 0x12)
    self.assertEqual(new_state.get_register_E(), 0x34)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x12_write_to_rom(self):
    # LD (DE), A
    commands = [0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_BC()), 0x00)

  def test_0x12_write_to_ram(self):
    # LD (DE), A
    commands = [0x12]
    new_state = self.load_command(commands)
    new_state.set_register("DE", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_DE()), new_state.get_register_A())
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)

  def test_0x13(self):
    # INC DE
    commands = [0x13]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_DE(), self.state.get_register_DE()+1)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x14(self):
    # INC D
    commands = [0x14]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_D(), self.state.get_register_D()+1)
    self.check_unchanged_register(new_state, ["A", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x15(self):
    # DEC D
    commands = [0x15]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_D(), (self.state.get_register_D()-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x16(self):
    # LD D,d8
    commands = [0x16, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_D(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x17(self):
    # RLA
    commands = [0x17]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), 0b00000011)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    for i in range(7):
      new_state.set_register("PC", 0x0100)
      cmd.excute(op_code, new_state, op_code_parameter)
      self.assertEqual(new_state.get_register_A(), (0b00000110<<i)&0xFF)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x18(self):
    # JR r8
    commands = [0x18, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length + 0x12)

  def test_0x19(self):
    # ADD HL, DE
    commands = [0x19]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), (self.state.get_register_DE()+self.state.get_register_HL())%0x10000)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x1A(self):
    # LD A, (DE)
    commands = [0x1A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), new_state.get_memory_value(new_state.get_register_DE()))
    
  def test_0x1B(self):
    # DEC DE
    commands = [0x1B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_DE(), self.state.get_register_DE() -1)

  def test_0x1C(self):
    # INC E
    commands = [0x1C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_E(), self.state.get_register_E()+1)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x1D(self):
    # DEC E
    commands = [0x1D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_E(), (self.state.get_register_E()-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x1E(self):
    # LD E,d8
    commands = [0x1E, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_E(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
  
  def test_0x1F(self):
    # RRA
    commands = [0x1F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), 0b10000000)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    for i in range(7):
      new_state.set_register("PC", 0x0100)
      cmd.excute(op_code, new_state, op_code_parameter)
      self.assertEqual(new_state.get_register_A(), 0b11000000>>i)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x20(self):
    # JR Z,r8
    # jump if zero flag is True
    commands = [0x20, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    # jump if zero flag is False
    new_state.set_register("PC", 0x0100)
    new_state.set_zero_flag(False)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length + 0x12)

  def test_0x21(self):
    # LD HL,d16
    commands = [0x21, 0x34, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_H(), 0x12)
    self.assertEqual(new_state.get_register_L(), 0x34)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x22_write_to_rom(self):
    # LD (HL+), A
    commands = [0x22]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL() +1)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)

  def test_0x22_write_to_ram(self):
    # LD (HL+), A
    commands = [0x22]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(0xC000), new_state.get_register_A())
    self.assertEqual(new_state.get_register_HL(), 0xC001)
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)

  def test_0x23(self):
    # INC HL
    commands = [0x23]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL()+1)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x24(self):
    # INC H
    commands = [0x24]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_H(), self.state.get_register_H()+1)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x25(self):
    # DEC H
    commands = [0x25]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_H(), (self.state.get_register_H()-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x26(self):
    # LD E,d8
    commands = [0x26, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_H(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x27(self):
    # DAA
    commands = [0x27]
    new_state = self.load_command(commands)
    new_state.set_register("A", 0x1F)
    new_state.set_carry_flag(False)
    new_state.set_subtract_flag(False)
    new_state.set_half_carry_flag(False)

    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), 0x25)
    self.assertFalse(new_state.get_zero_flag())
    self.assertEqual(new_state.get_subtract_flag(), self.state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x28(self):
    # JR Z,r8
    # jump if zero flag is True
    commands = [0x28, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length + 0x12)
    # jump if zero flag is False
    new_state.set_register("PC", 0x0100)
    new_state.set_zero_flag(False)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x29(self):
    # ADD HL, HL
    commands = [0x29]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), (self.state.get_register_HL()+self.state.get_register_HL())%0x10000)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x2A(self):
    # LD A, (HL+)
    commands = [0x2A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), new_state.get_memory_value(new_state.get_register_HL()))
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL()+1)

  def test_0x2B(self):
    # DEC HL
    commands = [0x2B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL() -1)

  def test_0x2C(self):
    # INC L
    commands = [0x2C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_L(), self.state.get_register_L()+1)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x2D(self):
    # DEC L
    commands = [0x2D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_L(), (self.state.get_register_L()-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x2E(self):
    # LD L,d8
    commands = [0x2E, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_L(), 0x12)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x2F(self):
    # CPL
    commands = [0x2F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), (~self.state.get_register_A())&0xFF)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())

  def test_0x30(self):
    # JR NC,r8
    # jump if carry flag is True
    commands = [0x30, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    # jump if carry flag is False
    new_state.set_register("PC", 0x0100)
    new_state.set_carry_flag(False)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length + 0x12)

  def test_0x31(self):
    # LD SP,d16
    commands = [0x31, 0x34, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_SP(), 0x1234)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x32_write_to_rom(self):
    # LD (HL-), A
    commands = [0x32]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL() -1)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)

  def test_0x32_write_to_ram(self):
    # LD (HL-), A
    commands = [0x32]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(0xC000), new_state.get_register_A())
    self.assertEqual(new_state.get_register_HL(), 0xBFFF)
    self.assertEqual(new_state.get_loaded_rom_bank_number(), 0x01)

  def test_0x33(self):
    # INC SP
    commands = [0x33]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_SP(), self.state.get_register_SP()+1)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x34_write_to_rom(self):
    # INC (HL)
    commands = [0x34]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x34_write_to_ram(self):
    # INC (HL)
    commands = [0x34]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), self.state.get_memory_value(self.state.get_register_HL())+1)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x35_write_to_rom(self):
    # DEC (HL)
    commands = [0x35]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x35_write_to_ram(self):
    # DEC (HL)
    commands = [0x35]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), (self.state.get_memory_value(self.state.get_register_HL())-1)%0x100)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x36_write_to_rom(self):
    # LD (HL),d8
    commands = [0x36, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)

  def test_0x36_write_to_ram(self):
    # LD (HL),d8
    commands = [0x36, 0x12]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x12)

  def test_0x37(self):
    # SCF
    commands = [0x37]
    new_state = self.load_command(commands)
    new_state.set_carry_flag(False)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertEqual(new_state.get_subtract_flag(), self.state.get_subtract_flag())
    self.assertEqual(new_state.get_half_carry_flag(), self.state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())
    # flag keeps set after set once more
    new_state.set_register("PC", 0x0100)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertEqual(new_state.get_subtract_flag(), self.state.get_subtract_flag())
    self.assertEqual(new_state.get_half_carry_flag(), self.state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

    new_state.set_register("HL", 0xC000)

  def test_0x38(self):
    # JR C,r8
    # jump if carry flag is True
    commands = [0x38, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length + 0x12)
    # jump if carry flag is False
    new_state.set_register("PC", 0x0100)
    new_state.set_carry_flag(False)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x39(self):
    # ADD HL, SP
    commands = [0x39]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_HL(), (self.state.get_register_SP()+self.state.get_register_HL())%0x10000)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x3A(self):
    # LD A, (HL-)
    commands = [0x3A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), new_state.get_memory_value(new_state.get_register_HL()))
    self.assertEqual(new_state.get_register_HL(), self.state.get_register_HL()-1)

  def test_0x3B(self):
    # DEC SP
    commands = [0x3B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_SP(), self.state.get_register_SP() -1)

  def test_0x3C(self):
    # INC A
    commands = [0x3C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), self.state.get_register_A()+1)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "L", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x3D(self):
    # DEC A
    commands = [0x3D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertEqual(new_state.get_carry_flag(), self.state.get_carry_flag())
    self.assertEqual(new_state.get_register_A(), (self.state.get_register_A()-1)%0x100)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)

  def test_0x3E(self):
    # LD A,d8
    commands = [0x3E, 0x12]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_A(), 0x12)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
  
  def test_0x3F(self):
    # CCF
    commands = [0x3F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertEqual(new_state.get_subtract_flag(), self.state.get_subtract_flag())
    self.assertEqual(new_state.get_half_carry_flag(), self.state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())
    # flag complement once more
    new_state.set_register("PC", 0x0100)
    cmd.excute(op_code, new_state, op_code_parameter)
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_zero_flag(), self.state.get_zero_flag())
    self.assertEqual(new_state.get_subtract_flag(), self.state.get_subtract_flag())
    self.assertEqual(new_state.get_half_carry_flag(), self.state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x40(self):
    # LD B,B
    commands = [0x40]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_B())

  def test_0x41(self):
    # LD B,C
    commands = [0x41]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_C())

  def test_0x42(self):
    # LD B,D
    commands = [0x42]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_D())

  def test_0x43(self):
    # LD B,E
    commands = [0x43]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_E())

  def test_0x44(self):
    # LD B,H
    commands = [0x44]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_H())

  def test_0x45(self):
    # LD B,L
    commands = [0x45]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_L())

  def test_0x46(self):
    # LD B,(HL)
    commands = [0x46]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x47(self):
    # LD B,A
    commands = [0x47]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_B(), self.state.get_register_A())

  def test_0x48(self):
    # LD C,B
    commands = [0x48]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_B())

  def test_0x49(self):
    # LD C,C
    commands = [0x49]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_C())

  def test_0x4A(self):
    # LD C,D
    commands = [0x4A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_D())

  def test_0x4B(self):
    # LD C,E
    commands = [0x4B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_E())

  def test_0x4C(self):
    # LD C,H
    commands = [0x4C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_H())

  def test_0x4D(self):
    # LD C,L
    commands = [0x4D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_L())

  def test_0x4E(self):
    # LD C,(HL)
    commands = [0x4E]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x4F(self):
    # LD C,A
    commands = [0x4F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_C(), self.state.get_register_A())

  def test_0x50(self):
    # LD D,B
    commands = [0x50]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_B())

  def test_0x51(self):
    # LD D,C
    commands = [0x51]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_C())

  def test_0x52(self):
    # LD D,D
    commands = [0x52]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_D())

  def test_0x53(self):
    # LD D,E
    commands = [0x53]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_E())

  def test_0x54(self):
    # LD D,H
    commands = [0x54]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_H())

  def test_0x55(self):
    # LD D,L
    commands = [0x55]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_L())

  def test_0x56(self):
    # LD D,(HL)
    commands = [0x56]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x57(self):
    # LD D,A
    commands = [0x57]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_D(), self.state.get_register_A())

  def test_0x58(self):
    # LD E,B
    commands = [0x58]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_B())

  def test_0x59(self):
    # LD E,C
    commands = [0x59]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_C())

  def test_0x5A(self):
    # LD E,D
    commands = [0x5A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_D())

  def test_0x5B(self):
    # LD E,E
    commands = [0x5B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_E())

  def test_0x5C(self):
    # LD E,H
    commands = [0x5C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_H())

  def test_0x5D(self):
    # LD E,L
    commands = [0x5D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_L())

  def test_0x5E(self):
    # LD E,(HL)
    commands = [0x5E]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x5F(self):
    # LD E,A
    commands = [0x5F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_E(), self.state.get_register_A())

  def test_0x60(self):
    # LD H,B
    commands = [0x60]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_B())

  def test_0x61(self):
    # LD H,C
    commands = [0x61]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_C())

  def test_0x62(self):
    # LD H,D
    commands = [0x62]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_D())

  def test_0x63(self):
    # LD H,E
    commands = [0x63]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_E())

  def test_0x64(self):
    # LD H,H
    commands = [0x64]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_H())

  def test_0x65(self):
    # LD H,L
    commands = [0x65]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_L())

  def test_0x66(self):
    # LD H,(HL)
    commands = [0x66]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x67(self):
    # LD H,A
    commands = [0x67]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_H(), self.state.get_register_A())

  def test_0x68(self):
    # LD L,B
    commands = [0x68]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_B())

  def test_0x69(self):
    # LD L,C
    commands = [0x69]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_C())

  def test_0x6A(self):
    # LD L,D
    commands = [0x6A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_D())

  def test_0x6B(self):
    # LD L,E
    commands = [0x6B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_E())

  def test_0x6C(self):
    # LD L,H
    commands = [0x6C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_H())

  def test_0x6D(self):
    # LD L,L
    commands = [0x6D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_L())

  def test_0x6E(self):
    # LD L,(HL)
    commands = [0x6E]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x6F(self):
    # LD L,A
    commands = [0x6F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_L(), self.state.get_register_A())

  def test_0x70_write_to_rom(self):
    # LD (HL),B
    commands = [0x70]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)
  
  def test_0x71_write_to_rom(self):
    # LD (HL),C
    commands = [0x71]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)

  def test_0x72_write_to_rom(self):
    # LD (HL),D
    commands = [0x72]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)

  def test_0x73_write_to_rom(self):
    # LD (HL),E
    commands = [0x73]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)

  def test_0x74_write_to_rom(self):
    # LD (HL),H
    commands = [0x74]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)

  def test_0x75_write_to_rom(self):
    # LD (HL),L
    commands = [0x75]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(self.state.get_memory_value(self.state.get_register_HL()), 0x00)

  def test_0x70_write_to_ram(self):
    # LD (HL),B
    commands = [0x70]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), self.state.get_register_B())
  
  def test_0x71_write_to_ram(self):
    # LD (HL),C
    commands = [0x71]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), self.state.get_register_C())

  def test_0x72_write_to_ram(self):
    # LD (HL),D
    commands = [0x72]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), self.state.get_register_D())

  def test_0x73_write_to_ram(self):
    # LD (HL),E
    commands = [0x73]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), self.state.get_register_E())

  def test_0x74_write_to_ram(self):
    # LD (HL),H
    commands = [0x74]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0xC0)

  def test_0x75_write_to_ram(self):
    # LD (HL),L
    commands = [0x75]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(new_state.get_register_HL()), 0x00)

  def test_0x76(self):
    # HALT
    commands = [0x76]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.is_in_halt())

  def test_0x78(self):
    # LD A,B
    commands = [0x78]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_B())

  def test_0x79(self):
    # LD A,C
    commands = [0x79]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_C())

  def test_0x7A(self):
    # LD A,D
    commands = [0x7A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_D())

  def test_0x7B(self):
    # LD A,E
    commands = [0x7B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_E())

  def test_0x7C(self):
    # LD A,H
    commands = [0x7C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_H())

  def test_0x7D(self):
    # LD A,L
    commands = [0x7D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_L())

  def test_0x7E(self):
    # LD A,(HL)
    commands = [0x7E]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_memory_value(self.state.get_register_HL()))

  def test_0x7F(self):
    # LD A,A
    commands = [0x7F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_register_A())

  def test_0x80(self):
    # ADD A,B
    commands = [0x80]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_B()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x81(self):
    # ADD A,C
    commands = [0x81]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_C()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x82(self):
    # ADD A,D
    commands = [0x82]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_D()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x83(self):
    # ADD A,E
    commands = [0x83]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_E()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x84(self):
    # ADD A,H
    commands = [0x84]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_H()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x85(self):
    # ADD A,L
    commands = [0x85]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_L()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x86(self):
    # ADD A,(HL)
    commands = [0x86]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0xFF)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + new_state.get_memory_value(new_state.get_register_HL())) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x87(self):
    # ADD A,A
    commands = [0x87]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_A()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x88(self):
    # ADDC A,B
    commands = [0x88]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_B() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x89(self):
    # ADDC A,C
    commands = [0x89]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_C() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x8A(self):
    # ADDC A,D
    commands = [0x8A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_D() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x8B(self):
    # ADDC A,E
    commands = [0x8B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_E() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x8C(self):
    # ADDC A,H
    commands = [0x8C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_H() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x8D(self):
    # ADDC A,L
    commands = [0x8D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_L() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x8E(self):
    # ADDC A,(HL)
    commands = [0x8E]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0xFE)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + new_state.get_memory_value(new_state.get_register_HL()) + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x8F(self):
    # ADDC A,A
    commands = [0x8F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() + self.state.get_register_A() + 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x90(self):
    # SUB A,B
    commands = [0x90]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_B()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x91(self):
    # SUB A,C
    commands = [0x91]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_C()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x92(self):
    # SUB A,D
    commands = [0x92]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_D()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x93(self):
    # SUB A,E
    commands = [0x93]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_E()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x94(self):
    # SUB A,H
    commands = [0x94]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_H()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x95(self):
    # SUB A,L
    commands = [0x95]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_L()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x96(self):
    # SUB A,(HL)
    commands = [0x96]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0x02)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - new_state.get_memory_value(new_state.get_register_HL())) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x97(self):
    # SUB A,A
    commands = [0x97]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_A()) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x98(self):
    # SUBC A,B
    commands = [0x98]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_B() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x99(self):
    # SUBC A,C
    commands = [0x99]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_C() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x9A(self):
    # SUBC A,D
    commands = [0x9A]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_D() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x9B(self):
    # SUBC A,E
    commands = [0x9B]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_E() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x9C(self):
    # SUBC A,H
    commands = [0x9C]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_H() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x9D(self):
    # SUBC A,L
    commands = [0x9D]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_L() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0x9E(self):
    # SUBC A,(HL)
    commands = [0x9E]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_register("A", 0x10)
    new_state.set_memory_value(0xC000, 0x02)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = 0x10-0x02-0x01
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0x9F(self):
    # SUBC A,A
    commands = [0x9F]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = (self.state.get_register_A() - self.state.get_register_A() - 1) % 0x100
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0xA0(self):
    # AND A,B
    commands = [0xA0]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_B()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA1(self):
    # AND A,C
    commands = [0xA1]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_C()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA2(self):
    # AND A,D
    commands = [0xA2]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_D()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA3(self):
    # AND A,E
    commands = [0xA3]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_E()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA4(self):
    # AND A,H
    commands = [0xA4]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_H()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA5(self):
    # AND A,L
    commands = [0xA5]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_L()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA6(self):
    # AND A,(HL)
    commands = [0xA6]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0xFF)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & new_state.get_memory_value(new_state.get_register_HL())
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA7(self):
    # AND A,A
    commands = [0xA7]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & self.state.get_register_A()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA8(self):
    # XOR A,B
    commands = [0xA8]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_B()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xA9(self):
    # XOR A,C
    commands = [0xA9]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_C()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAA(self):
    # XOR A,D
    commands = [0xAA]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_D()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAB(self):
    # XOR A,E
    commands = [0xAB]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_E()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAC(self):
    # XOR A,H
    commands = [0xAC]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_H()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAD(self):
    # XOR A,L
    commands = [0xAD]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_L()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAE(self):
    # XOR A,(HL)
    commands = [0xAE]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0xFE)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ 0xFE
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xAF(self):
    # XOR A,A
    commands = [0xAF]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() ^ self.state.get_register_H()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB0(self):
    # OR A,B
    commands = [0xB0]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_B()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB0_zero_flag(self):
    # OR A,B
    commands = [0xB0]
    new_state = self.load_command(commands)
    new_state.set_register("A", 0x00)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = 0x00
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB1(self):
    # OR A,C
    commands = [0xB1]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_C()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB2(self):
    # OR A,D
    commands = [0xB2]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_D()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB3(self):
    # OR A,E
    commands = [0xB3]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_E()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB4(self):
    # OR A,H
    commands = [0xB4]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_H()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB5(self):
    # OR A,L
    commands = [0xB5]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_L()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB6(self):
    # OR A,(HL)
    commands = [0xB6]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | 0x00
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB7(self):
    # OR A,A
    commands = [0xB7]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() | self.state.get_register_A()
    self.assertEqual(new_state.get_register_A(), result)
    self.assertFalse(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB8(self):
    # CP A,B
    commands = [0xB8]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xB9(self):
    # CP A,C
    commands = [0xB9]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0xBA(self):
    # CP A,D
    commands = [0xBA]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xBB(self):
    # CP A,E
    commands = [0xBB]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0xBC(self):
    # CP A,H
    commands = [0xBC]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xBD(self):
    # CP A,L
    commands = [0xBD]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertTrue(new_state.get_carry_flag())

  def test_0xBE(self):
    # CP A,(HL)
    commands = [0xBE]
    new_state = self.load_command(commands)
    new_state.set_register("HL", 0xC000)
    new_state.set_memory_value(0xC000, 0x01)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xBF(self):
    # CP A,A
    commands = [0xBF]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())

  def test_0xC3(self):
    # JP a16
    commands = [0xC3, 0x00, 0x01]
    new_state = self.load_command(commands)
    op_code, _, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), 0x0100)







  def test_0xE0(self):
    # LDH (a8),A
    commands = [0xE0, 0x80]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_memory_value(0xFF80), 0x01)




  def test_0xE6(self):
    # AND A,d8
    commands = [0xE6, 0xFE]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    result = self.state.get_register_A() & 0xFE
    self.assertEqual(new_state.get_register_A(), result)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertTrue(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())






  def test_0xF0(self):
    # LDH A,(a8)
    commands = [0xF0, 0x00]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertEqual(new_state.get_register_A(), self.state.get_memory_value(0xFF00))

  



  def test_0xF3(self):
    # DI
    commands = [0xF3]
    new_state = self.load_command(commands)
    new_state.enable_master_interrupt()
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "F", "B", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertFalse(new_state.master_interrupt_enabled())






  def test_0xFE(self):
    # CP A,d8
    commands = [0xFE, 0x01]
    new_state = self.load_command(commands)
    op_code, op_length, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "B", "C", "D", "E", "H", "L", "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + op_length)
    self.assertTrue(new_state.get_zero_flag())
    self.assertTrue(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())





  def test_0xCB_0x30(self):
    # SWAP B
    commands = [0xCB, 0x30]
    new_state = self.load_command(commands)
    op_code, _, op_code_parameter = new_state.get_next_op_code()
    cmd.excute(op_code, new_state, op_code_parameter)
    self.check_unchanged_register(new_state, ["A", "C", "D", "E", "H", "L" "SP"])
    self.assertEqual(new_state.get_register_PC(), self.state.get_register_PC() + 2)
    self.assertEqual(new_state.get_register_B(), 0x00)
    self.assertTrue(new_state.get_zero_flag())
    self.assertFalse(new_state.get_subtract_flag())
    self.assertFalse(new_state.get_half_carry_flag())
    self.assertFalse(new_state.get_carry_flag())


