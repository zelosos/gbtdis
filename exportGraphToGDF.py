#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import sys

"""
This script uses the by gbtdis generated jumps.json, separation.json and manual.json
to generate a graph in which each program section is represented by one
node. Edges are the jumps and call between them. They are weighted with the
distance of the jump / call.
The graph is then exported into a gdf file to be imported in gephi.
There you can visualize the program flow. Usefull there is the coloring by
distance to selected note. This way different areas can be spotted fast. 
"""

start_addresses = {0x0100}
jump_from = set()
jump_map = {}
data = {}
with open("jumps.json") as f:
  try:
    data = json.load(f)
  except:
    sys.exit(2)
  for line in data:
    from_address = int(line[1])
    target_address = int(line[3])
    jump_type = line[4]
    jump_from.add(from_address)
    start_addresses.add(target_address)
    if from_address not in jump_map:
      jump_map[from_address] = set()
    jump_map[from_address].add( (target_address, jump_type) )

end_addresses = set()
# read separation.json and remember start and end 
with open("separation.json") as f:
  try:
    data = json.load(f)
  except:
    sys.exit(2)
  for line in data["0"]["program"]:
    start_addresses.add(int(line[0]))
    end_addresses.add(int(line[1]))

# read manual.json and remember addresses
manual_addresses = []
with open("manual.json") as f:
  try:
    data = json.load(f)
  except:
    sys.exit(2)
  for line in data:
    address = int(line[1])
    manual_addresses.append(address)

nodes = start_addresses
node_length = {}
edges = set()
# make nodes of start address
# so jump from has to be converted to start address of the section
for from_address in jump_from:
  start = from_address
  # find beginning of section
  while start not in start_addresses:
    start -= 1

  for target in jump_map[from_address]:
    jump_type = target[1]
    target = target[0]
    if target != start:
      weight = 1/abs(start-target) + 0.5
    else:
      weight = 0.1
    edges.add( (start, target, weight, jump_type) ) # add edge beetween currect section and target of jump

# length is length of section (excluding the length of the jump command)
for address in start_addresses:
  # if following code flow is not known set lenght to 0
  if address in manual_addresses:
    node_length[address] = 0
    continue
  # determine length
  length = 1
  while address+length not in start_addresses and address+length not in end_addresses:
    length += 1
  node_length[address] = length

# export to GDF file
with open("graph.gdf", "w") as f:
  # write nodes to file
  f.write("nodedef>name VARCHAR, label VARCHAR, length DOUBLE\n")
  for i in nodes:
    f.write("0x{:04X}, 0x{:04X}, {}\n".format(i, i, node_length[i]))
  
  # write weighted edges to file
  f.write("edgedef>node1 VARCHAR,node2 VARCHAR, directed BOOLEAN, weight DOUBLE, type VARCHAR\n")
  for i in edges:
    f.write("0x{:04X}, 0x{:04X}, true, {}, {}\n".format(i[0], i[1], i[2], i[3])) # directed is allways true
